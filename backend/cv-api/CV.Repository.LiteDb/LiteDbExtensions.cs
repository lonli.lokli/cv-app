﻿// Copyright (c) 2019 under MIT license.

#region

using LiteDB;
using static LanguageExt.Prelude;

#endregion

namespace CV.Repository.LiteDb;

public static class LiteDbExtensions
{
    public static ObjectId AsId(this string id) =>
        Try(() => new ObjectId(id))
            .Match(succ => succ, _ => ObjectId.Empty);
}