﻿// Copyright (c) 2019 under MIT license.

#region

using System.Linq;
using CV.Domain.Models;
using CV.Domain.Types;
using CV.Repository.Abstractions;
using LanguageExt;
using static LanguageExt.Prelude;

#endregion

namespace CV.Repository.LiteDb.Repositories;

/// <inheritdoc />
internal class DocumentRepository : RepositoryBase, IDocumentRepository
{
    /// <inheritdoc />
    public DocumentRepository(IDbFactory factory)
        : base(factory)
    {
    }

    /// <inheritdoc />
    public EitherAsync<BadRequest, Unit> Save(Seq<Document> documents)
    {
        using var db = GetDatabase();
        var docCollection = db.GetCollection<DocumentEntity>();
        docCollection.Insert(documents.Select(e => e.ToEntity()));

        return RightAsync<BadRequest, Unit>(Unit.Default);
    }

    /// <inheritdoc />
    public EitherAsync<BadRequest, Seq<Document>> GetAll()
    {
        using var db = GetDatabase();
        var docCollection = db.GetCollection<DocumentEntity>();
        return Seq(docCollection.FindAll()
            .Select(i => i.ToDomain()).ToList()); // ToList required as LAzy Seq will lead to later DisposedException
    }

    /// <inheritdoc />
    public OptionAsync<Document> GetById(string id)
    {
        using var db = GetDatabase();
        var docCollection = db.GetCollection<DocumentEntity>();
        var dbItem = docCollection.FindById(id.AsId());
        return Optional(dbItem?.ToDomain()).ToAsync();
    }

    /// <inheritdoc />
    public EitherAsync<BadRequest, bool> Update(string documentId, Document document)
    {
        var dbItem = document.ToEntity(documentId);
        using var db = GetDatabase();
        var docCollection = db.GetCollection<DocumentEntity>();
        return docCollection.Update(dbItem);
    }

    /// <inheritdoc />
    public EitherAsync<BadRequest, bool> Delete(string id)
    {
        using var db = GetDatabase();
        var docCollection = db.GetCollection<DocumentEntity>();
        return docCollection.Delete(id.AsId());
    }
}