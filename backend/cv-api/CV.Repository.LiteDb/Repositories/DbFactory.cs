﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Domain.Configurations;
using LiteDB;

#endregion

namespace CV.Repository.LiteDb.Repositories;

/// <summary>
///     Db Factory.
/// </summary>
/// <seealso cref="CV.Repository.LiteDb.Repositories.IDbFactory" />
internal class DbFactory : IDbFactory
{
    private readonly LiteDbConfiguration _configuration;

    /// <inheritdoc />
    public DbFactory(LiteDbConfiguration configuration)
    {
        _configuration = configuration;
    }

    /// <inheritdoc />
    public LiteDatabase Create()
    {
        var connectionString = new ConnectionString(_configuration.DbPath)
        {
            Password = _configuration.Password
        };
        return new LiteDatabase(connectionString);
    }
}