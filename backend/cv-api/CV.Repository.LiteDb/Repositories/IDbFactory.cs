﻿// Copyright (c) 2019 under MIT license.

using LiteDB;

namespace CV.Repository.LiteDb.Repositories;

/// <summary>
///     Allows to create database instance.
/// </summary>
internal interface IDbFactory
{
    /// <summary>
    ///     Creates db instance.
    /// </summary>
    LiteDatabase Create();
}