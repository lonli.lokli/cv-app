﻿// Copyright (c) 2019 under MIT license.

using System.Linq;
using CV.Domain.Models;
using CV.Domain.Types;
using CV.Repository.Abstractions;
using LanguageExt;
using static LanguageExt.Prelude;

namespace CV.Repository.LiteDb.Repositories;

internal class JobRepository : RepositoryBase, IJobRepository
{
    public JobRepository(IDbFactory dbFactory) : base(dbFactory)
    {
    }

    /// <inheritdoc />
    public EitherAsync<BadRequest, Seq<Job>> GetAll()
    {
        using var db = GetDatabase();
        var docCollection = db.GetCollection<JobEntity>();
        return Seq(docCollection.FindAll()
            .Select(i => i.ToDomain())
            .ToList()); // ToList required as LAzy Seq will lead to later DisposedException
    }
}
