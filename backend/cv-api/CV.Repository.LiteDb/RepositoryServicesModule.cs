﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Repository.Abstractions;
using CV.Repository.LiteDb.Repositories;
using Microsoft.Extensions.DependencyInjection;

#endregion

namespace CV.Repository.LiteDb;

/// <summary>
///     Services module for Repository library.
/// </summary>
public class RepositoryServicesModule
{
    public static void ConfigureServices(IServiceCollection services)
    {
        services.AddSingleton<IDbFactory, DbFactory>();
        services.AddSingleton<IDocumentRepository, DocumentRepository>();
        services.AddSingleton<IJobRepository, JobRepository>();
    }
}