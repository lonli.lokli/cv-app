﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using LiteDB;

namespace CV.Repository.LiteDb;

internal record DocumentEntity([BsonId]ObjectId Id, string? Name, string? Author, byte[] Data);

internal enum JobEntityFeatures
{
    RemoteFeature_Office = 0b0000001,
    RemoteFeature_Remote = 0b0000010,
    RemoteFeature_Hybrid = 0b0000011,
    WorkAgreementFeature_Contract = 0b0000100,
    WorkAgreementFeature_FullTime = 0b0000101
}

internal record JobEntity([BsonId]ObjectId Id, int SalaryMin, int SalaryMax, string Occupation,
    string CompanyName, string Country, string Town, Dictionary<string, BsonValue> JobFeatures, string ContactEmail, string ContactPhone);