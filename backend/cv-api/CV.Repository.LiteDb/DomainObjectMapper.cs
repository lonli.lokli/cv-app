﻿// Copyright (c) 2019 under MIT license.

#region

using System;
using System.Collections.Generic;
using CV.Domain.Models;
using LiteDB;

#endregion

namespace CV.Repository.LiteDb;

internal static class DomainObjectMapper
{
    public static Document ToDomain(this DocumentEntity document) => new(document.Name, document.Author,
        document.Data, document.Id.ToString());

    public static Job ToDomain(this JobEntity job) => new(new SalaryRange(job.SalaryMin, job.SalaryMax), job.Occupation,
        new Company(job.CompanyName), new Place(job.Country, job.Town),
        new JobFeatures(MapRemoteFeatureToDomain(job.JobFeatures), MapWorkAgreementFeatureToDomain(job.JobFeatures)),
        new Contact(job.ContactEmail, job.ContactPhone), job.Id.ToString());

    public static DocumentEntity ToEntity(this Document document, string? id = null) => new(
        new ObjectId(id ?? document.Id),
        document.Name, document.Author, document.Data ?? Array.Empty<byte>());

    private static RemoteFeature MapRemoteFeatureToDomain(Dictionary<string, BsonValue> entity) =>
        entity[nameof(JobFeatures)]
                .AsString switch
            {
                nameof(JobEntityFeatures.RemoteFeature_Office) => RemoteFeature.Office,
                nameof(JobEntityFeatures.RemoteFeature_Remote) => RemoteFeature.Remote,
                _ => RemoteFeature.Hybrid
            };

    private static WorkAgreementFeature MapWorkAgreementFeatureToDomain(Dictionary<string, BsonValue> entity) =>
        entity[nameof(WorkAgreementFeature)]
                .AsString switch
            {
                nameof(JobEntityFeatures.WorkAgreementFeature_Contract) => WorkAgreementFeature.Contract,
                _ => WorkAgreementFeature.FullTime
            };
}