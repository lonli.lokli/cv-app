Resources used:
1. https://medium.com/@michaeldimoudis/hardening-asp-net-core-3-1-docker-images-f0c2ede1667f
2. https://www.softwaredeveloper.blog/multi-project-dotnet-core-solution-in-docker-image
3. https://github.com/terraform-aws-modules/terraform-aws-ecs
4. https://andrewlock.net/using-serilog-aspnetcore-in-asp-net-core-3-logging-the-selected-endpoint-name-with-serilog/
5. https://medium.com/imaginelearning/using-new-relic-with-docker-for-monitoring-net-core-applications-8ab57b0883aa