﻿// Copyright (c) 2019 under MIT license.
namespace CV.Domain

module WebApiConstants =

    [<Literal>]
    let DevelopmentCorsPolicy = "DevelopmentCorsPolicy"

    [<Literal>]
    let ProductionCorsPolicy = "ProductionCorsPolicy"

    [<Literal>]
    let LITEDB_CONFIGURATION_PREFIX = "LiteDb"

    [<Literal>]
    let SECURITY_CONFIGURATION_PREFIX = "Security"

    [<Literal>]
    let AUTH_CONFIGURATION_PREFIX = "Auth"
