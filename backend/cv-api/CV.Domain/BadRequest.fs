﻿namespace CV.Domain.Types

open System

type BadRequest private (message: string) =
    member this.Message = message
    static member Create message = BadRequest message

    static member Join(x: BadRequest, y: BadRequest) =
        BadRequest(x.Message + Environment.NewLine + y.Message)
