﻿namespace CV.Domain.Configurations

open System.ComponentModel.DataAnnotations
open CV.Domain

type AuthOptions() =
    inherit ConfigurationObjectBase()

    [<Required; MinLength(1)>]
    member val ClientId = "" with get, set

    [<Required; MinLength(1)>]
    member val ClientSecret = "" with get, set

    [<Required; MinLength(1)>]
    member val Authority = "" with get, set
    
    [<Required; MinLength(1)>]
    member val Audience = "" with get, set
