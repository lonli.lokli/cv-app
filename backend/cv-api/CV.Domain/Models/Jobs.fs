﻿namespace CV.Domain.Models

open System

type Company = {
    Name: string
}

type Place = {
    Country:  string
    Town: string
}

type SalaryRange = {
    Min: int
    Max: int
}

[<Flags>]
type RemoteFeature  =
    | Office    = 0b0000001
    | Remote    = 0b0000010
    | Hybrid    = 0b0000100

[<Flags>]
type WorkAgreementFeature  =
    | Contract    = 0b0000001
    | FullTime    = 0b0000010

type JobFeatures = {
    Remote: RemoteFeature
    WorkAgreement: WorkAgreementFeature
}

type Contact = {
    Email: string
    Phone: string
}

type Job = {
    Salary: SalaryRange
    Occupation: string
    Company: Company
    Place: Place
    Features: JobFeatures
    Contact: Contact
    Id: string
}