﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain.Models

open LiteDB

type Document(name: string, author: string, data: byte [], id: string) =
    member this.Name = name
    member this.Author = author
    member this.Data = data
    member this.Id = id

    new(name, author, data) = Document(name, author, data, ObjectId.NewObjectId().ToString())
