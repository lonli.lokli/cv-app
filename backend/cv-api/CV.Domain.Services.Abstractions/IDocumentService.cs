﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Domain.Models;
using CV.Domain.Types;
using LanguageExt;

#endregion

namespace CV.Domain.Services.Abstractions;

/// <summary>
///     Document processing service.
/// </summary>
public interface IDocumentService
{
    /// <summary>
    ///     Updates the specified document.
    /// </summary>
    /// <param name="document">The document.</param>
    OptionAsync<EitherAsync<BadRequest, Unit>> Update(string documentId, Document document, string user);

    /// <summary>
    ///     Deletes the specified document.
    /// </summary>
    /// <param name="documentId">The document identifier.</param>
    OptionAsync<EitherAsync<BadRequest, Unit>> Delete(string documentId, string user);
}