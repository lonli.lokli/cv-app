﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Models;
using CV.Domain.Types;
using LanguageExt;

namespace CV.Domain.Services.Abstractions;

/// <summary>
/// Job Service abstraction
/// </summary>
public interface IJobService
{
    /// <summary>
    ///     Retrieves the list of jobs.
    /// </summary>
    EitherAsync<BadRequest, Seq<Job>> GetAll();
}