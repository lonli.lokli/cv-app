﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain.Extensions

open System.Runtime.CompilerServices
open System
open System.Security.Claims
open Microsoft.Extensions.DependencyInjection

[<Extension>]
type CollectionExtensions =
    [<Extension>]
    static member inline ByteArrayCompare(a1: byte [], a2: ReadOnlySpan<byte>) = a1.AsSpan().SequenceEqual(a2)

[<Extension>]
type ObjectExtensions =
    [<Extension>]
    static member inline AsEnumerable(a: 'T) = seq { yield! [a]}
    
[<Extension>]
type ClaimsExtensions =
    [<Extension>]
    static member inline GetUserName(principal: ClaimsPrincipal) = principal.Identity.Name
    
[<Extension>]
type ServiceCollectionExtensions =
    [<Extension>]
    static member inline Add<'T when 'T :> IServiceCollection and 'T: (new : unit -> 'T)>
        (services: IServiceCollection)
        =
        for d in new 'T() do
            services.Add(d)
