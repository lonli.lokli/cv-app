﻿// Copyright (c) 2019 under MIT license.

namespace CV.WebApi.Extensions;

public static class UrlExtensions
{
    public static string UrlCombine(string url1, string url2)
    {
        if (url1.Length == 0)
        {
            return url2;
        }

        if (url2.Length == 0)
        {
            return url1;
        }

        return $"{url1.TrimEnd('/', '\\')}/{url2.TrimStart('/', '\\')}";
    }
}