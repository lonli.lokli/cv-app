﻿// Copyright (c) 2019 under MIT license.

#region

using System.Diagnostics.CodeAnalysis;
using System.Text.Json;

#endregion

namespace CV.WebApi.Extensions;

/// <summary>
///     ApplicationBuilder extensions.
/// </summary>
public static class ApplicationExtensions
{
    public static Task WriteDevelopmentResponse(HttpContext httpContext, Func<Task> next)
        => WriteResponse(httpContext);

    [RequiresUnreferencedCode("Calls System.Text.Json.JsonSerializer.SerializeAsync<Microsoft.AspNetCore.Mvc.ProblemDetails>(System.IO.Stream, Microsoft.AspNetCore.Mvc.ProblemDetails, System.Text.Json.JsonSerializerOptions?, System.Threading.CancellationToken)")]
    private static async Task WriteResponse(HttpContext httpContext)
    {
        // Try and retrieve the error from start the ExceptionHandler middleware
        var exceptionDetails = httpContext.Features.Get<IExceptionHandlerFeature>();
        var ex = exceptionDetails?.Error;

        // Should always exist, but best to be safe!
        if (ex != null)
        {
            // ProblemDetails has it's own content type
            httpContext.Response.ContentType = "application/problem+json";
            httpContext.Response.StatusCode = 500;

            var problem = new ProblemDetails
            {
                Detail = ex.Message,
                Title = ServiceConstants.ErrorResponseTitle,
                Status = 500 // TODO: change to constant
                // RequestId = Activity.Current?.Id ?? httpContext.TraceIdentifier ?? string.Empty - we probably need it
            };

            //Serialize the problem details object to the Response as JSON (using System.Text.Json)
            var stream = httpContext.Response.Body;
            await JsonSerializer.SerializeAsync(stream, problem, cancellationToken: httpContext.RequestAborted);
        }
    }
}