﻿// Copyright (c) 2019 under MIT license.

#region

using CV.WebApi.Contributors;

#endregion

namespace CV.WebApi.Extensions;

public static class ContributorExtensions
{
    public static void AddInfoContributor<T>(this IServiceCollection services) where T : class, IInfoContributor
    {
        services.AddSingleton<IInfoContributor, T>();
    }
}