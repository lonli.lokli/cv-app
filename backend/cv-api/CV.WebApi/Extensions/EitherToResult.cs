﻿// Copyright (c) 2019 under MIT license.

#region

#endregion

namespace CV.WebApi.Extensions;

public static class EitherToActionResultExtensions
{
    /// <summary>
    ///     Converts either of option unit to Action Result.
    /// </summary>
    /// <param name="either">The either.</param>
    /// <param name="response"></param>
    /// <returns></returns>
    public static Task<IResult> AsActionResult(this EitherAsync<BadRequest, Option<Unit>> either,
        HttpResponse response) =>
        either.Match(option => option.Match(r => Results.Ok(), () => Results.NotFound()),
            l =>
            {
                response.Headers.Add("X-Notify-User", new[] { bool.TrueString });
                return Results.BadRequest(l.Message);
            });

    /// <summary>
    ///     Converts either of option unit to Action Result.
    /// </summary>
    /// <param name="either">The either.</param>
    /// <param name="response"></param>
    /// <returns></returns>
    public static Task<IResult> AsActionResult(this EitherAsync<BadRequest, OptionAsync<Unit>> either,
        HttpResponse response) =>
        either.MatchAsync(option => option.Match(r => Results.Ok(), () => Results.NotFound()),
            l =>
            {
                response.Headers.Add("X-Notify-User", new[] { bool.TrueString });
                return Results.BadRequest(l.Message);
            });

    /// <summary>
    ///     Converts either of option unit to Action Result.
    /// </summary>
    /// <param name="either">The either.</param>
    /// <param name="response"></param>
    /// <returns></returns>
    public static Task<IResult> AsActionResult(this EitherAsync<BadRequest, Unit> either,
        HttpResponse response) =>
        either.Match(u => Results.Ok(),
            l =>
            {
                response.Headers.Add("X-Notify-User", new[] { bool.TrueString });
                return Results.BadRequest(l.Message);
            });

    /// <summary>
    ///     Converts either of option unit to Action Result.
    /// </summary>
    /// <param name="either">The either.</param>
    /// <returns></returns>
    public static Task<IResult> AsActionResult<T>(this EitherAsync<BadRequest, T> either) =>
        either.Match(data => Results.Ok(data),
            l => Results.BadRequest(l.Message));
}