﻿// Copyright (c) 2019 under MIT license.

using System.Threading;
using Microsoft.AspNetCore.StaticFiles;

namespace CV.WebApi.Extensions;

public static class DataExtensions
{
    private const string DefaultContentType = "application/octet-stream";

    public static string GetMimeTypeForFileExtension(this string filePath)
    {
        var provider = new FileExtensionContentTypeProvider();

        if (!provider.TryGetContentType(filePath, out var contentType))
        {
            contentType = DefaultContentType;
        }

        return contentType;
    }

    public static async Task<byte[]> GetContent(this IFormFile file, CancellationToken token)
    {
        await using var stream = file.OpenReadStream();
        await using var memoryStream = new MemoryStream();
        await stream.CopyToAsync(memoryStream, token);
        return memoryStream.ToArray();
    }
}