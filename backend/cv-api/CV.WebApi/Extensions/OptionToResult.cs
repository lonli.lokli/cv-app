﻿// Copyright (c) 2019 under MIT license.

#region

#endregion

namespace CV.WebApi.Extensions;

public static class OptionToIResultExtensions
{
    /// <summary>
    ///     Option WebAPI extensions.
    /// </summary>
    /// <summary>
    ///     Converts options to Action Result.
    /// </summary>
    /// <param name="option"></param>
    /// <param name="map"></param>
    /// <typeparam name="TI"></typeparam>
    /// <typeparam name="TO"></typeparam>
    /// <returns></returns>
    public static IResult AsActionResult<TI, TO>(this Option<TI> option, Func<TI, TO> map) =>
        option.Match(
            t => Results.Ok(map(t)),
            () => Results.NotFound());

    /// <summary>
    ///     Converts task to Action Result.
    /// </summary>
    /// <param name="option"></param>
    /// <param name="map"></param>
    /// <typeparam name="TI"></typeparam>
    /// <typeparam name="TO"></typeparam>
    /// <returns></returns>
    public static Task<IResult> AsActionResult<TI, TO>(this Task<Option<TI>> option, Func<TI, TO> map) =>
        option.Map(o => o.AsActionResult(map));

    /// <summary>
    ///     Converts options to Action Result.
    /// </summary>
    /// <param name="option"></param>
    /// <param name="map"></param>
    /// <typeparam name="TI"></typeparam>
    /// <typeparam name="TO"></typeparam>
    /// <returns></returns>
    public static Task<IResult> AsActionResult<TI, TO>(this OptionAsync<TI> option, Func<TI, TO> map) =>
        option.Match(
            t => Results.Ok(map(t)),
            () => Results.NotFound());

    /// <summary>
    ///     Converts options to Action Result.
    /// </summary>
    /// <typeparam name="TI">The type of the i.</typeparam>
    /// <typeparam name="TO">The type of the o.</typeparam>
    /// <param name="option">The option.</param>
    /// <param name="map">The map.</param>
    /// <returns></returns>
    public static Task<IResult> AsCustomActionResult<TI, TO>(this OptionAsync<TI> option, Func<TI, TO> map)
        where TO : IResult =>
        option.Match(
            t => map(t),
            () => Results.NotFound());

    /// <summary>
    ///     Converts options to Action Result.
    /// </summary>
    /// <typeparam name="TI">The type of the i.</typeparam>
    /// <typeparam name="TO">The type of the o.</typeparam>
    /// <param name="option">The option.</param>
    /// <param name="map">The map.</param>
    /// <returns></returns>
    public static Task<IResult> AsCustomActionResultAsync<TI, TO>(this OptionAsync<TI> option,
        Func<TI, TO> map) where TO : Task<IResult> =>
        option.MatchAsync(map, () => Results.NotFound());

    /// <summary>
    ///     Converts option of either to Action Result.
    /// </summary>
    /// <typeparam name="TI">The type of the i.</typeparam>
    /// <typeparam name="TO">The type of the o.</typeparam>
    /// <param name="option">The option.</param>
    /// <param name="map">The map.</param>
    /// <param name="response"></param>
    /// <returns></returns>
    public static IResult AsActionResult<TI, TO>(this Option<Either<BadRequest, TI>> option,
        Func<TI, TO> map, HttpResponse response) where TO : IResult =>
        option.Match(some => some.Match(r => map(r), l => AsBadRequest(l, response)),
            () => Results.NotFound());

    /// <summary>
    ///     Converts option of either to Action Result.
    /// </summary>
    /// <param name="option">The option.</param>
    /// <param name="response"></param>
    /// <returns></returns>
    public static IResult AsActionResult(this Option<Either<BadRequest, Unit>> option,
        HttpResponse response) =>
        option.Match(some => some.Match(r => Results.Ok(), l =>
        {
            response.Headers.Add(ServiceConstants.NotifyUserResponseHeader, new[] { bool.TrueString });
            return Results.BadRequest(l.Message);
        }), () => Results.NotFound());

    /// <summary>
    ///     Converts option of either to Action Result.
    /// </summary>
    /// <typeparam name="TI">The type of the i.</typeparam>
    /// <typeparam name="TO">The type of the o.</typeparam>
    /// <param name="option">The option.</param>
    /// <param name="map">The map.</param>
    /// <param name="response"></param>
    /// <returns></returns>
    public static Task<IResult> AsActionResult<TI, TO>(this OptionAsync<EitherAsync<BadRequest, TI>> option,
        Func<TI, TO> map, HttpResponse response) where TO : IResult =>
        option.MatchAsync(some => some
            .Match(r => map(r), l => AsBadRequest(l, response)), () => Results.NotFound());

    /// <summary>
    ///     Add specific header
    /// </summary>
    /// <param name="l"></param>
    /// <param name="response"></param>
    /// <returns></returns>
    public static IResult AsBadRequest(this BadRequest l, HttpResponse response)
    {
        response.Headers.Add(ServiceConstants.NotifyUserResponseHeader, new[] { bool.TrueString });
        return Results.BadRequest(l.Message);
    }

    /// <summary>
    ///     Converts option of either to Action Result.
    /// </summary>
    /// <param name="option">The option.</param>
    /// <param name="response"></param>
    /// <returns></returns>
    public static Task<IResult> AsActionResult(this OptionAsync<EitherAsync<BadRequest, Unit>> option,
        HttpResponse response) =>
        option.MatchAsync(some => some.Match(r => Results.Ok(), l =>
        {
            response.Headers.Add("X-Notify-User", new[] { bool.TrueString });
            return Results.BadRequest(l.Message);
        }), () => Results.NotFound());

    /// <summary>
    ///     Converts Unit to Action Result.
    /// </summary>
    /// <param name="option"></param>
    /// <returns></returns>
    public static Task<IResult> AsActionResult(this OptionAsync<Unit> option) =>
        option.Match(
            t => Results.Ok(),
            () => Results.NotFound());
}