﻿// Copyright (c) 2019 under MIT license.

#region

#endregion

namespace CV.WebApi.Extensions;

public static class ValidationToActionResult
{
    public static Task<IResult> ToActionResult<T>(
        this Validation<BadRequest, TryOptionAsync<T>> self) =>
        self.Match(
            Fail: e => Task.FromResult(
                Results.BadRequest(e)),
            Succ: valid => valid.Match(
                r => Results.Ok(r),
                () => Results.NotFound(),
                _ => Results.StatusCode(500)));
}