﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Domain;
using CV.Domain.Services;
using CV.Repository.LiteDb;
using CV.WebApi;
using CV.WebApi.Contributors;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Serilog;
using Serilog.Events;

#endregion

var configuration = new ConfigurationBuilder()
    .SetBasePath(AppContext.BaseDirectory)
    .AddJsonFile("appsettings.json")
    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json",
        true)
    .Build();

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(configuration)
    .CreateLogger();

try
{
    Log.Information($"Starting web host");
    var builder = WebApplication.CreateBuilder(args);

    builder.Host.UseSerilog((ctx, lc) => lc.ReadFrom.Configuration(ctx.Configuration));
    builder.WebHost.UseSentry();
    // Configure the shutdown to 15s
    builder.Services.Configure<HostOptions>(opts => opts.ShutdownTimeout = TimeSpan.FromSeconds(15));
    builder.AddAuthServices();
    builder.Services.AddProblemDetails();
    builder.AddOpenAPIServices();
    builder.AddSecurityServices();
    builder.Services.AddHealthChecks();
    builder.AddConfigurationOptions();
    builder.AddFluentValidationEndpointFilter();
    builder.Services.AddValidatorsFromAssemblyContaining<Program>();

    RepositoryServicesModule.ConfigureServices(builder.Services);
    DomainServicesModule.ConfigureServices(builder.Services);

    builder.Services.AddInfoContributor<GitInfoContributor>();
    // builder.Services.AddInfoContributor<EnvironmentContributor>(); DO NOT enable it

    var app = builder.Build();

    app.UseExceptionHandler(err => err.Use(ApplicationExtensions.WriteDevelopmentResponse));
    app.UseSerilogRequestLogging(opts
        =>
    {
        opts.EnrichDiagnosticContext = LogHelper.EnrichFromRequest;
        opts.GetLevel = LogHelper.GetLevel(LogEventLevel.Verbose, "Health checks");
    });
    app.UseSentryTracing();
    app.UseCors(app.Environment.IsDevelopment()
        ? WebApiConstants.DevelopmentCorsPolicy
        : WebApiConstants.ProductionCorsPolicy);

    app.UseOpenAPI();

    app.MapHealthChecks(Routes.HealthcheckRoot).WithMetadata(new AllowAnonymousAttribute());
    app.UseAuthentication();
    app.UseAuthorization();

    app.MapEndpointGroups();

    app.Run();
    return 0;
}
#pragma warning disable CA1031
catch (Exception ex)
#pragma warning restore CA1031
{
    Log.Fatal(ex, "Host terminated unexpectedly");
    return 1;
}
finally
{
    Log.CloseAndFlush();
}

#pragma warning disable CA1050 // Declare types in namespaces
public partial class Program
{
}
#pragma warning restore CA1050 // Declare types in namespaces