﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Domain.Configurations;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

#endregion

namespace CV.WebApi;

internal class JwtBearerOptionsSetup : IPostConfigureOptions<JwtBearerOptions>
{
    private readonly AuthOptions _authOptions;

    public JwtBearerOptionsSetup(AuthOptions authOptions)
    {
        _authOptions = authOptions;
    }

    public void PostConfigure(string name, JwtBearerOptions options)
    {
        options.Authority = _authOptions.Authority;
        options.Audience = _authOptions.Audience;
        options.IncludeErrorDetails = true;
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateLifetime = true,
            ValidateAudience = true,
            ValidateIssuerSigningKey = true,
            NameClaimType = ClaimTypes.NameIdentifier,
            RequireSignedTokens = true
        };
    }
}
