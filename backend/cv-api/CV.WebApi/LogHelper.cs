﻿// Copyright (c) 2019 under MIT license.

#region

using System.Linq;
using Serilog;
using Serilog.Events;

#endregion

namespace CV.WebApi;

public static class LogHelper
{
    public static void EnrichFromRequest(IDiagnosticContext diagnosticContext, HttpContext httpContext)
    {
        var request = httpContext.Request;

        // Set all the common properties available for every request
        diagnosticContext.Set("Host", request.Host);
        diagnosticContext.Set("Protocol", request.Protocol);
        diagnosticContext.Set("Scheme", request.Scheme);

        // Only set it if available
        if (request.QueryString.HasValue)
        {
            diagnosticContext.Set("QueryString", request.QueryString.Value);
        }

        // Set the content-type of the Response at this point
        diagnosticContext.Set("ContentType", httpContext.Response.ContentType);

        // Retrieve the IEndpointFeature selected for the request
        var endpoint = httpContext.GetEndpoint();
        if (endpoint != null)
        {
            diagnosticContext.Set("EndpointName", endpoint.DisplayName);
        }
    }

    public static LogEventLevel CustomGetLevel(HttpContext ctx, Exception? ex) =>
        ex != null
            ? LogEventLevel.Error
            : ctx.Response.StatusCode > 499
                ? LogEventLevel.Error
                : LogEventLevel.Debug; //Debug instead of Information

    /// <summary>
    /// Create a <see cref="Serilog.AspNetCore.RequestLoggingOptions.GetLevel"/> method that
    /// uses the default logging level, except for the specified endpoint names, which are
    /// logged using the provided <paramref name="traceLevel" />.
    /// </summary>
    /// <param name="traceLevel">The level to use for logging "trace" endpoints</param>
    /// <param name="traceEndpointNames">The display name of endpoints to be considered "trace" endpoints</param>
    /// <returns></returns>
    public static Func<HttpContext, double, Exception, LogEventLevel> GetLevel(LogEventLevel traceLevel,
        params string[] traceEndpointNames)
    {
        if (traceEndpointNames is null || traceEndpointNames.Length == 0)
        {
            throw new ArgumentNullException(nameof(traceEndpointNames));
        }

        return (ctx, _, ex) =>
            IsError(ctx, ex)
                ? LogEventLevel.Error
                : IsTraceEndpoint(ctx, traceEndpointNames)
                    ? traceLevel
                    : LogEventLevel.Information;

        static bool IsError(HttpContext ctx, Exception? ex)
            => ex != null || ctx.Response.StatusCode > 499;

        static bool IsTraceEndpoint(HttpContext ctx, string[] traceEndpoints)
        {
            var endpoint = ctx.GetEndpoint();
            return endpoint is not null && traceEndpoints.Any(t =>
                string.Equals(t, endpoint.DisplayName, StringComparison.OrdinalIgnoreCase));
        }
    }
}