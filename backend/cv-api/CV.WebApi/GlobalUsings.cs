﻿// Copyright (c) 2019 under MIT license.

global using Microsoft.AspNetCore.Builder;
global using Microsoft.Extensions.DependencyInjection;
global using System.IO;
global using Microsoft.AspNetCore.Http;
global using System;
global using Microsoft.AspNetCore.Diagnostics;
global using System.Collections.Generic;
global using System.Threading.Tasks;
global using System.Security.Claims;
global using Asp.Versioning.Builder;
global using Asp.Versioning.Conventions;
global using CV.Domain.Extensions;
global using CV.Domain.Models;
global using CV.Domain.Services.Abstractions;
global using CV.Domain.Types;
global using CV.WebApi.Constants;
global using CV.WebApi.Extensions;
global using LanguageExt;
global using LanguageExt.Common;
global using Microsoft.AspNetCore.Mvc;
global using static Microsoft.AspNetCore.Http.StatusCodes;
global using static LanguageExt.Prelude;