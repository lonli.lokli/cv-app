﻿// Copyright (c) 2019 under MIT license.

#region

using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

#endregion

namespace CV.WebApi;

/// <summary>
///     Swagger filter fot OAuth2.
/// </summary>
/// <seealso cref="IOperationFilter" />
public class SecurityRequirementsOperationFilter : IOperationFilter
{
    /// <inheritdoc />
    public void Apply(OpenApiOperation? operation, OperationFilterContext context)
    {
        if (operation == null)
        {
            return;
        }

        var oAuthRequirements = new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme { BearerFormat = "oauth2" }, new List<string>()
            }
        };
        operation.Security ??= new List<OpenApiSecurityRequirement>();
        operation.Security.Add(oAuthRequirements);
    }
}