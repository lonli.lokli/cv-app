﻿// Copyright (c) 2019 under MIT license.

using CV.Domain;

namespace Microsoft.Extensions.DependencyInjection;

public static partial class ServiceCollectionExtensions
{
    /// <summary>
    ///     Adds the standard security services.
    /// </summary>
    public static WebApplicationBuilder AddSecurityServices(this WebApplicationBuilder builder)
    {
        builder.Services.AttachAntiforgery();
        builder.Services.AddCorsPolicies();
        return builder;
    }

    private static void AddCorsPolicies(this IServiceCollection services)
    {
        string[] localHostOrigins =
        {
            "http://localhost:4200"
        };

        string[] productionHostOrigins =
        {
            "https://faircv.today"
        };

        services.AddCors(options => // CORS middleware must precede any defined endpoints
        {
            options.AddPolicy(WebApiConstants.DevelopmentCorsPolicy, builder =>
            {
                builder.WithOrigins(localHostOrigins)
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .WithExposedHeaders(ServiceConstants.NotifyUserResponseHeader);
            });
            options.AddPolicy(WebApiConstants.ProductionCorsPolicy, builder =>
            {
                builder.WithOrigins(productionHostOrigins)
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .WithExposedHeaders(ServiceConstants.NotifyUserResponseHeader);
            });
        });
    }

    private static void AttachAntiforgery(this IServiceCollection services)
    {
        services.AddAntiforgery(options => { options.HeaderName = "X-XSRF-TOKEN"; });
    }
}