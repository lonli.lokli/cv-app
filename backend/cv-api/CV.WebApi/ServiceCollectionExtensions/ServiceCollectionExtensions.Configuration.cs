﻿// Copyright (c) 2019 under MIT license.

using CV.Domain;
using CV.Domain.Configurations;
using CV.WebApi;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Microsoft.Extensions.DependencyInjection;

public static partial class ServiceCollectionExtensions
{
    public static WebApplicationBuilder AddConfigurationOptions(this WebApplicationBuilder builder)
    {
        // register validatable settings
        builder.Services.AddSetting<LiteDbConfiguration>(
            builder.Configuration.GetSection(WebApiConstants.LITEDB_CONFIGURATION_PREFIX));
        builder.Services.AddSetting<HashingOptions>(
            builder.Configuration.GetSection(HashingConstants.CONFIGURATION_PREFIX));
        builder.Services.AddSetting<SecurityOptions>(
            builder.Configuration.GetSection(WebApiConstants.SECURITY_CONFIGURATION_PREFIX));
        builder.Services.AddSetting<AuthOptions>(
            builder.Configuration.GetSection(WebApiConstants.AUTH_CONFIGURATION_PREFIX));

        builder.Services.AddSettingsValidatorActuator();

        return builder;
    }

    /// <summary>
    ///     Register a validatable setting/>
    /// </summary>
    /// <typeparam name="TValidatableSettings">Validatable setting type</typeparam>
    /// <param name="services">Service collection.</param>
    /// <param name="section">Configuration section.</param>
    /// <returns></returns>
    private static IServiceCollection AddSetting<TValidatableSettings>(
        this IServiceCollection services,
        IConfigurationSection section)
        where TValidatableSettings : class, IValidatable, new()
    {
        if (section == null)
        {
            throw new ArgumentNullException(nameof(section),
                "Cannot load configuration for " + typeof(TValidatableSettings).Name + ".");
        }

        services.Configure<TValidatableSettings>(section);
        services.AddSingleton(resolver => resolver.GetRequiredService<IOptions<TValidatableSettings>>()
            .Value);
        services.AddSingleton(resolver =>
            (IValidatable)resolver.GetRequiredService<IOptions<TValidatableSettings>>()
                .Value);
        return services;
    }

    /// <summary>
    ///     Register validator class to validate all validatable objects on application startup.
    /// </summary>
    /// <param name="services">The services.</param>
    /// <returns></returns>
    private static IServiceCollection AddSettingsValidatorActuator(
        this IServiceCollection services)
    {
        services.AddTransient<IStartupFilter, ConfigurationValidationStartupFilter>();
        return services;
    }
}