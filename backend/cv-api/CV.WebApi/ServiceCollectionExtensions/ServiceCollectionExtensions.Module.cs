﻿// Copyright (c) 2019 under MIT license.

using System.Diagnostics.CodeAnalysis;
using CV.WebApi;

namespace Microsoft.Extensions.DependencyInjection;

public static partial class ServiceCollectionExtensions
{
    public static WebApplication MapEndpointGroups(this WebApplication app)
    {
        var groups = DiscoverGroups();
        foreach (var group in groups)
        {
            group.GroupEndpoints(app.MapGroup(group.Prefix).AddFluentValidationFilter());
        }

        return app;
    }

    [RequiresUnreferencedCode("Calls System.Reflection.Assembly.GetTypes()")]
    private static IEnumerable<IMinimalApiGroup> DiscoverGroups()
    {
        return typeof(IMinimalApiGroup).Assembly
            .GetTypes()
            .Where(p => p.IsClass && p.IsAssignableTo(typeof(IMinimalApiGroup)))
            .Select(Activator.CreateInstance)
            .Cast<IMinimalApiGroup>();
    }
}