﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Domain;
using Microsoft.AspNetCore.Hosting;

#endregion

namespace CV.WebApi;

/// <summary>
///     Class to validate validatable settings on application startup
/// </summary>
public class ConfigurationValidationStartupFilter : IStartupFilter
{
    private readonly IEnumerable<IValidatable> _validatableObjects;

    /// <summary>
    ///     Initializes a new instance of the <see cref="ConfigurationValidationStartupFilter" /> class.
    /// </summary>
    /// <param name="validatableObjects">The validatable objects.</param>
    public ConfigurationValidationStartupFilter(IEnumerable<IValidatable> validatableObjects)
    {
        _validatableObjects = validatableObjects;
    }

    /// <inheritdoc />
    public Action<IApplicationBuilder> Configure(
        Action<IApplicationBuilder> next)
    {
        foreach (var validatableObject in _validatableObjects)
        {
            validatableObject.Validate();
        }

        return next;
    }
}