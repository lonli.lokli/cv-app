﻿// Copyright (c) 2019 under MIT license.

namespace CV.WebApi.Constants;

public static class Routes
{
    public const string EmptyEndpointRoute = "";
    public const string DocumentsRoot = "api/Documents";
    public const string JobsRoot = "api/Jobs";
    public const string InfoRoot = "Info";
    public const string HealthcheckRoot = "/healthcheck";
}