﻿// Copyright (c) 2019 under MIT license.

namespace CV.WebApi.Constants;

public static class ContentTypeConstants
{
    /// <summary>MIME Webform; Defined in RFC 2388</summary>
    public const string MultipartFormData = "multipart/form-data";
}