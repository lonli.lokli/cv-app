﻿// Copyright (c) 2019 under MIT license.

namespace CV.WebApi.Constants;

/// <summary>
///     Global Service constants.
/// </summary>
public static class ServiceConstants
{
    /// <summary>
    ///     Header in response, indicating whether message should be displayed for user,
    /// </summary>
    public const string NotifyUserResponseHeader = "X-Notify-User";

    /// <summary>
    ///     Title in the Error response object.
    /// </summary>
    public const string ErrorResponseTitle = "An application error occurred.";
}