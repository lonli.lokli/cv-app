﻿// Copyright (c) 2019 under MIT license.

#region

using System.Linq;
using Microsoft.AspNetCore.Authorization;

#endregion

namespace CV.WebApi;

/// <summary>
///     Application Roles
/// </summary>
[Flags]
public enum Roles
{
    /// <summary>
    ///     Empty role.
    /// </summary>
    None = 0,

    /// <summary>
    ///     Full Access for all modules.
    /// </summary>
    View = 1,

    /// <summary>
    ///     Usual user
    /// </summary>
    User = 2,

    /// <summary>
    ///     Readonly mode.
    /// </summary>
    Admin = 4,

    /// <summary>
    ///     All roles except None.
    /// </summary>
    All = Admin | User | View
}

public sealed class CvAuthorizeAttribute : AuthorizeAttribute
{
    /// <summary>Initializes a new instance of the SavoAuthorizeAttribute class.</summary>
    /// <param name="roles">The roles.</param>
    public CvAuthorizeAttribute(Roles roles)
    {
        var values = Enum.GetValues(typeof(Roles))
            .Cast<Roles>()
            .Where(r => r != WebApi.Roles.None && roles.HasFlag(r))
            .Select(r => r.ToString())
            .ToArray();

        Roles = string.Join(",", values);
    }
}