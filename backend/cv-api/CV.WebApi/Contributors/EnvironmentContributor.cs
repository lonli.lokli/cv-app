﻿// Copyright (c) 2019 under MIT license.

#region

using System.Collections;
using System.Linq;

#endregion

namespace CV.WebApi.Contributors;

public class EnvironmentContributor : IInfoContributor
{
    public void Contribute(IInfoBuilder builder)
    {
        builder.WithInfo(CreateDictionary());
    }

    protected virtual IDictionary<string, object?> CreateDictionary()
    {
        return new Dictionary<string, object?>(Environment.GetEnvironmentVariables()
            .Cast<DictionaryEntry>()
            .Select(e => new KeyValuePair<string, object?>(e.Key.ToString() ?? $"EMPTY_{Guid.NewGuid()}", e.Value)), StringComparer.Ordinal);
    }
}