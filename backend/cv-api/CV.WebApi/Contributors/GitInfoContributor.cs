﻿// Copyright (c) 2019 under MIT license.

namespace CV.WebApi.Contributors;

public class GitInfoContributor : IInfoContributor
{
    public void Contribute(IInfoBuilder builder)
    {
        builder.WithInfo("Build #",
            $"{ThisAssembly.Git.Commits} ({ThisAssembly.Git.Branch}+{ThisAssembly.Git.Commit})");
    }
}