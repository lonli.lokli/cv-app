﻿// Copyright (c) 2019 under MIT license.

#region

#endregion

namespace CV.WebApi.Contributors;

public class InfoBuilder : IInfoBuilder
{
    private readonly Dictionary<string, object?> _info = new(System.StringComparer.Ordinal);

    public IDictionary<string, object?> Build()
    {
        return _info;
    }

    public IInfoBuilder WithInfo(string key, object? value)
    {
        if (!string.IsNullOrEmpty(key))
        {
            _info[key] = value;
        }

        return this;
    }

    public IInfoBuilder WithInfo(IDictionary<string, object?> details)
    {
        foreach (var (key, value) in details)
        {
            _info[key] = value;
        }

        return this;
    }
}