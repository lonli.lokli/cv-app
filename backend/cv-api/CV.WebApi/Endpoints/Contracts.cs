﻿// Copyright (c) 2019 under MIT license.

using System.ComponentModel.DataAnnotations;

namespace CV.WebApi.Endpoints;

public record struct DocumentContract([property: Required] string Author, [property: Required] string Id);

public record struct JobContract([property: Required] string Id, [property: Required] SalaryRangeContract Salary,
    [property: Required] string Occupation, [property: Required] CompanyContract Company,
    [property: Required] ContactContract Contact, [property: Required] string[] features);

public record struct SalaryRangeContract([property: Required] int Min, [property: Required] int Max);

public record struct CompanyContract([property: Required] string Name, [property: Required] string Country,
    [property: Required] string Town);

public record struct ContactContract([property: Required] string Email, [property: Required] string Phone);

public record struct CreateJobContract([property: Required] int MinSalary, [property: Required] int MaxSalary,
    [property: Required] string Occupation, [property: Required] string[] RequiredSkills,
    string[] OptionalSkills);