﻿// Copyright (c) 2019 under MIT license.

using CV.WebApi.Contributors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Routing;

namespace CV.WebApi.Endpoints.Info;

public static class GetInfo
{
    public static RouteGroupBuilder MapGetInfo(this RouteGroupBuilder app)
    {
        app.MapGet(Routes.EmptyEndpointRoute,
                [AllowAnonymous] (IEnumerable<IInfoContributor> contributors) =>
                {
                    IInfoBuilder builder = new InfoBuilder();
                    foreach (var contributor in contributors)
                    {
                        try
                        {
                            contributor.Contribute(builder);
                        }
#pragma warning disable CA1031
                        catch (Exception e)
#pragma warning restore CA1031
                        {
                            // TODO: restore loggin
                            // _logger?.LogError("Exception: {0}", e);
                        }
                    }

                    return Results.Ok(builder.Build());
                })
            .Produces<Dictionary<string, object>>()
            .WithApiVersionSet(app.NewApiVersionSet("Info")
                .HasApiVersion(1.0)
                .Build());
        return app;
    }
}