﻿// Copyright (c) 2019 under MIT license.

using Microsoft.AspNetCore.Routing;

namespace CV.WebApi.Endpoints.Info;

public class InfoGroup : IMinimalApiGroup
{
    public string Prefix => Routes.InfoRoot;

    public RouteGroupBuilder GroupEndpoints(RouteGroupBuilder app) => app.MapGetInfo();
}