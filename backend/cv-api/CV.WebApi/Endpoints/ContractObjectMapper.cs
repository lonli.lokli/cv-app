﻿// Copyright (c) 2019 under MIT license.

#region

#endregion

namespace CV.WebApi.Endpoints;

public static class ContractObjectMapper
{
    public static DocumentContract ToContract(this Document document) => new(document.Author, document.Id);

    public static JobContract ToContract(this Job job) => new(job.Id,
        new SalaryRangeContract(job.Salary.Min, job.Salary.Max), job.Occupation,
        new CompanyContract(job.Company.Name, job.Place.Country, job.Place.Town),
        new ContactContract(job.Contact.Email, job.Contact.Phone), CollectEnums(job.Features));

    public static JobContract ToDomain(this Job job) => new(job.Id,
        new SalaryRangeContract(job.Salary.Min, job.Salary.Max), job.Occupation,
        new CompanyContract(job.Company.Name, job.Place.Country, job.Place.Town),
        new ContactContract(job.Contact.Email, job.Contact.Phone), CollectEnums(job.Features));

    private static string[] CollectEnums(JobFeatures features) => new[]
        {
            features.Remote.ToString(), features.WorkAgreement.ToString()
        };
}
