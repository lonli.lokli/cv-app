﻿// Copyright (c) 2019 under MIT license.

using CV.Repository.Abstractions;
using Microsoft.AspNetCore.Routing;
using static CV.WebApi.Extensions.UrlExtensions;

namespace CV.WebApi.Endpoints.Documents;

public static class GetDocument
{
    public static RouteGroupBuilder MapGetDocument(this RouteGroupBuilder app)
    {
        app.MapGet("/{documentId}",
                [CvAuthorize(Roles.All)] (string documentId, IDocumentRepository documentRepository) => documentRepository
                    .GetById(documentId)
                    .AsActionResult(document => Results.File(document.Data,
                        document.Name.GetMimeTypeForFileExtension(), document.Name)))
            .Produces<FileContentResult>()
            .Produces(Status404NotFound)
            .WithApiVersionSet(app.NewApiVersionSet("Documents")
                .HasApiVersion(1.0)
                .ReportApiVersions()
                .Build());
        return app;
    }
}