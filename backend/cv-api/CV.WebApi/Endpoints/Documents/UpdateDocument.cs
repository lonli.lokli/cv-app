﻿// Copyright (c) 2019 under MIT license.

using System.Linq;
using Microsoft.AspNetCore.Routing;
using static CV.WebApi.Extensions.UrlExtensions;

namespace CV.WebApi.Endpoints.Documents;

public static class UpdateDocument
{
    public static RouteGroupBuilder MapUpdateDocument(this RouteGroupBuilder app)
    {
        app.MapPost("/{documentId}",
                [CvAuthorize(Roles.User | Roles.Admin)]
                async (string documentId, IDocumentService documentService, HttpRequest req, ClaimsPrincipal principal,
                    HttpResponse response) => await LoadDocumentsFromStream(req, principal.GetUserName())
                    .Bind(ValidateDocuments)
                    .Bind(document =>
                        UpdateDocumentInDatabase(documentService, documentId, document, principal.GetUserName()))
                    .AsActionResult(response))
            .Accepts<FileContentResult>(ContentTypeConstants.MultipartFormData)
            .Produces(Status200OK)
            .Produces(Status400BadRequest)
            .Produces(Status404NotFound)
            .WithApiVersionSet(app.NewApiVersionSet("Documents")
                .HasApiVersion(1.0)
                .ReportApiVersions()
                .Build());
        return app;
    }

    private static EitherAsync<BadRequest, IList<Document>> LoadDocumentsFromStream(HttpRequest request,
        string user) => LoadDocuments(request, user)
        .MapLeft(error => BadRequest.Create(error.Message));

    private static EitherAsync<BadRequest, Document> ValidateDocuments(IList<Document> documents) =>
        EnsureListHasOnlyOneElement(documents)
            .ToEither()
            .ToAsync()
            .MapLeft(seq => seq.Aggregate(BadRequest.Join));

    private static EitherAsync<BadRequest, OptionAsync<Unit>> UpdateDocumentInDatabase(IDocumentService documentService, string documentId, Document document,
        string user) => documentService.Update(documentId, document, user)
        .Traverse(r => r);

    private static Validation<BadRequest, T> EnsureListHasOnlyOneElement<T>(IList<T> items) =>
        items.Count == 1
            ? Success<BadRequest, T>(items.Single())
            : Fail<BadRequest, T>(BadRequest.Create(ErrorMessageConstants.OnlyOneFileAllowed));

    private static EitherAsync<Error, IList<Document>> LoadDocuments(HttpRequest request, string user) =>
        TryAsync(() => request.ReadFormAsync(request.HttpContext.RequestAborted))
            .ToEither()
            .MapAsync(
                async formFiles =>
                {
                    IList<Document> documents = new List<Document>();
                    foreach (var file in formFiles.Files)
                    {
                        var content = await file.GetContent(request.HttpContext.RequestAborted);
                        documents.Add(new Document(file.FileName, user, content));
                    }

                    return documents;
                });
}