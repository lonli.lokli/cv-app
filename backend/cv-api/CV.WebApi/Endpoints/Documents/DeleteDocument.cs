﻿// Copyright (c) 2019 under MIT license.

using Microsoft.AspNetCore.Routing;
using static CV.WebApi.Extensions.UrlExtensions;

namespace CV.WebApi.Endpoints.Documents;

public static class DeleteDocument
{
    public static RouteGroupBuilder MapDeleteDocument(this RouteGroupBuilder app)
    {
        app.MapDelete("/{documentId}",
                [CvAuthorize(Roles.All)] (string documentId, ClaimsPrincipal principal, HttpResponse response,
                        IDocumentService documentService) =>
                    documentService.Delete(documentId, principal.GetUserName())
                        .AsActionResult(response))
            .Produces(Status200OK)
            .Produces(Status400BadRequest)
            .Produces(Status404NotFound)
            .WithApiVersionSet(app.NewApiVersionSet("Documents")
                .HasApiVersion(1.0)
                .ReportApiVersions()
                .Build());
        return app;
    }
}