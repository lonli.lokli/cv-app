﻿// Copyright (c) 2019 under MIT license.

using CV.Repository.Abstractions;
using Microsoft.AspNetCore.Routing;

namespace CV.WebApi.Endpoints.Documents;

public static class AddDocument
{
    public static RouteGroupBuilder MapAddDocument(this RouteGroupBuilder app)
    {
        app.MapPut(Routes.EmptyEndpointRoute,
                [CvAuthorize(Roles.All)] (IDocumentRepository documentRepository, ClaimsPrincipal principal,
                    HttpRequest request,
                    HttpResponse response) => LoadDocuments(request, principal.GetUserName())
                    .MapLeft(error => BadRequest.Create(error.Message))
                    .Bind(documents => EnsureListHasSomeElements(documents)
                        .ToEither()
                        .ToAsync()
                        .MapLeft(seq => seq.Aggregate(BadRequest.Join)))
                    .Bind(documentRepository.Save)
                    .AsActionResult(response))
            .Accepts<FileContentResult>(ContentTypeConstants.MultipartFormData)
            .Produces(Status200OK)
            .Produces(Status400BadRequest)
            .WithApiVersionSet(app.NewApiVersionSet("Documents")
                .HasApiVersion(1.0)
                .ReportApiVersions()
                .Build());

        return app;
    }

    private static Validation<BadRequest, Seq<T>> EnsureListHasSomeElements<T>(Seq<T> items) =>
        items.Count != 0
            ? Success<BadRequest, Seq<T>>(items)
            : Fail<BadRequest, Seq<T>>(BadRequest.Create(ErrorMessageConstants.NoFilesAttached));

    private static EitherAsync<Error, Seq<Document>> LoadDocuments(HttpRequest request, string user) =>
        TryAsync(() => request.ReadFormAsync(request.HttpContext.RequestAborted))
            .ToEither()
            .MapAsync(
                async formFiles =>
                {
                    IList<Document> documents = new List<Document>();
                    foreach (var file in formFiles.Files)
                    {
                        var content = await file.GetContent(request.HttpContext.RequestAborted);
                        documents.Add(new Document(file.FileName, user, content));
                    }

                    return Seq(documents);
                });
}
