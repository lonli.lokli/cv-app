﻿// Copyright (c) 2019 under MIT license.

using CV.Repository.Abstractions;
using Microsoft.AspNetCore.Routing;

namespace CV.WebApi.Endpoints.Documents;

public static class GetDocuments
{
    public static RouteGroupBuilder MapGetDocuments(this RouteGroupBuilder app)
    {
        app.MapGet(Routes.EmptyEndpointRoute,
                [CvAuthorize(Roles.All)] (IDocumentRepository documentRepository) => documentRepository.GetAll()
                    .Select(e => e.ToContract())
                    .AsActionResult())
            .Produces<IReadOnlyCollection<DocumentContract>>()
            .WithApiVersionSet(app.NewApiVersionSet("Documents")
                .HasApiVersion(1.0)
                .ReportApiVersions()
                .Build());
        return app;
    }
}