﻿// Copyright (c) 2019 under MIT license.

using Microsoft.AspNetCore.Routing;

namespace CV.WebApi.Endpoints.Documents;

public class DocumentsGroup : IMinimalApiGroup
{
    public string Prefix => Routes.DocumentsRoot;

    public RouteGroupBuilder GroupEndpoints(RouteGroupBuilder app) =>
        app.MapUpdateDocument()
            .MapGetDocuments()
            .MapGetDocument()
            .MapAddDocument()
            .MapDeleteDocument();
}