﻿// Copyright (c) 2019 under MIT license.

using Microsoft.AspNetCore.Routing;

namespace CV.WebApi.Endpoints.Jobs;

public static class GetJobs
{
    public static RouteGroupBuilder MapGetJobs(this RouteGroupBuilder app)
    {
        app.MapGet(Routes.EmptyEndpointRoute,
                [CvAuthorize(Roles.All)] (IJobService jobService) => jobService.GetAll()
                    .Select(e => e.ToContract())
                    .AsActionResult())
            .Produces<IReadOnlyCollection<JobContract>>()
            .WithApiVersionSet(app.NewApiVersionSet("Jobs")
                .HasApiVersion(1.0)
                .Build());

        return app;
    }
}