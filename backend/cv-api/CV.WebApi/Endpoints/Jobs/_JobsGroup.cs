﻿// Copyright (c) 2019 under MIT license.

using Microsoft.AspNetCore.Routing;

namespace CV.WebApi.Endpoints.Jobs;

public class JobsGroup : IMinimalApiGroup
{
    public string Prefix => Routes.JobsRoot;

    public RouteGroupBuilder GroupEndpoints(RouteGroupBuilder app) => app.MapGetJobs();
}