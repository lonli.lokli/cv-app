﻿// Copyright (c) 2019 under MIT license.

using FluentValidation;

namespace CV.WebApi.Endpoints.Validators;

public class CreateJobValidator : AbstractValidator<CreateJobContract>
{
    public CreateJobValidator()
    {
        RuleFor(request => request.MinSalary)
            .GreaterThan(0);
        RuleFor(request => request.MaxSalary)
            .GreaterThan(0);
    }
}