﻿// Copyright (c) 2019 under MIT license.

using Microsoft.AspNetCore.Routing;

namespace CV.WebApi;

/// <summary>
/// Base interface for Minimal API Modules
/// </summary>
internal interface IMinimalApiGroup
{
    /// <summary>
    /// Prefix for the whole group
    /// </summary>
    public string Prefix { get; }

    /// <summary>
    /// Maps endpoints for Versioned API.
    /// </summary>
    /// <param name="app"></param>
    RouteGroupBuilder GroupEndpoints(RouteGroupBuilder app);
}