﻿// Copyright (c) 2019 under MIT license.

#region

using System.Linq;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

#endregion

namespace CV.WebApi;

/// <summary>
///     Represents the Swagger/Swashbuckle operation filter used to document the implicit API version parameter.
/// </summary>
/// <remarks>
///     This <see cref="IOperationFilter" /> is only required due to bugs in the <see cref="SwaggerGenerator" />.
///     Once they are fixed and published, this class can be removed.
/// </remarks>
public class SwaggerDefaultValues : IOperationFilter
{
    /// <summary>
    ///     Applies the filter to the specified operation using the given context.
    /// </summary>
    /// <param name="operation">The operation to apply the filter to.</param>
    /// <param name="context">The current operation filter context.</param>
    public void Apply(OpenApiOperation? operation, OperationFilterContext? context)
    {
        if (operation == null || context == null)
        {
            return;
        }

        var apiDescription = context.ApiDescription;

        operation.Deprecated |= apiDescription.IsDeprecated();
        operation.OperationId ??= string.Join(string.Empty, (new string[] { apiDescription.HttpMethod ?? string.Empty })
            .Concat((apiDescription.RelativePath ?? string.Empty).Split(@"/"))
            .Select(word => string.Join(string.Empty, word.Split('{', '}')))
            .Select(FirstCharToUpper));

        if (operation.Parameters == null)
        {
            return;
        }

        // REF: https://github.com/domaindrivendev/Swashbuckle.AspNetCore/issues/412
        // REF: https://github.com/domaindrivendev/Swashbuckle.AspNetCore/pull/413
        foreach (var parameter in operation.Parameters)
        {
            var description = apiDescription.ParameterDescriptions.First(p => p.Name == parameter.Name);

            parameter.Description ??= description.ModelMetadata?.Description;

            // parameter.Schema.Default = description.DefaultValue;

            parameter.Required |= description.IsRequired;
        }
    }

    public static string FirstCharToUpper(string input) =>
        input switch
        {
            null => string.Empty,
            "" => string.Empty,
            _ => string.Concat(input[0]
                .ToString()
                .ToUpperInvariant(), input.AsSpan(1))
        };
}