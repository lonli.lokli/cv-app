# Fair CV Backend
This project contains backend source code for 'Fair CV' application.

## Table Of Content
- [Prerequisites](#prerequisites)
- [Running Locally](#running-locally)
- [Project Structure](#project-structure)
- [License](#license)

<a name="prerequisites"></a>
## Prerequisites
* [.NET 6 SDK](https://dotnet.microsoft.com/en-us/download/dotnet/6.0) installed

<a name="running-locally"></a>
## Running application locally
All the settings are stored either in app settings or in local User Secrets.
Those kind of nodes should be setup in User Secrets
* Database settings (`Password` must be more than `1` symbols)
```json
"LiteDb": {
  "DbPath": "d:\\somefile.db",
  "Password": "12345678987654321"
}
```
* Security settings (`Secret` must be more than `20` symbols)
```json
"Security": {
  "Secret": "abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc"
},
```
* Authentication settings
```json
"Auth": {
    "Authority": "https://faircv.eu.auth0.com",
    "Audience": "https://api.faircv.today",
    "ClientId": "{REDUCTED}",
    "ClientSecret": "{REDUCTED}"
}
```
Please note that in case of running locally you can follow an advice [here](https://auth0.com/docs/quickstart/webapp/aspnet-core) on how to setup Auth0 for .NET
Ensure that UI and API share the same Audience and Authority.

<a name="project-structure"></a>
## Project Structure

|           Folder Name           | Description                                       |
|---------------------------------|---------------------------------------------------|
| CV.Core                         | Extension methods                                 |
| CV.Domain                       | Domain objects, constants, configuration settings |
| CD.Domain.Services              | Service layer                                     |
| CV.Domain.Services.Abstractions | Abstraction over Service Layer                    |
| CV.Domain.Services.Tests        | Tests for Service Layer                           |
| CV.Domain.Tests                 | Tests for Domain                                  |
| CV.Repository.Abstractions      | Abstraction over Repository Level                 |
| CV.Repository.LiteDb            | LiteDb implementation of Repository Level         |
| CV.Repository.LiteDb.Tests      | Tests for LiteDb Repositories                     |
| CV.WebApi                       | Web API                                           |
| CV.WebApi.Tests                 | Tests for Web API Layer                           |
| scripts                         | Docker scripts for deployment image               |


<a name="license"></a>
## License
MIT