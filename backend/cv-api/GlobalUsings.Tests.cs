﻿// Copyright (c) 2019 under MIT license.

global using Microsoft.Extensions.DependencyInjection;
global using System.IO;
global using System;
global using System.Collections.Generic;
global using System.Threading.Tasks;
global using System.Security.Claims;
global using LanguageExt;
global using LanguageExt.Common;
global using static LanguageExt.Prelude;