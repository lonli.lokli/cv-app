﻿// Copyright (c) 2019 under MIT license.

#region

using System.Text;
using CV.Domain.Extensions;
using CV.Domain.Models;
using CV.Repository.LiteDb.Repositories;
using FluentAssertions;
using LanguageExt.UnitTesting;
using Xunit;

#endregion

namespace CV.Repository.LiteDb.Tests;

public class DocumentRepositoryTests
{
    [Fact]
    public async Task Insert_Test()
    {
        // Arrange
        var newItems = Seq(new Document[]
        {
            new("testDocument1", "testAuthor1", Encoding.UTF8.GetBytes("testContent1")),
            new("testDocument2", "testAuthor2", Encoding.UTF8.GetBytes("testContent2"))
        });
        var tc = CreateTestCandidate();

        // Act
        await tc.Save(newItems);

        // Assert
        await tc.GetAll()
            .ShouldBeRight(r => r.Length.Should()
                .Be(2));
        await tc.GetAll()
            .ShouldBeRight(seq => seq.Should()
                .Contain(i => i.Name == "testDocument1"));
        await tc.GetAll()
            .ShouldBeRight(seq => seq.Should()
                .Contain(i => i.Author == "testAuthor1"));
        await tc.GetAll()
            .ShouldBeRight(seq => seq.Should()
                .Contain(i => i.Data.ByteArrayCompare(Encoding.UTF8.GetBytes("testContent1"))));
    }

    [Fact]
    public async Task Delete_Test()
    {
        // Arrange
        var newItems = Seq(new Document[]
        {
            new("testDocument1", "testAuthor1", Encoding.UTF8.GetBytes("testContent1")),
            new("testDocument2", "testAuthor2", Encoding.UTF8.GetBytes("testContent2"))
        });
        var tc = CreateTestCandidate();

        // Act / Assert
        await tc.Save(newItems);
        await tc.GetAll()
            .ShouldBeRight(r => r.Length.Should()
                .Be(2));
        await tc.Delete(newItems[0].Id);

        // Assert
        await tc.GetAll()
            .ShouldBeRight(r => r.Length.Should()
                .Be(1));
        await tc.GetAll()
            .ShouldBeRight(seq => seq.Should()
                .Contain(i => i.Name == "testDocument2"));
        await tc.GetAll()
            .ShouldBeRight(seq => seq.Should()
                .Contain(i => i.Author == "testAuthor2"));
        await tc.GetAll()
            .ShouldBeRight(seq => seq.Should()
                .Contain(i => i.Data.ByteArrayCompare(Encoding.UTF8.GetBytes("testContent2"))));
    }

    [Fact]
    public async Task Update_Test()
    {
        // Arrange
        var newItems = Seq(new Document[]
        {
            new("testDocument1", "testAuthor1", Encoding.UTF8.GetBytes("testContent1"))
        });
        var tc = CreateTestCandidate();

        // Act / Assert
        await tc.Save(newItems);
        await tc.GetAll()
            .ShouldBeRight(r => r.Length.Should()
                .Be(1));
        await tc.Update(newItems[0]
            .Id, new Document("any", "newAuthor", Encoding.UTF8.GetBytes("newContent")));

        // Assert
        var current = await tc.GetAll();
        await tc.GetAll()
            .ShouldBeRight(r => r.Length.Should()
                .Be(1));
        await tc.GetAll()
            .ShouldBeRight(seq => seq.Should()
                .Contain(i => i.Name == "any"));
        await tc.GetAll()
            .ShouldBeRight(seq => seq.Should()
                .Contain(i => i.Author == "newAuthor"));
        await tc.GetAll()
            .ShouldBeRight(seq => seq.Should()
                .Contain(i => i.Data.ByteArrayCompare(Encoding.UTF8.GetBytes("newContent"))));
    }

    private static DocumentRepository CreateTestCandidate()
    {
        return new DocumentRepository(new TestDbFactory());
    }
}