﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Repository.LiteDb.Repositories;
using LiteDB;

#endregion

namespace CV.Repository.LiteDb.Tests;

public sealed class TestDbFactory : IDbFactory, IDisposable
{
    private readonly MemoryStream _stream = new();

    public LiteDatabase Create()
    {
        return new LiteDatabase(_stream, new BsonMapper());
    }

    public void Dispose()
    {
        _stream.Dispose();
    }
}