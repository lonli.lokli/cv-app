﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Models;
using CV.Domain.Services.Abstractions;
using CV.Domain.Types;
using CV.Repository.Abstractions;
using LanguageExt;

namespace CV.Domain.Services;

internal class JobService : IJobService
{
    private readonly IJobRepository _jobRepository;

    public JobService(IJobRepository jobRepository)
    {
        _jobRepository = jobRepository;
    }

    public EitherAsync<BadRequest, Seq<Job>> GetAll() => _jobRepository.GetAll();
}