﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Domain.Types;
using LanguageExt;

#endregion

namespace CV.Domain.Services;

public static class Helpers
{
    public static OptionAsync<EitherAsync<BadRequest, T>> CollectErrors<T>(
        this OptionAsync<Validation<BadRequest, T>> item) =>
        item.Map(v => v.ToEither()
            .ToAsync()
            .MapLeft(seq => seq.Aggregate(BadRequest.Join)));
}