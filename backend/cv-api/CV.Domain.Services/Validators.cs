﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Domain.Models;
using CV.Domain.Types;
using LanguageExt;
using static LanguageExt.Prelude;

#endregion

namespace CV.Domain.Services;

public static class Validators
{
    public static Validation<BadRequest, Document> EnsureAuthorIsSame(Document document, string author) =>
        document.Author == author
            ? Success<BadRequest, Document>(document)
            : Fail<BadRequest, Document>(BadRequest.Create("Action is allowed only for document author"));
}