﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Domain.Services.Abstractions;
using Microsoft.Extensions.DependencyInjection;

#endregion

namespace CV.Domain.Services;

/// <summary>
///     Services module for Domain layer.
/// </summary>
public class DomainServicesModule
{
    public static void ConfigureServices(IServiceCollection services)
    {
        services.AddSingleton<IDocumentService, DocumentService>();
        services.AddSingleton<IJobService, JobService>();
    }
}