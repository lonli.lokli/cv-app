﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Domain.Models;
using CV.Domain.Services.Abstractions;
using CV.Domain.Types;
using CV.Repository.Abstractions;
using LanguageExt;
using static LanguageExt.Prelude;
using static CV.Domain.Services.Validators;

#endregion

namespace CV.Domain.Services;

/// <inheritdoc />
internal class DocumentService : IDocumentService
{
    private readonly IDocumentRepository _repository;

    /// <inheritdoc />
    public DocumentService(IDocumentRepository repository) => _repository = repository;

    /// <inheritdoc />
    public OptionAsync<EitherAsync<BadRequest, Unit>> Update(string documentId, Document document, string user) =>
        _repository.GetById(documentId)
            .Map(doc => EnsureAuthorIsSame(doc, user))
            .Map(validation => validation.ToEither()
                .ToAsync()
                .MapLeft(seq => seq.Aggregate(BadRequest.Join)))
            .BindT(_ => _repository.Update(documentId, document))
            .BindT(updateSucceed => updateSucceed
                ? RightAsync<BadRequest, Unit>(Unit.Default)
                : LeftAsync<BadRequest, Unit>(BadRequest.Create("Db was not able to update the document")));

    /// <inheritdoc />
    public OptionAsync<EitherAsync<BadRequest, Unit>> Delete(string documentId, string user) =>
        _repository.GetById(documentId)
            .Map(doc => EnsureAuthorIsSame(doc, user))
            .Map(validation => validation.ToEither()
                .ToAsync()
                .MapLeft(seq => seq.Aggregate(BadRequest.Join)))
            .BindT(_ => _repository.Delete(documentId))
            .BindT(deleteSucceed => deleteSucceed
                ? RightAsync<BadRequest, Unit>(Unit.Default)
                : LeftAsync<BadRequest, Unit>(BadRequest.Create("Db was not able to delete the document")));
}
