﻿// Copyright (c) 2019 under MIT license.

#region

using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using FluentAssertions;
using ImmediateReflection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.ObjectPool;
using Xunit;
using Array = System.Array;

#endregion

namespace CV.WebApi.Tests.Authorization;

/// <summary>
///     Test suite for WebApi controllers.
/// </summary>
public class AuthorizationMinimalApiTests
{
    /// <summary>
    ///     Ensures that all minimal API endpoints have CvAuthorize or AllowAnonymous attribute.
    /// </summary>
    [Fact]
    public void Missed_Authorize_Attribute_On_MinimalAPI_Test()
    {
        var types = DiscoverGroups();
        var application = new WebApplicationFactory<Program>()
            .WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                    {
                        services.AddSingleton<ILoggerFactory, LoggerFactory>();
                        services.AddSingleton<ObjectPoolProvider, DefaultObjectPoolProvider>();
                        services.AddSingleton<IHostEnvironment>(provider => new HostingEnvironment());
                        services.AddSingleton<DiagnosticSource>(new DiagnosticListener("test"));
                    })
                    .ConfigureAppConfiguration(cfgBuilder =>
                        cfgBuilder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.test.json")))
                    .UseEnvironment(Environments.Development);
            });

        var endpointBuilder = new TestEndpointRouteBuilder(application.Services);
        foreach (var group in types)
        {
            group.GroupEndpoints(endpointBuilder.MapGroup(group.Prefix));
        }

        endpointBuilder.DataSources.Count.Should().BePositive();
        endpointBuilder.DataSources.First()
            .Endpoints.Count.Should()
            .BePositive();

        var unprotectedEndpoints = endpointBuilder.DataSources.First()
            .Endpoints
                // either CvAuthorizeAttribute or AllowAnonymousAttribute should be defined for all endpoints
            .Where(endpoint => endpoint.Metadata.GetMetadata<CvAuthorizeAttribute>() == null && endpoint.Metadata.GetMetadata<AllowAnonymousAttribute>() == null)
            .ToArray();

        unprotectedEndpoints.Length.Should()
            .Be(0, string.Join(Environment.NewLine, unprotectedEndpoints.Select(endpoint => endpoint.DisplayName)));
    }

    private static ICollection<IMinimalApiGroup> DiscoverGroups()
    {
        return typeof(IMinimalApiGroup).Assembly
            .GetTypes()
            .Where(p => p.IsClass && p.IsAssignableTo(typeof(IMinimalApiGroup)))
            .Select(Activator.CreateInstance)
            .Cast<IMinimalApiGroup>()
            .ToArray();
    }
}

internal class TestEndpointRouteBuilder : IEndpointRouteBuilder
{
    public IApplicationBuilder CreateApplicationBuilder() => throw new NotImplementedException();

    public TestEndpointRouteBuilder(IServiceProvider serviceProvider)
    {
        ServiceProvider = serviceProvider;
        DataSources = new List<EndpointDataSource>();
    }

    public IServiceProvider ServiceProvider { get; }

    public ICollection<EndpointDataSource> DataSources { get; }
}
