﻿// Copyright (c) 2019 under MIT license.

#region

using System.Linq;
using System.Net.Http;
using ImmediateReflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Xunit;

#endregion

namespace CV.WebApi.Tests.Authorization;

/// <summary>
///     Test suite for WebApi controllers.
/// </summary>
public class AuthorizationControllerTests
{
    /// <summary>
    ///     Ensures that all public controllers have CvAuthorize attribute.
    /// </summary>
    [Fact]
    public void Missed_Authorize_Attribute_On_Controller_Test()
    {
        var types = typeof(Program).Assembly
            .GetTypes()
            .Distinct()
            .Where(_ => typeof(ControllerBase).IsAssignableFrom(_))
            .ToArray();

        var exclude = new[]
        {
            "BaseController",
            "InfoController"
        };

        var missingAttrs = types.Where(type =>
                !type.IsDefinedImmediateAttribute<CvAuthorizeAttribute>() && !type.IsAbstract &&
                !exclude.Contains(type.Name, StringComparer.Ordinal))
            .ToList();

        Assert.True(missingAttrs.Count == 0,
            string.Join(Environment.NewLine,
                missingAttrs.Select(a => a.Name + " " + a.Assembly.GetName()
                        .Name)
                    .OrderBy(a => a, StringComparer.OrdinalIgnoreCase)));
    }

    /// <summary>
    ///     Ensures that all public api Non-Get methods controllers have Authorize attribute.
    /// </summary>
    [Fact]
    public void Missed_Authorize_Attribute_On_Write_Methods_Test()
    {
        var controllerTypes = typeof(Program).Assembly
            .GetTypes()
            .Distinct()
            .Where(_ => typeof(ControllerBase).IsAssignableFrom(_))
            .ToArray();

        var writableMethodNames = new[]
        {
            HttpMethod.Put.Method,
            HttpMethod.Post.Method,
            HttpMethod.Patch.Method,
            HttpMethod.Delete.Method,
        };
        var exclude = System.Array.Empty<string>();

        var missingAttrs = (from type in controllerTypes
            from methodAttribute in type.GetMembers()
            where !exclude.Contains(type.Name, StringComparer.Ordinal) &&
                  !methodAttribute.IsDefinedImmediateAttribute<CvAuthorizeAttribute>() && methodAttribute
                      .GetImmediateAttributes<HttpMethodAttribute>()
                      .SelectMany(a => a.HttpMethods)
                      .Any(a => writableMethodNames.Contains(a, StringComparer.Ordinal))
            select type).ToList();

        Assert.True(missingAttrs.Count == 0,
            string.Join(Environment.NewLine,
                missingAttrs.Select(a => a.Name + " " + a.Assembly.GetName()
                        .Name)
                    .OrderBy(a => a, StringComparer.OrdinalIgnoreCase)));
    }
}