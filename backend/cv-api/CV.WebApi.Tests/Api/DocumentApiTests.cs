﻿// Copyright (c) 2019 under MIT license.

#region

using System.Net.Http;
using System.Text;
using CV.Domain.Models;
using CV.Domain.Types;
using CV.Repository.Abstractions;
using CV.WebApi.Constants;
using CV.WebApi.Endpoints.Documents;
using LiteDB;
using Microsoft.AspNetCore.Mvc.Testing;
using Moq;
using Xunit;
using static CV.WebApi.Extensions.UrlExtensions;

#endregion

namespace CV.WebApi.Tests.Api;

public class DocumentsControllerTests : IClassFixture<WebApplicationFactory<Program>>
{
    private readonly string _existingDocumentId = ObjectId.NewObjectId()
        .ToString();

    private readonly WebApplicationFactory<Program> _factory;

    private readonly string _missingDocumentId = ObjectId.NewObjectId()
        .ToString();

    /// <summary>
    ///     Initializes a new instance of the <see cref="DocumentsControllerTests" /> class.
    /// </summary>
    /// <param name="factory">The factory.</param>
    public DocumentsControllerTests(WebApplicationFactory<Program> factory)
    {
        _factory = factory;
    }

    /// <summary>
    ///     Ensures that <see cref="GetDocuments" /> returns correct status code.
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task GetDocuments_ReturnsCorrectStatusCode()
    {
        // Arrange
        var repository = new Mock<IDocumentRepository>();
        repository.Setup(_ => _.GetAll())
            .Returns(RightAsync<BadRequest, Seq<Document>>(Seq(new[]
            {
                new Document("test", "me", Encoding.UTF8.GetBytes("testdata"))
            })));
        var client = _factory.CreateAuthorizedClient(services => { services.AddSingleton(_ => repository.Object); });

        var resultResponseMessage = await client.GetAsync(Routes.DocumentsRoot);

        // Assert
        resultResponseMessage.ShouldHave200Code();
    }

    /// <summary>
    ///     With unknown Id provided controller should return 404 status code.
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task GetUnknownDocument_ReturnsNotFound()
    {
        // Arrange
        using var client = _factory.CreateAuthorizedClient();

        // Act
        var resultResponseMessage = await client.GetAsync(UrlCombine(Routes.DocumentsRoot, _missingDocumentId));

        // Assert
        resultResponseMessage.ShouldHave404Code();
    }

    /// <summary>
    ///     Given existing id, when requested then I should get valid document and code
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task GetKnownDocument_ReturnsOk()
    {
        // Arrange
        var repository = new Mock<IDocumentRepository>();
        repository.Setup(_ => _.GetById(_existingDocumentId))
            .Returns(OptionAsync<Document>.Some(new Document("test", "me", Encoding.UTF8.GetBytes("testdata"))));
        using var client = _factory.CreateAuthorizedClient(services =>
        {
            services.AddSingleton(_ => repository.Object);
        });

        // Act
        var resultResponseMessage = await client.GetAsync(UrlCombine(Routes.DocumentsRoot, _existingDocumentId));

        // Assert
        resultResponseMessage.ShouldHave200Code();
    }

    /// <summary>
    ///     Given empty database, when deleting document then I should get Not Found code result
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task DeleteDocument_With_UknownID_Returns_NotFound()
    {
        // Arrange
        using var client = _factory.CreateAuthorizedClient();

        // Act
        var resultResponseMessage = await client.DeleteAsync(UrlCombine(Routes.DocumentsRoot, _missingDocumentId));

        // Assert
        resultResponseMessage.ShouldHave404Code();
    }

    /// <summary>
    ///     Given empty database, when deleting document then I should get Not Found code result
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task DeleteDocument_With_KnownID_Returns_Ok()
    {
        // Arrange
        var repository = new Mock<IDocumentRepository>();
        repository.Setup(_ => _.GetById(_existingDocumentId))
            .Returns(new Document("test", TestClaimsProvider.Admin,
                Encoding.UTF8.GetBytes("testdata")));
        repository.Setup(_ => _.Delete(_existingDocumentId))
            .Returns(true);
        using var client =
            _factory.CreateAuthorizedClient(services => services.AddSingleton(_ => repository.Object));

        // Act
        var resultResponseMessage = await client.DeleteAsync(UrlCombine(Routes.DocumentsRoot, _existingDocumentId));

        // Assert
        resultResponseMessage.ShouldHave200Code();
        repository.Verify(_ => _.Delete(_existingDocumentId), Times.Once);
    }

    /// <summary>
    ///     Given external files, when uploading them then I should get Success code result
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task UploadDocument_Returns_Ok()
    {
        // Arrange
        var repository = new Mock<IDocumentRepository>();
        repository.Setup(_ => _.Save(It.IsAny<Seq<Document>>()))
            .Returns(RightAsync<BadRequest, Unit>(Unit.Default));
        using var client = _factory.CreateAuthorizedClient(services =>
        {
            services.AddSingleton(_ => repository.Object);
        });

        // Act
        var resultResponseMessage = await client.PutAsync(Routes.DocumentsRoot,
            WebApiExtensions.CreateTestFiles(new[]
            {
                new
                    { Content = "file content", Name = "test.csv" }
            }));

        // Assert
        resultResponseMessage.ShouldHave200Code();
        repository.Verify(_ => _.Save(It.IsAny<Seq<Document>>()), Times.Once);
    }

    /// <summary>
    ///     Given no files, when uploading them then I should get BadRequest code result
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task UploadNoDocument_Returns_BadRequest()
    {
        // Arrange
        using var client = _factory.CreateAuthorizedClient();

        // Act
        var resultResponseMessage = await client.PutAsync(Routes.DocumentsRoot,
            new MultipartFormDataContent
            {
                new StringContent("test")
            });

        // Assert
        resultResponseMessage.ShouldHave400Code(false);
    }

    /// <summary>
    ///     Given several files, when uploading them then I should get BadRequest code result
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task UpdateMultipleDocuments_Returns_BadRequest()
    {
        // Arrange
        using var client = _factory.CreateAuthorizedClient();

        // Act
        var resultResponseMessage = await client.PostAsync(UrlCombine(Routes.DocumentsRoot, _existingDocumentId),
            WebApiExtensions.CreateTestFiles(new[]
            {
                new { Content = "file content", Name = "test.csv" },
                new { Content = "file content 2", Name = "test2.csv" }
            }));

        // Assert
        resultResponseMessage.ShouldHave400Code(false);
    }

    /// <summary>
    ///     Given single file, when uploading for update then I should get Ok code result
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task UpdateKnownDocument_Returns_Ok()
    {
        // Arrange
        var repository = new Mock<IDocumentRepository>();
        repository.Setup(_ => _.GetById(_existingDocumentId))
            .Returns(new Document("test.csv", TestClaimsProvider.Admin,
                Encoding.UTF8.GetBytes("testdata")));
        repository.Setup(_ => _.Update(_existingDocumentId, It.IsAny<Document>()))
            .Returns(true);
        using var client = _factory.CreateAuthorizedClient(services => services.AddSingleton(_ => repository.Object));

        var newContent = "new content";
        // Act
        var resultResponseMessage = await client.PostAsync(UrlCombine(Routes.DocumentsRoot, _existingDocumentId),
            WebApiExtensions.CreateTestFiles(new[]
            {
                new { Content = newContent, Name = "test.csv" }
            }));

        // Assert

        resultResponseMessage.ShouldHave200Code();
        repository.Verify(_ => _.Update(_existingDocumentId,
            It.Is<Document>(document => Encoding.UTF8.GetString(document.Data) == newContent)), Times.Once);
    }

    /// <summary>
    ///     Given unknown id, when uploading for update then I should get 404 code result
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task UpdateUnknownDocument_Returns_NotFound()
    {
        // Arrange
        var repository = new Mock<IDocumentRepository>();
        repository.Setup(_ => _.GetById(_existingDocumentId))
            .Returns(new Document("test.csv", TestClaimsProvider.Admin,
                Encoding.UTF8.GetBytes("testdata")));
        using var client = _factory.CreateAuthorizedClient(services => services.AddSingleton(_ => repository.Object));

        var newContent = "new content";
        // Act
        var resultResponseMessage = await client.PostAsync(UrlCombine(Routes.DocumentsRoot, _missingDocumentId),
            WebApiExtensions.CreateTestFiles(new[]
            {
                new { Content = newContent, Name = "test.csv" }
            }));

        // Assert

        resultResponseMessage.ShouldHave404Code();
    }
}