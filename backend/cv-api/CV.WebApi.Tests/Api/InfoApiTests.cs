﻿// Copyright (c) 2019 under MIT license.

#region

using CV.WebApi.Constants;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

#endregion

namespace CV.WebApi.Tests.Api;

public class InfoControllerTests : IClassFixture<WebApplicationFactory<Program>>
{
    private readonly WebApplicationFactory<Program> _factory;

    /// <summary>
    ///     Initializes a new instance of the <see cref="InfoControllerTests" /> class.
    /// </summary>
    /// <param name="factory">The factory.</param>
    public InfoControllerTests(WebApplicationFactory<Program> factory)
    {
        _factory = factory;
    }

    /// <summary>
    ///     Ensures that <see cref="Routes.InfoRoot" /> returns correct status code.
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task GetInfo_ReturnsCorrectStatusCode()
    {
        // Arrange
        var client = _factory.CreateAnonymousClient();

        var resultResponseMessage = await client.GetAsync(Routes.HealthcheckRoot);

        // Assert
        resultResponseMessage.ShouldHave200Code();
    }
}