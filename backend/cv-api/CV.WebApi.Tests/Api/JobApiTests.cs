﻿// Copyright (c) 2019 under MIT license.

using System.Linq;
using CV.Domain.Models;
using CV.Domain.Services.Abstractions;
using CV.Domain.Types;
using CV.WebApi.Constants;
using CV.WebApi.Endpoints.Jobs;
using Microsoft.AspNetCore.Mvc.Testing;
using Moq;
using Xunit;

namespace CV.WebApi.Tests.Api;

public class JobApiTests : IClassFixture<WebApplicationFactory<Program>>
{
    private readonly WebApplicationFactory<Program> _factory;

    /// <summary>
    ///     Initializes a new instance of the <see cref="DocumentsControllerTests" /> class.
    /// </summary>
    /// <param name="factory">The factory.</param>
    public JobApiTests(WebApplicationFactory<Program> factory)
    {
        _factory = factory;
    }

    /// <summary>
    ///     Ensures that <see cref="GetJobs" /> returns correct status code.
    /// </summary>
    /// <returns></returns>
    [Fact]
    public async Task GetJobs_ReturnsCorrectStatusCode()
    {
        // Arrange
        var repository = new Mock<IJobService>();
        repository.Setup(_ => _.GetAll())
            .Returns(RightAsync<BadRequest, Seq<Job>>(Seq(Enumerable.Empty<Job>())));
        var client = _factory.CreateAuthorizedClient(services => { services.AddSingleton(_ => repository.Object); });

        var resultResponseMessage = await client.GetAsync(Routes.DocumentsRoot);

        // Assert
        // TODO: ensure that returned items count is same
        resultResponseMessage.ShouldHave200Code();
    }
}