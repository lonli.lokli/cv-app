﻿// Copyright (c) 2019 under MIT license.

#region

#endregion

namespace CV.WebApi.Tests;

public class TestClaimsProvider
{
    public static readonly string Admin = "Admin";

    public TestClaimsProvider(IList<Claim> claims)
    {
        Claims = claims;
    }

    public TestClaimsProvider()
    {
        Claims = new List<Claim>();
    }

    public IList<Claim> Claims { get; }

    public static TestClaimsProvider WithUserClaims()
    {
        var provider = new TestClaimsProvider();
        provider.Claims.Add(new Claim(ClaimTypes.NameIdentifier, Guid.NewGuid()
            .ToString()));
        provider.Claims.Add(new Claim(ClaimTypes.Name, Admin));
        provider.Claims.Add(new Claim(ClaimTypes.Role, "Admin"));
        return provider;
    }
}