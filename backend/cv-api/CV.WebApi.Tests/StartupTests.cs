﻿// Copyright (c) 2019 under MIT license.

#region

using System.Diagnostics;
using CV.Domain.Configurations;
using CV.Domain.Services.Abstractions;
using CV.Repository.Abstractions;
using FluentAssertions;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.ObjectPool;
using Swashbuckle.AspNetCore.Swagger;
using Xunit;

#endregion

namespace CV.WebApi.Tests;

/// <summary>
///     Class StartupTests
/// </summary>
public class StartupTests
{
    /// <summary>
    ///     Defines the test method Startup_Configure.
    /// </summary>
    [Theory]
    [InlineData("Development")]
    [InlineData("Production")]
    public void StartupConfigure(string environmentName)
    {
        // Arrange
        var application = new WebApplicationFactory<Program>()
            .WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                    {
                        services.AddSingleton<ILoggerFactory, LoggerFactory>();
                        services.AddSingleton<ObjectPoolProvider, DefaultObjectPoolProvider>();
                        services.AddSingleton<IHostEnvironment>(provider => new HostingEnvironment());
                        services.AddSingleton<DiagnosticSource>(new DiagnosticListener("test"));
                    })
                    .ConfigureAppConfiguration(cfgBuilder =>
                        cfgBuilder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.test.json")))
                    .UseEnvironment(environmentName);
            });
        // Act
        var serviceProvider = application.Services;

        // Assert
        serviceProvider.Should()
            .NotBeNull();
        serviceProvider.GetRequiredService<IConfiguration>()
            .Should()
            .NotBeNull();
        serviceProvider.GetRequiredService<IAntiforgery>()
            .Should()
            .NotBeNull();
        serviceProvider.GetRequiredService<ISwaggerProvider>()
            .Should()
            .NotBeNull();

        var dbConfiguration = serviceProvider.GetRequiredService<LiteDbConfiguration>();
        dbConfiguration.Should()
            .NotBeNull();
        dbConfiguration.DbPath.Should()
            .NotBeEmpty();
        dbConfiguration.Password.Should()
            .NotBeEmpty();

        var hashingConfiguration = serviceProvider.GetRequiredService<HashingOptions>();
        hashingConfiguration.Should()
            .NotBeNull();
        hashingConfiguration.Iterations.Should()
            .BeGreaterThan(0);

        serviceProvider.GetRequiredService<IDocumentRepository>()
            .Should()
            .NotBeNull();
        serviceProvider.GetRequiredService<IDocumentService>()
            .Should()
            .NotBeNull();
        serviceProvider.GetRequiredService<IWebHostEnvironment>()
            .IsEnvironment(environmentName)
            .Should()
            .BeTrue();
        serviceProvider.GetRequiredService<IJobRepository>()
            .Should()
            .NotBeNull();
        serviceProvider.GetRequiredService<IJobService>()
            .Should()
            .NotBeNull();
    }
}