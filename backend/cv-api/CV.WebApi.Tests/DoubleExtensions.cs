﻿// Copyright (c) 2019 under MIT license.

#region

using System.Linq;
using System.Net.Http;
using CV.WebApi.Constants;
using FluentAssertions;
using static Microsoft.AspNetCore.Http.StatusCodes;

#endregion

namespace CV.WebApi.Tests;

/// <summary>
///     Extensions for tes purpose.
/// </summary>
public static class DoubleExtensions
{
    /// <summary>
    ///     Ensures that StatusCode is OK.
    /// </summary>
    /// <param name="message"></param>
    public static void ShouldHave200Code(this HttpResponseMessage message, bool includeHeaders = false) =>
        ((int)message.StatusCode).Should()
        .Be(Status200OK, message.AsString(includeHeaders));

    /// <summary>
    ///     Ensures that StatusCode is 500.
    /// </summary>
    /// <param name="message"></param>
    public static void ShouldHave500Code(this HttpResponseMessage message) =>
        ((int)message.StatusCode).Should()
        .Be(Status500InternalServerError, message.AsString());

    /// <summary>
    ///     Ensures that StatusCode is 201.
    /// </summary>
    /// <param name="message"></param>
    public static void ShouldHave201Code(this HttpResponseMessage message) =>
        ((int)message.StatusCode).Should()
        .Be(Status201Created, message.AsString());

    /// <summary>
    ///     Ensures that StatusCode is 404.
    /// </summary>
    /// <param name="message"></param>
    public static void ShouldHave404Code(this HttpResponseMessage message) =>
        ((int)message.StatusCode).Should()
        .Be(Status404NotFound, message.AsString());

    /// <summary>
    ///     Ensures that StatusCode is 400.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="notifyUser">If user should be notified</param>
    public static void ShouldHave400Code(this HttpResponseMessage message, bool notifyUser)
    {
        ((int)message.StatusCode).Should()
            .Be(Status400BadRequest, message.AsString());
        if (notifyUser)
        {
            message
                .As<HttpResponseMessage>()
                .Headers
                .TryGetValues(ServiceConstants.NotifyUserResponseHeader, out var values)
                .Should()
                .BeTrue(
                    $"Header {ServiceConstants.NotifyUserResponseHeader} must be specified");
            values.Should()
                .NotBeEmpty()
                .And.Subject.First()
                .Should()
                .Be(bool.TrueString);
        }
    }

    /// <summary>
    ///     As String
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public static string AsString(this HttpResponseMessage message, bool includeHeaders = false) =>
        message.Content.ReadAsStringAsync()
            .Result.IfEmpty(message.Headers.ToString(), includeHeaders)
                .IfEmpty(message.Content.ReadAsStringAsync().Result);

    /// <summary>
    ///     If Empty
    /// </summary>
    /// <param name="input"></param>
    /// <param name="other"></param>
    /// <returns></returns>
    public static string IfEmpty(this string input, string other, bool force = false) =>
        string.IsNullOrEmpty(input) && !force ? other : input;
}