﻿// Copyright (c) 2019 under MIT license.

#region

using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;

#endregion

namespace CV.WebApi.Tests;

public static class WebApiExtensions
{
    public static WebApplicationFactory<T> WithAuthentication<T>(this WebApplicationFactory<T> factory,
        TestClaimsProvider claimsProvider, Action<IServiceCollection>? servicesConfiguration = null) where T : class
    {
        return factory.WithWebHostBuilder(builder =>
        {
            builder.ConfigureAppConfiguration((ctx, cfgBuilder) =>
                cfgBuilder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.test.json")));
            builder.ConfigureTestServices(services =>
            {
                services.AddAuthentication("Test")
                    .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>("Test", op => { });

                services.AddScoped(_ => claimsProvider);
                servicesConfiguration?.Invoke(services);
            });
        });
    }

    public static HttpClient CreateAuthorizedClient<T>(this WebApplicationFactory<T> factory,
        Action<IServiceCollection>? servicesConfiguration = null) where T : class
    {
        var client = CreateAnonymousClient(factory, servicesConfiguration);
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Test");
        return client;
    }

    public static HttpClient CreateAnonymousClient<T>(this WebApplicationFactory<T> factory,
        Action<IServiceCollection>? servicesConfiguration = null) where T : class
    {
        var client = factory.WithAuthentication(TestClaimsProvider.WithUserClaims(), servicesConfiguration)
            .CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });

        return client;
    }

    public static MultipartFormDataContent CreateTestFiles(IEnumerable<dynamic> files)
    {
        var content = new MultipartFormDataContent();
        foreach (var file in files)
        {
#pragma warning disable CA2000
            content.Add(new ByteArrayContent(Encoding.UTF8.GetBytes(file.Content)), "files", file.Name);
#pragma warning restore CA2000
        }

        return content;
    }
}