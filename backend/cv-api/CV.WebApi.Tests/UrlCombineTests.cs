﻿// Copyright (c) 2019 under MIT license.

using FluentAssertions;
using Xunit;
using static CV.WebApi.Extensions.UrlExtensions;

namespace CV.WebApi.Tests;

public class UrlCombineTests
{
    [Theory]
    [MemberData(nameof(Urls), MemberType = typeof(UrlCombineTests))]
    public void UrlCombine_Tests(Tuple<string, string, string> items)
    {
        var (url1, url2, result) = items;

        UrlCombine(url1, url2)
            .Should()
            .Be(result);
    }

    public static TheoryData<Tuple<string, string, string>> Urls =>
        new()
        {
            System.Tuple.Create<string, string, string>("test1", "test2", "test1/test2")
        };
}