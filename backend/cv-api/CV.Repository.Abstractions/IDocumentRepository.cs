﻿// Copyright (c) 2019 under MIT license.

#region

using CV.Domain.Models;
using CV.Domain.Types;
using LanguageExt;

#endregion

namespace CV.Repository.Abstractions;

/// <summary>
///     Document manager repository
/// </summary>
public interface IDocumentRepository
{
    /// <summary>
    ///     Saves the specified documents.
    /// </summary>
    /// <param name="documents">The documents.</param>
    EitherAsync<BadRequest, Unit> Save(Seq<Document> documents);

    /// <summary>
    ///     Gets the document list.
    /// </summary>
    EitherAsync<BadRequest, Seq<Document>> GetAll();

    /// <summary>
    ///     Gets the document.
    /// </summary>
    /// <param name="id">The identifier.</param>
    OptionAsync<Document> GetById(string id);

    /// <summary>
    ///     Updates the specified document.
    /// </summary>
    /// <param name="document">The document.</param>
    /// <returns>True if succeeded.</returns>
    EitherAsync<BadRequest, bool> Update(string documentId, Document document);

    /// <summary>
    ///     Deletes the specified document.
    /// </summary>
    /// <param name="documentId">The document identifier.</param>
    /// <returns>True if succeeded.</returns>
    EitherAsync<BadRequest, bool> Delete(string documentId);
}