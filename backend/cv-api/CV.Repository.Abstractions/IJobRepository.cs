﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Models;
using CV.Domain.Types;
using LanguageExt;

namespace CV.Repository.Abstractions;

/// <summary>
/// Job Repository abstraction
/// </summary>
public interface IJobRepository
{
    /// <summary>
    /// Loads all existing jobs
    /// </summary>
    public EitherAsync<BadRequest, Seq<Job>> GetAll();
}