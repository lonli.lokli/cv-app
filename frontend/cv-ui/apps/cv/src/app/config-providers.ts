import { APP_INITIALIZER, Provider } from '@angular/core';
import { AuthClientConfig } from '@auth0/auth0-angular';

import { AuthSettings } from '@cv/common/auth';
import { BuildSettings, HOST_SETTINGS } from '@cv/common/core';
import { environment } from '../environments/environment';
import { ConfigService } from './config.service';

export function configServiceFactory(config: ConfigService, authConfig: AuthClientConfig) {
  return () => config.loadConfig(authConfig);
}

export function configHostSettingFactory(service: ConfigService) {
  return service.config?.host;
}

export function configAuthSettingFactory(service: ConfigService) {
  return service.config?.auth;
}

export function buildSettingFactory(): BuildSettings {
  return new BuildSettings(environment.build.version, environment.build.time);
}

export const CONFIG_SERVICE_PROVIDERS: Provider[] = [
  ConfigService,
  {
    provide: APP_INITIALIZER,
    useFactory: configServiceFactory,
    deps: [ConfigService, AuthClientConfig],
    multi: true
  },
  {
    provide: HOST_SETTINGS,
    useFactory: configHostSettingFactory,
    deps: [ConfigService]
  },
  {
    provide: AuthSettings,
    useFactory: configAuthSettingFactory,
    deps: [ConfigService]
  },
  {
    provide: BuildSettings,
    useFactory: buildSettingFactory
  }
];
