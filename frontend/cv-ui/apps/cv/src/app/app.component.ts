import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterModule } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { TippyDirective } from '@ngneat/helipopper';
import { groupBy, some, find } from 'lodash-es';
import { ToastrService } from 'ngx-toastr';
import { combineLatest, Observable } from 'rxjs';
import { filter, startWith } from 'rxjs/operators';

import { ApiInfoComponent } from '@cv/about/ui';
import {
  EntitlementsService,
  CvUserEntitlementsData,
  AuthenticationComponent,
  ProfileComponent
} from '@cv/common/auth';
import { RouteHelper } from '@cv/common/core';
import { IconComponent } from '@cv/common/icons';
import { NavbarComponent, NavbarData } from '@cv/common/navbar';

import { APPLICATION_ROUTES } from '../application.routes';
import { AsyncPipe, NgIf } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    IconComponent,
    RouterModule,
    TippyDirective,
    ApiInfoComponent,
    AuthenticationComponent,
    ProfileComponent,
    NavbarComponent,
    AsyncPipe,
    NgIf
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  public activeNavBars: { path: string; data: NavbarData }[] = [];
  public isAuthenticated$: Observable<boolean>;

  constructor(
    private route: ActivatedRoute,
    private entitlementsService: EntitlementsService,
    private router: Router,
    private auth: AuthService,
    private toastr: ToastrService
  ) {
    this.isAuthenticated$ = this.auth.isAuthenticated$;

    const navBarGroups = groupBy(
      APPLICATION_ROUTES.filter(r => r.data && r.data['navbar']).map(r => ({
        path: r.path || '',
        data: r.data?.['navbar'] as NavbarData
      })),
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      (menu: any) => menu.data.options.group
    );

    combineLatest([
      this.entitlementsService.getUserEntitlements(),
      this.router.events.pipe(
        filter((event): event is NavigationEnd => event instanceof NavigationEnd),
        startWith(null)
      )
    ]).subscribe(([entitlements]) => {
      const firstLevelPath = RouteHelper.getChildModuleName(this.route) || '';
      const activeNavBarGroup =
        find(navBarGroups, (group: any) => some(group, (navBar: any) => navBar.path === firstLevelPath)) ||
        // eslint-disable-next-line unicorn/no-useless-undefined
        navBarGroups[String(undefined)];

      this.activeNavBars = activeNavBarGroup.filter(item => this.isNavBarItemVisible(item.path, entitlements));
    });
  }

  ngOnInit(): void {
    this.auth.error$.subscribe((error: any) => {
      console.error('Auth:', error);
      this.toastr.error(error.error_description);
    });
  }

  private isNavBarItemVisible(path: string, entitlements: CvUserEntitlementsData) {
    return entitlements.Modules[path];
  }
}
