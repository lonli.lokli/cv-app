import { ApplicationConfig , APP_INITIALIZER, ErrorHandler, importProvidersFrom } from '@angular/core';
import { provideRouter, Router } from '@angular/router';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideTippyConfig, tooltipVariation, popperVariation } from '@ngneat/helipopper';
import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptors, withInterceptorsFromDi } from '@angular/common/http';
import { HTTP_ERROR_INTERCEPTOR } from '@cv/common/http';

import { DialogModule } from '@ngneat/dialog';
import { ToastrModule } from 'ngx-toastr';
import { AuthHttpInterceptor, AuthModule as Auth0Module } from '@auth0/auth0-angular';
import { ServiceWorkerModule } from '@angular/service-worker';
import { CONFIG_SERVICE_PROVIDERS } from './config-providers';
import { API_PROVIDERS } from '@cv/api';
import { createErrorHandler, TraceService } from '@sentry/angular-ivy';
import { APPLICATION_ROUTES } from '../application.routes';
import { environment } from '../environments/environment';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(APPLICATION_ROUTES),
    provideAnimations(),
    provideTippyConfig({
      defaultVariation: 'tooltip',
      variations: {
        tooltip: tooltipVariation,
        popper: popperVariation
      }
    }),
    { provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true },
    provideHttpClient(withInterceptorsFromDi(), withInterceptors([HTTP_ERROR_INTERCEPTOR])),
    importProvidersFrom([
      DialogModule.forRoot(),
      ToastrModule.forRoot(),
      Auth0Module.forRoot(), // actual settings are used in config-provider.ts, do not pass them here

      ServiceWorkerModule.register('ngsw-worker.js', {
        enabled: environment.production,
        // Register the ServiceWorker as soon as the application is stable
        // or after 30 seconds (whichever comes first).
        registrationStrategy: 'registerWhenStable:30000'
      })
    ]),
    CONFIG_SERVICE_PROVIDERS,
    API_PROVIDERS,
    {
      provide: ErrorHandler,
      useValue: createErrorHandler({
        showDialog: false
      })
    },
    {
      provide: TraceService,
      deps: [Router]
    },
    {
      provide: APP_INITIALIZER,
      useFactory: () => () => {},
      deps: [TraceService],
      multi: true
    }
  ]
};
