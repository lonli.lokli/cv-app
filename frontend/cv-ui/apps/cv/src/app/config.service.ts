import { HttpBackend, HttpClient } from '@angular/common/http';
import { ErrorHandler, Injectable, inject } from '@angular/core';
import { AuthClientConfig } from '@auth0/auth0-angular';

import { AuthSettings } from '@cv/common/auth';
import { HostSettings } from '@cv/common/core';
import { lastValueFrom } from 'rxjs';
import { z } from 'zod';
import { generateErrorMessage } from 'zod-error';
import { DialogService } from '@ngneat/dialog';
import { stringToJSON } from '@cv/fp';
import { hasOneOrMore } from '@cv/common/utils';

const ApplicationConfiguration = z.object({
  host: HostSettings,
  auth: AuthSettings
});
type ApplicationConfiguration = z.infer<typeof ApplicationConfiguration>;

@Injectable()
export class ConfigService {
  public config: ApplicationConfiguration | undefined = undefined;
  #handler = inject(HttpBackend);
  #dialog = inject(DialogService);
  #erroHanlder = inject(ErrorHandler);

  public loadConfig(authConfig: AuthClientConfig): Promise<ApplicationConfiguration | undefined> {
    return lastValueFrom(
      new HttpClient(this.#handler).get('host.config.json', {
        responseType: 'text'
      })
    )
      .then(ok => {
        let issues: z.ZodIssue[] = [];
        const schema = stringToJSON().safeParse(ok);
        if (schema.success) {
          const applicationConfiguration = ApplicationConfiguration.safeParse(schema.data);
          if (applicationConfiguration.success) {
            this.config = applicationConfiguration.data;
          } else issues = applicationConfiguration.error.issues;
        } else issues = schema.error.issues;

        authConfig.set({
          clientId: this.config?.auth.clientId ?? '',
          domain: this.config?.auth.endpoint ?? '',
          authorizationParams: {
            audience: this.config?.auth.audience,
            redirect_uri: window.location.origin
          },
          httpInterceptor: {
            allowedList: [`${this.config?.host.api}/api/*`]
          },
          useCookiesForTransactions: true,
          authorizeTimeoutInSeconds: 5
        });

        if (hasOneOrMore(issues)) {
          this.reportFailure(generateErrorMessage(issues));
        }
        return this.config;
      })
      .catch(error => {
        authConfig.set({
          clientId: '',
          domain: ''
        });
        this.reportFailure('Cannot find host.config.json file', error);
        return undefined;
      });
  }

  private reportFailure(details: string, error?: any) {
    this.#erroHanlder.handleError(new Error(JSON.stringify(error ?? details)));
    this.#dialog.error({ title: 'Application Initialization failed', body: details });
  }
}
