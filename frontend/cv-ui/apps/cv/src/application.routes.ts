import { Routes } from '@angular/router';
import { identity } from 'rxjs';

import { CAN_ACTIVATE_GUARD } from '@cv/common/auth';
import { NavbarData } from '@cv/common/navbar';
import { ModuleType } from '@cv/typings';

export const staticModules = [
  {
    path: 'about',
    loadChildren: () => import('@cv/about/ui').then(m => m.ABOUT_ROUTES),
    data: {
      navbar: new NavbarData('About')
    }
  }
];

export const APPLICATION_ROUTES: Routes = [
  {
    path: identity<ModuleType>('colleague'),
    loadChildren: () => import('@cv/jobs/ui').then(m => m.COLLEAGUES_ROUTES),
    data: {
      navbar: new NavbarData('Colleagues')
    },
    canActivate: [CAN_ACTIVATE_GUARD],
    canActivateChild: [CAN_ACTIVATE_GUARD]
  },
  {
    path: identity<ModuleType>('documents'),
    loadChildren: () => import('@cv/docs/ui').then(m => m.DOCUMENTS_ROUTES),
    data: {
      navbar: new NavbarData('Documents')
    },
    canActivate: [CAN_ACTIVATE_GUARD],
    canActivateChild: [CAN_ACTIVATE_GUARD]
  },
  ...staticModules,
  { path: '**', redirectTo: 'about' }
];
