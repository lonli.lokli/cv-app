import { appConfig } from './app/app.config';

import { bootstrapApplication } from '@angular/platform-browser';

import { AppComponent } from './app/app.component';
import { enableProdMode } from '@angular/core';
import { BrowserTracing, init as initSentry, routingInstrumentation } from '@sentry/angular-ivy';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();

  initSentry({
    dsn: 'https://0a65643a264245d883084cb24db18cda@o473632.ingest.sentry.io/5508698',
    integrations: [
      new BrowserTracing({
        tracingOrigins: ['https://api.faircv.today'],
        routingInstrumentation: routingInstrumentation
      })
    ],
    release: environment.build.version,
    tracesSampleRate: 0.25
  });
}
bootstrapApplication(AppComponent, appConfig).catch(error => console.error(error));
