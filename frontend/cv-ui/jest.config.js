const { pathsToModuleNameMapper } = require('ts-jest');
const { paths } = require('./tsconfig.json').compilerOptions;

module.exports = {
  globalSetup: 'jest-preset-angular/global-setup',
  testPathIgnorePatterns: ['e2e'],
  preset: 'jest-preset-angular',
  collectCoverageFrom: ['<rootDir>/apps/cv/*.{ts,js}', '<rootDir>/libs/**/*.{ts,js}', '<rootDir>/modules/**/*.{ts,js}'],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        addFileAttribute: 'true'
      }
    ]
  ],
  coverageReporters: ['text-summary', 'lcov'],
  moduleNameMapper: {
    ...pathsToModuleNameMapper(paths, { prefix: '<rootDir>' })
  },
  coverageProvider: 'v8',
  transformIgnorePatterns: ['node_modules/(?!@angular|@ngneat|ngx-toastr|lodash-es)'],
  setupFilesAfterEnv: ['<rootDir>/doubles/setup-jest.ts']
};
