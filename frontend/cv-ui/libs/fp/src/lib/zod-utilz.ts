import { z } from 'zod';

const literalSchema = z.union([z.string(), z.number(), z.boolean(), z.null()]);

type Literal = z.infer<typeof literalSchema>;
type Json = Literal | { [key: string]: Json } | Json[];

const jsonSchema: z.ZodType<Json> = z.lazy(() => z.union([literalSchema, z.array(jsonSchema), z.record(jsonSchema)]));
export const json = () => jsonSchema;

const stringToJSONSchema = z.string().transform((input, context): z.infer<ReturnType<typeof json>> => {
  try {
    return JSON.parse(input);
  } catch {
    context.addIssue({ code: 'custom', message: 'Invalid JSON' });
    return z.NEVER;
  }
});

/**
stringToJSON() is a schema that validates JSON encoded as a string, then returns the parsed value
@example
import { zu } from 'zod_utilz'
const schema = zu.stringToJSON()
schema.parse( 'true' ) // true
schema.parse( 'null' ) // null
schema.parse( '["one", "two", "three"]' ) // ['one', 'two', 'three']
schema.parse( '<html>not a JSON string</html>' ) // throws
*/
export const stringToJSON = () => stringToJSONSchema;
