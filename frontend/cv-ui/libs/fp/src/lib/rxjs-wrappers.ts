import { HttpErrorResponse } from '@angular/common/http';
import { Either, left, right } from '@sweet-monads/either';
import { OperatorFunction, of, pipe as pipeRx, ObservableInput } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { isDefined } from '@cv/common/utils';
import { CvSingleError, CvErrors } from '@cv/typings';

export function debug<T = unknown>(message = 'LOG', extractor: <T>(item: T) => unknown = item => item) {
  return tap<T>({
    next(value) {
      console.log(
        `%c[${message}: Next]`,
        'background: #009688; color: #fff; padding: 3px; font-size: 9px;',
        extractor(value)
      );
    },
    error(error) {
      console.log(
        `%[${message}: Error]`,
        'background: #E91E63; color: #fff; padding: 3px; font-size: 9px;',
        extractor(error)
      );
    },
    complete() {
      console.log(`%c[${message}]: Complete`, 'background: #00BCD4; color: #fff; padding: 3px; font-size: 9px;');
    }
  });
}

export function log<T>(message?: string): OperatorFunction<T, T> {
  return tap(value => console.log(message, value));
}

export const isArrayLike = <T>(x: any): x is ArrayLike<T> =>
  isDefined(x) && typeof x.length === 'number' && typeof x !== 'function';

export function isPromise(value: any): value is PromiseLike<any> {
  return isDefined(value) && typeof value.subscribe !== 'function' && typeof value.then === 'function';
}

export function mapEither<E, D, ND>(mapDataOrValue: ((data: D) => ND) | ND) {
  const mapDataResult =
    typeof mapDataOrValue === 'function' ? (mapDataOrValue as (data: D) => ND) : () => mapDataOrValue;
  return map<Either<E, D>, Either<E, ND>>((either: Either<E, D>) => either.map(mapDataResult));
}

export function mapEitherToValue<E, D>(mapDataOrValue: ((data: E) => D) | D) {
  const mapDataResult =
    typeof mapDataOrValue === 'function' ? (mapDataOrValue as (data: E) => D) : () => mapDataOrValue;
  return map<Either<E, D>, D>((either: Either<E, D>) =>
    either.isRight() ? either.value : mapDataResult(either.value)
  );
}

export function tapEither<E, D>(tapData: (data: D) => void) {
  return tap<Either<E, D>>((either: Either<E, D>) => {
    either.mapRight(tapData);
  });
}

export function switchMapEither<E, D, ND>(mapDataOrValue: ((data: D) => ND) | ND) {
  const mapDataResult =
    typeof mapDataOrValue === 'function' ? (mapDataOrValue as (data: D) => ND) : () => mapDataOrValue;
  return switchMap<Either<E, D>, ObservableInput<Either<E, ND>>>((either: Either<E, D>) =>
    of(either.map(mapDataResult))
  );
}

export function eitherify<T>(): OperatorFunction<T, Either<CvErrors, T>> {
  return pipeRx(
    map(right),
    catchError(error =>
      of(
        left(
          asErrors({
            code: 'external_failure',
            friendlyMessage: error instanceof HttpErrorResponse ? error.message : `Failure happened, reason: ${error}`
          })
        )
      )
    )
  );
}

export interface Lazy<A> {
  (): A;
}

export interface Task<A> {
  (): Promise<A>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface TaskEither<E, A> extends Task<Either<E, A>> {}

export const tryCatch =
  <E, A>(f: Lazy<Promise<A>>, onRejected: (reason: unknown) => E): TaskEither<E, A> =>
  async () => {
    try {
      return await f().then(right);
    } catch (error) {
      return left(onRejected(error));
    }
  };

export const asErrors = (error: CvSingleError): CvErrors => [error];

export const addError =
  (error: CvSingleError) =>
  (errors: CvErrors): CvErrors =>
    [...errors, error];
