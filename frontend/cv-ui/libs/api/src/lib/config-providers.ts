import { HostSettings, HOST_SETTINGS } from '@cv/common/core';
import { ApiConfiguration } from './generated/api-configuration';

function cvTokenFactory(host: HostSettings): ApiConfiguration {
  const c = new ApiConfiguration();
  c.rootUrl = host.api;
  return c;
}

export const API_PROVIDERS = [
  {
    provide: ApiConfiguration,
    useFactory: cvTokenFactory,
    deps: [HOST_SETTINGS]
  }
];
