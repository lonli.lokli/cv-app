import { HttpErrorResponse } from '@angular/common/http';
import { Either, left, right } from '@sweet-monads/either';
import { Observable, of, map, catchError } from 'rxjs';

import { parseApiError } from '@cv/common/utils';

import { StrictHttpResponse } from '../generated/strict-http-response';
import { ServerError } from './types';

export function eitherify<T>(): (source: Observable<StrictHttpResponse<T>>) => Observable<Either<ServerError, T>> {
  return (source: Observable<StrictHttpResponse<T>>) =>
    source.pipe(
      map(r => right<ServerError, T>(r.body as T)),
      catchError(error =>
        of(
          left<ServerError, T>(
            error instanceof HttpErrorResponse
              ? parseApiError(error)
              : {
                  details: 'Cannot load data.',
                  title: 'API call failure'
                }
          )
        )
      )
    );
}
