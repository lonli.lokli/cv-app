import { Either } from '@sweet-monads/either';
import { Observable } from 'rxjs';

export interface ApiError {
  title: string;
  details: string;
}

export type ServerError = ApiError;
export type ValidationError = ApiError;

export type ExternalApiCall<T> = Observable<Either<ApiError, T>>;
