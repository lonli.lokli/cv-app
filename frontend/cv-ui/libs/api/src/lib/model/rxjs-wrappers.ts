import { Either, left, right } from '@sweet-monads/either';
import { map } from 'rxjs';
import { z, ZodTypeAny } from 'zod';

import { ApiError } from './types';

export function mapValidation<D, V extends ZodTypeAny>(schema: V) {
  return map<Either<ApiError, D>, Either<ApiError, D>>((either: Either<ApiError, D>) =>
    either.chain(items => {
      const parsed = schema.safeParse(items);
      return parsed.success
        ? right<ApiError, z.infer<typeof schema>>(parsed.data)
        : left<ApiError, z.infer<typeof schema>>({
            title: 'API Parsing Failed',
            details: JSON.stringify(parsed.error.format())
          });
    })
  );
}
