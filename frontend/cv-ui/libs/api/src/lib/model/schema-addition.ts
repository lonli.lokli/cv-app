import { z } from 'zod';

import { DocumentContract, JobContract } from './schema-generated';

export const DocumentContracts = z.array(DocumentContract);
export const JobContracts = z.array(JobContract);
