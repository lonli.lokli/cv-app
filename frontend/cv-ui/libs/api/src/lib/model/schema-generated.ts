/**
 * Generated code, DO NOT modify directly.
 */
import { z } from 'zod';

export const CompanyContract = zodSchemaCompanyContract();
export type CompanyContract = z.infer<typeof CompanyContract>;

export const ContactContract = zodSchemaContactContract();
export type ContactContract = z.infer<typeof ContactContract>;

export const DocumentContract = zodSchemaDocumentContract();
export type DocumentContract = z.infer<typeof DocumentContract>;

export const JobContract = zodSchemaJobContract();
export type JobContract = z.infer<typeof JobContract>;

export const SalaryRangeContract = zodSchemaSalaryRangeContract();
export type SalaryRangeContract = z.infer<typeof SalaryRangeContract>;

export const __Empty = zodSchema__Empty();
export type __Empty = z.infer<typeof __Empty>;

function zodSchemaCompanyContract() {
  return z.object({
    country: z.string(),
    name: z.string(),
    town: z.string()
  });
}

function zodSchemaContactContract() {
  return z.object({
    email: z.string(),
    phone: z.string()
  });
}

function zodSchemaDocumentContract() {
  return z.object({
    author: z.string(),
    id: z.string()
  });
}

function zodSchemaJobContract() {
  return z.object({
    company: zodSchemaCompanyContract(),
    contact: zodSchemaContactContract(),
    features: z.array(z.string()),
    id: z.string(),
    occupation: z.string(),
    salary: zodSchemaSalaryRangeContract()
  });
}

function zodSchemaSalaryRangeContract() {
  return z.object({
    max: z.number().int(),
    min: z.number().int()
  });
}

function zodSchema__Empty() {
  return z.string().max(0).optional();
}
