/* tslint:disable */
/* eslint-disable */
export interface ContactContract {
  email: string;
  phone: string;
}
