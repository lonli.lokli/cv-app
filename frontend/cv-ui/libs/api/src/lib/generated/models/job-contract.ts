/* tslint:disable */
/* eslint-disable */
import { CompanyContract } from '../models/company-contract';
import { ContactContract } from '../models/contact-contract';
import { SalaryRangeContract } from '../models/salary-range-contract';
export interface JobContract {
  company: CompanyContract;
  contact: ContactContract;
  features: Array<string>;
  id: string;
  occupation: string;
  salary: SalaryRangeContract;
}
