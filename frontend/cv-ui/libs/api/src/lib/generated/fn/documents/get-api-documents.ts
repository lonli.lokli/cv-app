/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { DocumentContract } from '../../models/document-contract';

export interface GetApiDocuments$Params {

/**
 * The requested API version
 */
  'api-version'?: string;
}

export function getApiDocuments(http: HttpClient, rootUrl: string, params?: GetApiDocuments$Params, context?: HttpContext): Observable<StrictHttpResponse<Array<DocumentContract>>> {
  const rb = new RequestBuilder(rootUrl, getApiDocuments.PATH, 'get');
  if (params) {
    rb.query('api-version', params['api-version'], {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Array<DocumentContract>>;
    })
  );
}

getApiDocuments.PATH = '/api/Documents';
