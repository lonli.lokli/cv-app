/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';


export interface PostApiDocumentsDocumentId$Params {
  documentId: string;

/**
 * The requested API version
 */
  'api-version'?: string;
      body: Blob
}

export function postApiDocumentsDocumentId(http: HttpClient, rootUrl: string, params: PostApiDocumentsDocumentId$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
  const rb = new RequestBuilder(rootUrl, postApiDocumentsDocumentId.PATH, 'post');
  if (params) {
    rb.path('documentId', params.documentId, {});
    rb.query('api-version', params['api-version'], {});
    rb.body(params.body, 'multipart/form-data');
  }

  return http.request(
    rb.build({ responseType: 'text', accept: '*/*', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
    })
  );
}

postApiDocumentsDocumentId.PATH = '/api/Documents/{documentId}';
