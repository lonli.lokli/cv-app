/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';


export interface GetApiDocumentsDocumentId$Params {
  documentId: string;

/**
 * The requested API version
 */
  'api-version'?: string;
}

export function getApiDocumentsDocumentId(http: HttpClient, rootUrl: string, params: GetApiDocumentsDocumentId$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
  const rb = new RequestBuilder(rootUrl, getApiDocumentsDocumentId.PATH, 'get');
  if (params) {
    rb.path('documentId', params.documentId, {});
    rb.query('api-version', params['api-version'], {});
  }

  return http.request(
    rb.build({ responseType: 'blob', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Blob>;
    })
  );
}

getApiDocumentsDocumentId.PATH = '/api/Documents/{documentId}';
