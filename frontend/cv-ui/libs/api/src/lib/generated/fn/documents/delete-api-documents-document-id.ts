/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';


export interface DeleteApiDocumentsDocumentId$Params {
  documentId: string;

/**
 * The requested API version
 */
  'api-version'?: string;
}

export function deleteApiDocumentsDocumentId(http: HttpClient, rootUrl: string, params: DeleteApiDocumentsDocumentId$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
  const rb = new RequestBuilder(rootUrl, deleteApiDocumentsDocumentId.PATH, 'delete');
  if (params) {
    rb.path('documentId', params.documentId, {});
    rb.query('api-version', params['api-version'], {});
  }

  return http.request(
    rb.build({ responseType: 'text', accept: '*/*', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
    })
  );
}

deleteApiDocumentsDocumentId.PATH = '/api/Documents/{documentId}';
