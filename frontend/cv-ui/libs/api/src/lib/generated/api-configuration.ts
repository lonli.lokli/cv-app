import { Injectable } from '@angular/core';

/**
 * Global configuration
 */
@Injectable()
export class ApiConfiguration {
  rootUrl = 'http://example.com';
}

/**
 * Parameters for `.forRoot()`
 */
export interface ApiConfigurationParameters {
  rootUrl?: string;
}