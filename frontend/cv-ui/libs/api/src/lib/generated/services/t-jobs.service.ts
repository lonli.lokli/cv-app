/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { Observable } from 'rxjs';
import { eitherify } from '../../model/functions';
import { ServerError } from '../../model/types';
import { Either } from '@sweet-monads/either';


import { getApiJobs } from '../fn/jobs/get-api-jobs';
import { GetApiJobs$Params } from '../fn/jobs/get-api-jobs';
import { JobContract } from '../models/job-contract';

@Injectable({
  providedIn: 'root',
})
export class tJobsService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /** Path part for operation `getApiJobs()` */
  static readonly GetApiJobsPath = '/api/Jobs';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getApiJobs()` instead.
   *
   * This method doesn't expect any request body.
   */
  getApiJobs$Response(params?: GetApiJobs$Params, context?: HttpContext): Observable<StrictHttpResponse<Array<JobContract>>> {
    return getApiJobs(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getApiJobs$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getApiJobs(params?: GetApiJobs$Params, context?: HttpContext): Observable<Either<ServerError, Array<JobContract>>> {

    return this.getApiJobs$Response(params, context).pipe(
      eitherify()
    );
  }
}