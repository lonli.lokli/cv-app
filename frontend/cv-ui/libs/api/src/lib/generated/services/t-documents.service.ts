/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { Observable } from 'rxjs';
import { eitherify } from '../../model/functions';
import { ServerError } from '../../model/types';
import { Either } from '@sweet-monads/either';


import { deleteApiDocumentsDocumentId } from '../fn/documents/delete-api-documents-document-id';
import { DeleteApiDocumentsDocumentId$Params } from '../fn/documents/delete-api-documents-document-id';
import { DocumentContract } from '../models/document-contract';
import { getApiDocuments } from '../fn/documents/get-api-documents';
import { GetApiDocuments$Params } from '../fn/documents/get-api-documents';
import { getApiDocumentsDocumentId } from '../fn/documents/get-api-documents-document-id';
import { GetApiDocumentsDocumentId$Params } from '../fn/documents/get-api-documents-document-id';
import { postApiDocumentsDocumentId } from '../fn/documents/post-api-documents-document-id';
import { PostApiDocumentsDocumentId$Params } from '../fn/documents/post-api-documents-document-id';
import { putApiDocuments } from '../fn/documents/put-api-documents';
import { PutApiDocuments$Params } from '../fn/documents/put-api-documents';

@Injectable({
  providedIn: 'root',
})
export class tDocumentsService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /** Path part for operation `getApiDocumentsDocumentId()` */
  static readonly GetApiDocumentsDocumentIdPath = '/api/Documents/{documentId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getApiDocumentsDocumentId()` instead.
   *
   * This method doesn't expect any request body.
   */
  getApiDocumentsDocumentId$Response(params: GetApiDocumentsDocumentId$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return getApiDocumentsDocumentId(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getApiDocumentsDocumentId$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getApiDocumentsDocumentId(params: GetApiDocumentsDocumentId$Params, context?: HttpContext): Observable<Either<ServerError, Blob>> {

    return this.getApiDocumentsDocumentId$Response(params, context).pipe(
      eitherify()
    );
  }
  /** Path part for operation `postApiDocumentsDocumentId()` */
  static readonly PostApiDocumentsDocumentIdPath = '/api/Documents/{documentId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `postApiDocumentsDocumentId()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  postApiDocumentsDocumentId$Response(params: PostApiDocumentsDocumentId$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return postApiDocumentsDocumentId(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `postApiDocumentsDocumentId$Response()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  postApiDocumentsDocumentId(params: PostApiDocumentsDocumentId$Params, context?: HttpContext): Observable<Either<ServerError, void>> {

    return this.postApiDocumentsDocumentId$Response(params, context).pipe(
      eitherify()
    );
  }
  /** Path part for operation `deleteApiDocumentsDocumentId()` */
  static readonly DeleteApiDocumentsDocumentIdPath = '/api/Documents/{documentId}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteApiDocumentsDocumentId()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteApiDocumentsDocumentId$Response(params: DeleteApiDocumentsDocumentId$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return deleteApiDocumentsDocumentId(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deleteApiDocumentsDocumentId$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteApiDocumentsDocumentId(params: DeleteApiDocumentsDocumentId$Params, context?: HttpContext): Observable<Either<ServerError, void>> {

    return this.deleteApiDocumentsDocumentId$Response(params, context).pipe(
      eitherify()
    );
  }
  /** Path part for operation `getApiDocuments()` */
  static readonly GetApiDocumentsPath = '/api/Documents';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getApiDocuments()` instead.
   *
   * This method doesn't expect any request body.
   */
  getApiDocuments$Response(params?: GetApiDocuments$Params, context?: HttpContext): Observable<StrictHttpResponse<Array<DocumentContract>>> {
    return getApiDocuments(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getApiDocuments$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getApiDocuments(params?: GetApiDocuments$Params, context?: HttpContext): Observable<Either<ServerError, Array<DocumentContract>>> {

    return this.getApiDocuments$Response(params, context).pipe(
      eitherify()
    );
  }
  /** Path part for operation `putApiDocuments()` */
  static readonly PutApiDocumentsPath = '/api/Documents';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `putApiDocuments()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  putApiDocuments$Response(params: PutApiDocuments$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return putApiDocuments(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `putApiDocuments$Response()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  putApiDocuments(params: PutApiDocuments$Params, context?: HttpContext): Observable<Either<ServerError, void>> {

    return this.putApiDocuments$Response(params, context).pipe(
      eitherify()
    );
  }
}