/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpContext } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { Observable } from 'rxjs';
import { eitherify } from '../../model/functions';
import { ServerError } from '../../model/types';
import { Either } from '@sweet-monads/either';


import { getInfo } from '../fn/info/get-info';
import { GetInfo$Params } from '../fn/info/get-info';

@Injectable({
  providedIn: 'root',
})
export class tInfoService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /** Path part for operation `getInfo()` */
  static readonly GetInfoPath = '/Info';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getInfo()` instead.
   *
   * This method doesn't expect any request body.
   */
  getInfo$Response(params?: GetInfo$Params, context?: HttpContext): Observable<StrictHttpResponse<{
[key: string]: any;
}>> {
    return getInfo(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getInfo$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getInfo(params?: GetInfo$Params, context?: HttpContext): Observable<Either<ServerError, {
[key: string]: any;
}>> {

    return this.getInfo$Response(params, context).pipe(
      eitherify()
    );
  }
}