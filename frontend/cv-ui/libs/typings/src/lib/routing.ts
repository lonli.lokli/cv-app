//type Routed<T> = T extends { route: infer R } ? R : never;
export type View<T> = T extends { view: infer U } ? U : never;

export type ColleagueRequest = {
  // I want to change job
  path: 'request';
  view: 'details';
  id?: number;
};
export type ColleagueRequire = {
  // I already working
  path: 'require';
  view: 'details';
  id?: number;
};

export type colleagueRoute = {
  tag: 'job';
  route: ColleagueRequest | ColleagueRequire;
  state?: { [k: string]: any };
};

export type applicationRoute = colleagueRoute;

export type Section<T> = T extends {
  route: {
    path: infer U;
  };
}
  ? U
  : never;

export type ColleagueSection = Section<colleagueRoute>;

export type ModuleType = 'documents' | 'colleague' | 'about';
