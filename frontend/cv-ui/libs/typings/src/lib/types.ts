export type NotEmptyArray<T> = [T, ...T[]];

export type CvErrorCode = 'external_failure';
export type CvSingleError = {
  code: CvErrorCode;
  friendlyMessage: string;
  details?: Error;
};

export type CvErrors = NotEmptyArray<CvSingleError>;

export interface LoginInfo {
  userName: string;
  password: string;
}

export type ApiDefinition<A, D> = (...parameters: A[]) => D;

export type Nullable<T> = T | undefined | null;
