import { Effect, createStore, createEffect, attach } from 'effector';

export function createSingleEffect<Parameters, Done>(
  handler: (params: Parameters, signal: AbortSignal) => Promise<Done>
): Effect<Parameters, Done> {
  const $signal = createStore(new AbortController());

  const requestFx = createEffect(async (parameters: Parameters) => {
    const { signal } = await abortFx();
    return handler(parameters, signal);
  });

  const abortFx = attach({
    source: $signal,
    effect(ctrl) {
      ctrl.abort();
      return new AbortController();
    }
  });

  $signal.on(abortFx.doneData, (_, ctrl) => ctrl);

  return requestFx;
}
