import { HttpContextToken } from '@angular/common/http';

export const NOTIFY_ERROR = new HttpContextToken(() => true);
