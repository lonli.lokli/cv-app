import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';

export class RouteHelper {
  public static getActiveModuleName(route: ActivatedRoute | ActivatedRouteSnapshot) {
    return this.getPath(route.pathFromRoot[2]);
  }

  public static getActiveSubModuleName(route: ActivatedRoute | ActivatedRouteSnapshot) {
    return this.getPath(route.pathFromRoot[4]);
  }

  public static getChildModuleName(route: ActivatedRoute | ActivatedRouteSnapshot) {
    return this.getPath(route.firstChild && route.firstChild?.firstChild);
  }

  private static getPath(route: ActivatedRoute | ActivatedRouteSnapshot | null) {
    return route ? (route?.routeConfig?.path as string) : undefined;
  }
}
