import { Directive, Input, ViewContainerRef, TemplateRef } from '@angular/core';
import { Either } from '@sweet-monads/either';

import { initialIfContext, initialReferences, IfContext, assertTemplate, updateView } from './common';

@Directive({ selector: '[cvIfRight]', standalone: true })
export class IfRightDirective<TE = unknown, TD = unknown> {
  private context: IfContext<TE | TD> = initialIfContext();

  private refs = initialReferences();

  constructor(viewContainer: ViewContainerRef, templateReference: TemplateRef<IfContext<TE | TD>>) {
    this.refs.viewContainer = viewContainer;
    this.refs.thenTemplateRef = templateReference;
  }

  @Input()
  set cvIfRight(either: Either<TE, TD> | null) {
    if (either) {
      either
        .mapLeft(l => {
          this.context.ifTrue = false;
          this.context.$implicit = l;
        })
        .mapRight(r => {
          this.context.ifTrue = true;
          this.context.$implicit = r;
        });
    }
    updateView(this.context, this.refs);
  }

  @Input()
  set cvIfRightThen(templateReference: TemplateRef<IfContext<TE | TD>> | null) {
    assertTemplate('cvIfRightThen', templateReference);
    this.refs.thenTemplateRef = templateReference;
    this.refs.thenViewRef = null;
    updateView(this.context, this.refs);
  }

  @Input()
  set cvIfRightElse(templateReference: TemplateRef<IfContext<TE | TD>> | null) {
    assertTemplate('cvIfRightElse', templateReference);
    this.refs.elseTemplateRef = templateReference;
    this.refs.elseViewRef = null;
    updateView(this.context, this.refs);
  }

  static ngTemplateGuard_cvIfRight: 'binding';

  static ngTemplateContextGuard<TE, TD>(
    _directive: IfRightDirective<TE, TD>,
    _context: any
  ): _context is IfContext<Exclude<TD, false | 0 | '' | null | undefined>> {
    return true;
  }
}

@Directive({ selector: '[cvIfLeft]', standalone: true })
export class IfLeftDirective<TE = unknown, TD = unknown> {
  private context: IfContext<TE | TD> = initialIfContext();

  private refs = initialReferences();

  constructor(viewContainer: ViewContainerRef, templateReference: TemplateRef<IfContext<TE | TD>>) {
    this.refs.viewContainer = viewContainer;
    this.refs.thenTemplateRef = templateReference;
  }

  @Input()
  set cvIfLeft(either: Either<TE, TD> | null) {
    if (either) {
      either
        .mapLeft(l => {
          this.context.ifTrue = true;
          this.context.$implicit = l;
        })
        .mapRight(r => {
          this.context.ifTrue = false;
          this.context.$implicit = r;
        });
    }
    updateView(this.context, this.refs);
  }

  @Input()
  set cvIfLeftThen(templateReference: TemplateRef<IfContext<TE | TD>> | null) {
    assertTemplate('cvIfLeftThen', templateReference);
    this.refs.thenTemplateRef = templateReference;
    this.refs.thenViewRef = null;
    updateView(this.context, this.refs);
  }

  @Input()
  set cvIfLeftElse(templateReference: TemplateRef<IfContext<TE | TD>> | null) {
    assertTemplate('cvIfLeftElse', templateReference);
    this.refs.elseTemplateRef = templateReference;
    this.refs.elseViewRef = null;
    updateView(this.context, this.refs);
  }

  static ngTemplateGuard_cvIfLeft: 'binding';

  static ngTemplateContextGuard<TE, TD>(_directive: IfLeftDirective<TE, TD>, _context: any): _context is IfContext<TE> {
    return true;
  }
}
