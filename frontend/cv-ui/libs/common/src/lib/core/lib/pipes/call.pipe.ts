import { EmbeddedViewRef, Pipe, PipeTransform, ChangeDetectorRef } from '@angular/core';

type Method<T = any> = (this: T, ...parameters: any[]) => any;
type Head<T extends Method> = Parameters<T>[0];
type Tail<T extends Method> = T extends (first: any, ...rest: infer R) => any ? R : never;

@Pipe({ name: 'call', standalone: true })
export class CallPipe<C> implements PipeTransform {
  private readonly context: C;

  constructor(private cd: ChangeDetectorRef) {
    this.context = (this.cd as EmbeddedViewRef<C>).context;
  }

  transform<M extends Method<C>>(head: Head<M>, mapper: M, ...parameters: Tail<M>): ReturnType<M> {
    return mapper.apply(this.context, [head, ...parameters]);
  }
}
