import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

export class LetContext<T = unknown> {
  public $implicit: T = null!;
  public cvLet: T = null!;
}

@Directive({
  selector: '[cvLet]',
  standalone: true
})
export class LetDirective<T = unknown> {
  private _context: LetContext<T> = new LetContext<T>();

  constructor(viewContainer: ViewContainerRef, templateReference: TemplateRef<LetContext<T>>) {
    viewContainer.createEmbeddedView(templateReference, this._context);
  }

  @Input()
  set cvLet(value: T) {
    this._context.$implicit = this._context.cvLet = value;
  }

  static ngTemplateGuard_clLet: 'binding';
  static ngTemplateContextGuard<T>(
    _directive: LetDirective<T>,
    _context: any
  ): _context is LetContext<Exclude<T, false | 0 | '' | null | undefined>> {
    return true;
  }
}
