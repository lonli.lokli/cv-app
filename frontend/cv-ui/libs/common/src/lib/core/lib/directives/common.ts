import { EmbeddedViewRef, TemplateRef, ViewContainerRef, ɵstringify as stringify } from '@angular/core';

export interface IfContext<T = unknown> {
  $implicit: T;
  ifTrue: boolean;
}

export const initialIfContext = <T>(): IfContext<T> => ({
  $implicit: null!,
  ifTrue: false
});

export interface References {
  viewContainer: ViewContainerRef | null;
  thenTemplateRef: TemplateRef<IfContext> | null;
  elseTemplateRef: TemplateRef<IfContext> | null;
  thenViewRef: EmbeddedViewRef<IfContext> | null;
  elseViewRef: EmbeddedViewRef<IfContext> | null;
}
export const initialReferences = (): References => ({
  viewContainer: null,
  thenTemplateRef: null,
  elseTemplateRef: null,
  thenViewRef: null,
  elseViewRef: null
});

export function updateView(context: IfContext, references: References) {
  if (context.ifTrue) {
    if (!references.thenViewRef && references.viewContainer) {
      references.viewContainer.clear();
      references.elseViewRef = null;
      if (references.thenTemplateRef) {
        references.thenViewRef = references.viewContainer.createEmbeddedView(references.thenTemplateRef, context);
      }
    }
  } else {
    if (!references.elseViewRef && references.viewContainer) {
      references.viewContainer.clear();
      references.thenViewRef = null;
      if (references.elseTemplateRef) {
        references.elseViewRef = references.viewContainer.createEmbeddedView(references.elseTemplateRef, context);
      }
    }
  }
}

export function assertTemplate(property: string, templateReference: TemplateRef<any> | null): void {
  const isTemplateReferenceOrNull = !!(!templateReference || templateReference.createEmbeddedView);
  if (!isTemplateReferenceOrNull) {
    throw new Error(`${property} must be a TemplateRef, but received '${stringify(templateReference)}'.`);
  }
}
