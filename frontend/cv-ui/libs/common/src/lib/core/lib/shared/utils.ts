import { NgZone } from '@angular/core';
import { first } from 'rxjs';

export const createGuid = () =>
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replaceAll(/[xy]/g, function (c) {
    const r = Math.trunc(Math.random() * 16),
      v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });

export const executeOnStable = (ngZone: NgZone, action: () => any): void => {
  if (ngZone.isStable) {
    action();
  } else {
    ngZone.onStable.asObservable().pipe(first()).subscribe(action);
  }
};
