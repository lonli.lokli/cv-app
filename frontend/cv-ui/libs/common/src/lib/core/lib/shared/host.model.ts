import { InjectionToken } from '@angular/core';
import { z } from 'zod';

export const HostSettings = z.object({
  api: z.string({ required_error: 'Api endpoint is not specified' }).min(1)
});

export type HostSettings = z.infer<typeof HostSettings>;
export const HOST_SETTINGS = new InjectionToken<HostSettings>('Host Settings');

export class BuildSettings {
  constructor(
    public commit: string,
    public time: string
  ) {}
}
