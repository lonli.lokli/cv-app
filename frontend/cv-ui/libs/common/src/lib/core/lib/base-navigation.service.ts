import { Title } from '@angular/platform-browser';
import { ActivatedRouteSnapshot, Event, NavigationEnd, Params, Router } from '@angular/router';
import { Observable, Subject, filter, shareReplay } from 'rxjs';

import { isDefined } from '@cv/common/utils';
import { ModuleType } from '@cv/typings';

export class BaseStateParameter<TSection, TView> {
  section?: TSection;
  view?: TView;
  title?: string;
  id?: number;

  query?: Params;
  navigationData?: any;
  state?: any;
}

export abstract class BaseNavigationService<
  TSection extends string,
  TView extends string,
  TStateParameters extends BaseStateParameter<TSection, TView> = BaseStateParameter<TSection, TView>
> {
  private _stateParams: Subject<TStateParameters> = new Subject<TStateParameters>();
  private _stateNavigationData = undefined;

  public stateParams$ = this._stateParams.pipe(shareReplay(1));

  constructor(
    private router: Router,
    private title: Title,
    public readonly moduleName: ModuleType
  ) {
    this.onNavigationEnd.subscribe(() => {
      const params = this.getParams();
      params.navigationData = this._stateNavigationData;
      this._stateNavigationData = undefined;
      this.title.setTitle(params.title ? `${params.title} | Fair CV` : 'Fair CV');
      this._stateParams.next(params);
    });
  }

  protected get onNavigationEnd(): Observable<Event | NavigationEnd> {
    return this.router.events.pipe(
      filter(event => event instanceof NavigationEnd && event.url.startsWith(`/${this.moduleName}`))
    );
  }

  public navigate(cmd: string[], query?: any, stateNavigationData?: any) {
    this._stateNavigationData = stateNavigationData;
    return this.router.navigate(cmd, { queryParams: query });
  }

  public maybeNavigate(section: TSection) {
    if (this.getParams().section) {
      return;
    }

    this.navigateTo(section, undefined, undefined);
  }

  public navigateToSection(section: TSection, id?: number) {
    if (this.getParams().section === section && !id) {
      return;
    }

    const query = typeof id === 'number' ? { id } : undefined;
    this.navigateTo(section, undefined, query);
  }

  public navigateToSectionWithCurrentView(section: TSection, id?: number) {
    const currentParameters = this.getParams();
    if (currentParameters.section === section && !id) {
      return;
    }

    const query = typeof id === 'number' ? { id } : undefined;
    this.navigateTo(section, currentParameters.view, query);
  }

  public navigateToView(view?: TView) {
    const currentParameters = this.getParams(); // preserve current routes
    if (currentParameters.view === view) {
      return;
    }

    this.navigateTo(currentParameters.section, view, currentParameters.query);
  }

  public navigateToId(id?: number) {
    const currentParameters = this.getParams(); // preserve current section
    if (currentParameters.id === id) {
      return;
    }
    const query = typeof id === 'number' ? { id } : undefined;
    this.navigateTo(currentParameters.section, currentParameters.view, query);
  }

  public navigateTo(section?: TSection, view?: TView, query?: any) {
    const cmd: string[] = [this.moduleName];
    if (section) {
      cmd.push(section);
    }
    if (view) {
      cmd.push(view);
    }

    this.navigate(cmd, query);
  }

  /**
   * Returns all state parameters using current router state (from root, including all childrens).
   */
  protected getParams(): TStateParameters {
    return this.collectAllParams({} as any, this.router.routerState.snapshot.root);
  }

  private collectAllParams(params: TStateParameters, state: ActivatedRouteSnapshot): TStateParameters {
    if (state.params) {
      for (const property of Object.getOwnPropertyNames(state.params)) {
        params[property as keyof TStateParameters] = state.params[property];
      }
    }
    if (state.data) {
      for (const property of Object.getOwnPropertyNames(state.data)) {
        params[property as keyof TStateParameters] = state.data[property];
      }
    }
    const currentNavigation = this.router.getCurrentNavigation();
    if (currentNavigation?.extras?.state) {
      params['state'] = currentNavigation?.extras?.state;
    }
    for (const child of state.children) {
      this.collectAllParams(params, child);
    }
    params.query = state.queryParams;
    if (isDefined(params.query['id'])) {
      params.id = +params.query['id'];
    }
    return params;
  }
}
