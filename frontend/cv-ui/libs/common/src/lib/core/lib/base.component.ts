import { Directive, Injectable, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { defer, filter, map, merge, Observable, of, Subject, Unsubscribable } from 'rxjs';

import { createGuid } from './shared/utils';

@Injectable()
export abstract class BaseSubscribable implements OnDestroy {
  protected destroyed = false;
  protected _subscriptions: { [key: string]: Unsubscribable } = {};

  public ngOnDestroy() {
    this.clearSubsscriptions();
    this.destroyed = true;
  }

  protected addSubscription(subscription: Unsubscribable | undefined, key?: string) {
    if (key) {
      this.removeSubscription(key);
    } else {
      key = createGuid();
    }
    if (subscription !== undefined) {
      this._subscriptions[key] = subscription;
    }
  }

  protected removeSubscription(key: string) {
    if (this._subscriptions[key]) {
      this._subscriptions[key].unsubscribe();
      delete this._subscriptions[key];
    }
  }

  protected clearSubsscriptions() {
    for (const key in this._subscriptions) {
      if (this._subscriptions[key]) {
        this._subscriptions[key].unsubscribe();
      }
    }
  }
}

@Directive()
export class BaseDirective<TActual extends BaseDirective<TActual> = BaseDirective<any>>
  extends BaseSubscribable
  implements OnChanges
{
  private _changes$: Subject<SimpleChanges> | undefined;

  public ngOnChanges(changes: SimpleChanges) {
    if (this._changes$) {
      this._changes$.next(changes);
    }
  }

  protected observeInputProperty<TKey extends keyof TActual>(
    propertyName: TKey,
    includeCurrent?: boolean
  ): Observable<TActual[TKey]> {
    if (!this._changes$) {
      this._changes$ = new Subject<SimpleChanges>();
    }

    const changes$ = this._changes$.pipe(
      filter(changes => changes.hasOwnProperty(propertyName)),
      map(changes => changes[String(propertyName)].currentValue as TActual[TKey])
    );
    return includeCurrent
      ? merge(
          defer(() => of((this as unknown as TActual)[propertyName])),
          changes$
        )
      : changes$;
  }

  protected addInputPropertySubscription<TKey extends keyof TActual>(
    propertyName: TKey,
    subscribeAction: (currentValue: TActual[TKey]) => Unsubscribable
  ) {
    const subscriptionKey = `${propertyName as string}Subscription`;
    this.observeInputProperty(propertyName).subscribe(value => {
      this.addSubscription(value ? subscribeAction(value) : undefined, subscriptionKey);
    });
  }
}

export class BaseComponent<
  TActual extends BaseComponent<TActual> = BaseComponent<any>
> extends BaseDirective<TActual> {}
