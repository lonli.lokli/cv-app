import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { Maybe } from '@sweet-monads/maybe';

import { initialIfContext, initialReferences, IfContext, assertTemplate, updateView } from './common';

@Directive({ selector: '[cvIfSome]', standalone: true })
export class IfSomeDirective<T = unknown> {
  private context: IfContext<T> = initialIfContext();

  private refs = initialReferences();

  constructor(viewContainer: ViewContainerRef, templateReference: TemplateRef<IfContext<T>>) {
    this.refs.viewContainer = viewContainer;
    this.refs.thenTemplateRef = templateReference;
  }

  @Input()
  set cvIfSome(option: Maybe<T> | null) {
    if (option && option.isJust()) {
      this.context.ifTrue = true;
      this.context.$implicit = option.value;
    } else {
      this.context.ifTrue = false;
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      this.context.$implicit = null!;
    }
    updateView(this.context, this.refs);
  }

  @Input()
  set cvIfSomeThen(templateReference: TemplateRef<IfContext<T>> | null) {
    assertTemplate('cvIfSomeThen', templateReference);
    this.refs.thenTemplateRef = templateReference;
    this.refs.thenViewRef = null;
    updateView(this.context, this.refs);
  }

  @Input()
  set cvIfSomeElse(templateReference: TemplateRef<IfContext<T>> | null) {
    assertTemplate('cvIfSomeElse', templateReference);
    this.refs.elseTemplateRef = templateReference;
    this.refs.elseViewRef = null;
    updateView(this.context, this.refs);
  }

  static ngTemplateGuard_ifSome: 'binding';

  static ngTemplateContextGuard<T>(
    _directive: IfSomeDirective<T>,
    _context: any
  ): _context is IfContext<Exclude<T, false | 0 | '' | null | undefined>> {
    return true;
  }
}

@Directive({ selector: '[cvIfNone]', standalone: true })
export class IfNoneDirective<T = unknown> {
  private context: IfContext<T> = initialIfContext();

  private refs = initialReferences();

  constructor(viewContainer: ViewContainerRef, templateReference: TemplateRef<IfContext<T>>) {
    this.refs.viewContainer = viewContainer;
    this.refs.thenTemplateRef = templateReference;
  }

  @Input()
  set cvIfNone(option: Maybe<T> | null) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    this.context.$implicit = null!;
    this.context.ifTrue = option === null || option.isNone() ? true : false;
    updateView(this.context, this.refs);
  }

  @Input()
  set cvIfNoneThen(templateReference: TemplateRef<IfContext<T>> | null) {
    assertTemplate('cvIfNoneThen', templateReference);
    this.refs.thenTemplateRef = templateReference;
    this.refs.thenViewRef = null;
    updateView(this.context, this.refs);
  }

  @Input()
  set cvIfNoneElse(templateReference: TemplateRef<IfContext<T>> | null) {
    assertTemplate('cvIfNoneElse', templateReference);
    this.refs.elseTemplateRef = templateReference;
    this.refs.elseViewRef = null;
    updateView(this.context, this.refs);
  }
}
