import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cvNot',
  standalone: true
})
export class NotPipe implements PipeTransform {
  transform(value: any, strict?: boolean): any {
    // with strict we should compare value with false, without double negation
    return strict === true ? value === false : !value;
  }
}
