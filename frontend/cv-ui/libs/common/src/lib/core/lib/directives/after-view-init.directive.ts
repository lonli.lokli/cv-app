import { Directive, AfterViewInit, Output, EventEmitter, ElementRef } from '@angular/core';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Directive({
  selector: '[cvAfterViewInit], [cvAfterViewInitDelay]',
  standalone: true
})
export class AfterViewInitDirective implements AfterViewInit {
  @Output()
  public cvAfterViewInit = new EventEmitter<ElementRef>();

  @Output()
  public cvAfterViewInitDelay = new EventEmitter<ElementRef>();

  constructor(private elementReference: ElementRef) {}

  public ngAfterViewInit() {
    this.cvAfterViewInit.next(this.elementReference);
    of(true)
      .pipe(delay(0))
      .subscribe(() => this.cvAfterViewInitDelay.next(this.elementReference));
  }
}
