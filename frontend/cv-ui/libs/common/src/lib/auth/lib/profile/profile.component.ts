import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { TippyDirective } from '@ngneat/helipopper';
import { filter } from 'rxjs';

import { ButtonComponent } from '@cv/common/button';
import { IconComponent } from '@cv/common/icons';
import { LetDirective } from '@cv/common/core';
import { isDefined } from '@cv/common/utils';
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'cv-profile',
  standalone: true,
  imports: [IconComponent, TippyDirective, ButtonComponent, LetDirective, AsyncPipe],
  template: `<cv-icon name="person" [size]="24" [tippy]="tpl" variation="popper"></cv-icon>

    <ng-template #tpl>
      <ng-container *cvLet="user$ | async as user">
        <div>Name: {{ user?.name }}</div>
        <div>E-mail: {{ user?.email }}</div>
      </ng-container>
      <button cv-button (buttonClick)="logout()" label="Logout" class="cv-btn-primary"></button>
    </ng-template>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent {
  #authSvc = inject(AuthService);
  public user$ = this.#authSvc.user$.pipe(filter(isDefined));

  logout() {
    this.#authSvc.logout({
      logoutParams: {
        returnTo: window.location.origin
      }
    });
  }
}
