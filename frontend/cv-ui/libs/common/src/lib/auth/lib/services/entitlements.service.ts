import { inject, Injectable } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { isDefined } from '@cv/common/utils';
import { ModuleType } from '@cv/typings';

export interface CvUserEntitlementsData {
  Modules: { [name: string]: CvUserModuleEntitlementsData };
}

export interface CvUserModuleEntitlementsData {
  Role: UserRole;
  RestrictLevel: number;
}
export type UserRole = 'Viewer' | 'User' | 'Admin';

@Injectable({
  providedIn: 'root'
})
export class EntitlementsService {
  private roleClaim = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role';
  private docsModule: ModuleType = 'documents';
  private collleagueModule: ModuleType = 'colleague';
  private aboutModule: ModuleType = 'about';
  private authService = inject(AuthService);

  public getUserEntitlements(): Observable<CvUserEntitlementsData> {
    return this.authService.idTokenClaims$.pipe(
      map(idToken => {
        const roles = isDefined(idToken)
          ? Array.isArray(idToken[this.roleClaim])
            ? (idToken[this.roleClaim] as string[])
            : []
          : [];
        const entitlements: CvUserEntitlementsData = {
          Modules: {}
        };

        const addModuleRole = (moduleType: ModuleType, role: UserRole, restrictLevel = 0) => {
          const module = entitlements.Modules[moduleType];
          if (!module || module.Role < role) {
            entitlements.Modules[moduleType] = {
              Role: role,
              RestrictLevel: restrictLevel
            };
          } else if (module.Role === role) {
            module.RestrictLevel = Math.min(module.RestrictLevel, restrictLevel);
          }
        };

        for (const role of roles) {
          switch (role?.toUpperCase()) {
            case 'ADMIN':
            case 'USER':
              addModuleRole(this.docsModule, 'User');
              addModuleRole(this.collleagueModule, 'User');
              break;
            default:
              console.error(`Unknown role: '${role?.toUpperCase()}'`);
              break;
          }
        }

        // Always available modules
        addModuleRole(this.aboutModule, 'User');

        return entitlements;
      })
    );
  }
}
