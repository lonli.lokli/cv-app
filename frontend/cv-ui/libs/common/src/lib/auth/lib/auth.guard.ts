import { inject, InjectionToken } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { DialogService } from '@ngneat/dialog';
import { Observable, map } from 'rxjs';

import { CvUserEntitlementsData, EntitlementsService } from './services/entitlements.service';

export const CAN_ACTIVATE_GUARD = new InjectionToken<(route: ActivatedRouteSnapshot) => Observable<boolean> | boolean>(
  'CanActivate Guard',
  {
    providedIn: 'root',
    factory:
      () =>
      (route: ActivatedRouteSnapshot): Observable<boolean> | boolean => {
        const moduleName = route.pathFromRoot[1].routeConfig?.path ?? '';

        const canAccess = (entls: CvUserEntitlementsData, module: string) => module === '' || !!entls.Modules[module];

        return inject(EntitlementsService)
          .getUserEntitlements()
          .pipe(
            map(entitlements => {
              if (Object.keys(entitlements.Modules).length === 0) {
                inject(DialogService).error({
                  title: 'Not authorized',
                  body: 'You are not authorized to access this application'
                });
              }
              return canAccess(entitlements, moduleName);
            })
          );
      }
  }
);
