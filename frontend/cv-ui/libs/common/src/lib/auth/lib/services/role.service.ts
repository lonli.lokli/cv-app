import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, distinctUntilChanged } from 'rxjs/operators';

import { EntitlementsService, UserRole } from './entitlements.service';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private entlService = inject(EntitlementsService);

  public hasPermission(role: UserRole, module: string): Observable<boolean> {
    return this.entlService.getUserEntitlements().pipe(
      map(entitlements => {
        const data = entitlements.Modules[module];
        return data && data.Role >= role;
      }),
      distinctUntilChanged()
    );
  }
}
