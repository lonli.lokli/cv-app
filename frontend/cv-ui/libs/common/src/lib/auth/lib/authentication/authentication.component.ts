import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

import { ButtonComponent } from '@cv/common/button';
import { NotPipe } from '@cv/common/core';
import { AsyncPipe, NgIf } from '@angular/common';

@Component({
  selector: 'cv-authentication',
  standalone: true,
  imports: [ButtonComponent, NotPipe, AsyncPipe, NgIf],
  template: `<div *ngIf="isLoggedIn$ | async | cvNot: true">
    <button cv-button label="Login/Register" (buttonClick)="onLogin()" class="cv-btn-secondary"></button>
  </div>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthenticationComponent {
  #authSvc = inject(AuthService);
  public isLoggedIn$ = this.#authSvc.isAuthenticated$;

  onLogin() {
    this.#authSvc.loginWithRedirect();
  }
}
