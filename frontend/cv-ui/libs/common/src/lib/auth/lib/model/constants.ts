export type ErrorCodes = 'login_required' | 'external_failure' | 'unknown';
export type Errors = {
  code: ErrorCodes;
  details: string;
};

export const mapStringToErrorCode = (input: string): ErrorCodes => {
  switch (input) {
    case 'login_required':
      return 'login_required';
    case 'external_failure':
      return 'external_failure';
    default:
      return 'unknown';
  }
};
