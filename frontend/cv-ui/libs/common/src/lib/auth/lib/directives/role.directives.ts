import { Directive, ElementRef, inject, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BaseComponent, RouteHelper } from '@cv/common/core';

import { UserRole } from '../services/entitlements.service';
import { RoleService } from '../services/role.service';

@Directive({
  selector: '[cvRole]',
  standalone: true
})
export class RoleDirective extends BaseComponent implements OnInit {
  @Input()
  public cvRole: UserRole = 'User';

  #elementReference = inject(ElementRef);
  #roleService = inject(RoleService);
  #route = inject(ActivatedRoute);

  protected applyPermissions(moduleType: string) {
    const role = this.cvRole ?? 'User';
    this.addSubscription(
      this.#roleService.hasPermission(role, moduleType).subscribe(granted => {
        this.#elementReference.nativeElement.style.display = granted ? '' : 'none';
      })
    );
  }

  public ngOnInit() {
    const route = RouteHelper.getActiveModuleName(this.#route);
    if (route) {
      this.applyPermissions(route);
    }
  }
}
