import { z } from 'zod';

export const AuthSettings = z.object({
  endpoint: z.string({ required_error: 'Auth endpoint is not specified' }).min(1),
  audience: z.string({ required_error: 'Auth audience is not specified' }).min(1),
  clientId: z.string({ required_error: 'Auth clientId is not specified' }).min(1)
});

export type AuthSettings = z.infer<typeof AuthSettings>;
