import { ChangeDetectionStrategy, ChangeDetectorRef, Component, HostBinding, Input, OnChanges } from '@angular/core';
import { from, lastValueFrom } from 'rxjs';
import { INode, parse, stringify } from 'svgson';

import { SafeHtmlPipe } from '@cv/common/core';
import { mapEither, mapEitherToValue, tryCatch } from '@cv/fp';

import { completeIconSet, CvIconType } from '../cv-icon.model';
import { NgIf } from '@angular/common';

const defaultIconColor = 'red'; // todo: use another color

@Component({
  selector: 'cv-icon',
  standalone: true,
  imports: [SafeHtmlPipe, NgIf],
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent implements OnChanges {
  private rawSvg = '';

  public svgData = '';

  @Input()
  set name(iconName: CvIconType) {
    this.rawSvg = completeIconSet.find(icon => icon.name === iconName)?.data ?? '';
  }

  @Input()
  public color: string | undefined;

  @Input()
  public coloringMode: 'fill' | 'stroke' = 'fill';

  @Input()
  public size = 16;

  @HostBinding('attr.tabindex')
  @Input()
  public tabIndex = 0;

  constructor(private changeDetectorReference: ChangeDetectorRef) {}

  async ngOnChanges() {
    if (this.rawSvg) {
      this.svgData = await lastValueFrom(
        from(convertSvgToJson(this.rawSvg)()).pipe(
          mapEither(clearObsoleteAttribute),
          mapEither(node => addColorAttribute(node, this.color ?? defaultIconColor, this.coloringMode)),
          mapEither(node => addSizeAttribute(node, this.size)),
          mapEither(convertJsonToSvg),
          mapEitherToValue(l => l.message)
        )
      );
    }

    this.changeDetectorReference.markForCheck();
  }
}

const convertSvgToJson = (originalSvg: string) =>
  tryCatch(
    () => parse(originalSvg),
    reason => new Error(`${reason}`)
  );

const addColorAttribute = (node: INode, color: string, mode: 'fill' | 'stroke') => {
  node.attributes[mode] = color;
  return node;
};

const clearObsoleteAttribute = (node: INode) => {
  delete node.attributes['height'];
  delete node.attributes['width'];
  delete node.attributes['fill'];
  delete node.attributes['stroke'];
  return node;
};

const addSizeAttribute = (node: INode, size: number) => {
  node.attributes['class'] = `size_${size}`;
  return node;
};

const convertJsonToSvg = (node: INode) => stringify(node);
