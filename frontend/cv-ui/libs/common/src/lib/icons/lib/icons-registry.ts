import { Injectable } from '@angular/core';

import { CvIcon, CvIconType } from './cv-icon.model';

@Injectable({
  providedIn: 'root'
})
export class IconsRegistry {
  private registry = new Map<string, string>();

  public registerIcons(icons: CvIcon[]) {
    icons.forEach(icon => this.registry.set(icon.name, icon.data));
  }

  public getIcon(iconName: CvIconType): string {
    if (!this.registry.has(iconName)) {
      console.warn(`We could not find the Icon with the name ${iconName}, did you add it to the Icon registry?`);
    }
    return this.registry.get(iconName) ?? '';
  }
}
