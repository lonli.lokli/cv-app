import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { ApiError } from '@cv/api';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  constructor(private toastr: ToastrService) {}
  public showError(error: string) {
    this.toastr.error(error);
  }

  public showApiError(error: ApiError) {
    this.toastr.error(error.details, error.title);
  }
}
