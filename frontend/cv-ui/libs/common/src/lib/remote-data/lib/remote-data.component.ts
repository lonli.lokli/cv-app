import { NgSwitch, NgSwitchCase } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';

import { ApiError } from '@cv/api';
import { ButtonComponent } from '@cv/common/button';
import { IconComponent } from '@cv/common/icons';
import { ProgressComponent } from '@cv/common/progress';
import { Result } from '@lonli-lokli/ts-result';

import { IfFailureDirective } from './directives/if-failure.directive';
import { IfInitialDirective } from './directives/if-initial.directive';
import { IfPendingDirective } from './directives/if-pending.directive';

@Component({
  selector: 'cv-remote-data',
  standalone: true,
  styleUrls: ['./remote-data.component.scss'],
  templateUrl: './remote-data.component.html',
  imports: [
    ProgressComponent,
    IconComponent,
    IfInitialDirective,
    IfPendingDirective,
    IfFailureDirective,
    ButtonComponent,
    NgSwitch,
    NgSwitchCase
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoteDataComponent<D> {
  @Input()
  public data: Result<ApiError, D> | null = null;

  @Input()
  initialText = 'There is no info to display.';

  @Output()
  public refresh = new EventEmitter<void>();

  public initialRef: TemplateRef<any> | null = null;
  public failureRef: TemplateRef<any> | null = null;
}
