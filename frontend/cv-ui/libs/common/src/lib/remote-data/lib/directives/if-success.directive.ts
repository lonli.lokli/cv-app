import { Directive, Input, ViewContainerRef, TemplateRef } from '@angular/core';

import { isFailure, isSuccess, Result } from '@lonli-lokli/ts-result';

import { initialIfContext, initialReferences, IfContext, updateView } from './common';

@Directive({ selector: '[cvIfSuccess]', standalone: true })
export class IfSuccessDirective<TE = unknown, TD = unknown> {
  private context: IfContext<TE | TD> = initialIfContext();

  private refs = initialReferences();

  constructor(viewContainer: ViewContainerRef, templateReference: TemplateRef<IfContext<TE | TD>>) {
    this.refs.viewContainer = viewContainer;
    this.refs.thenTemplateRef = templateReference;
  }

  @Input()
  set cvIfSuccess(monadValue: Result<TE, TD> | null) {
    if (monadValue) {
      if (isSuccess(monadValue)) {
        this.context.ifTrue = true;
        this.context.$implicit = monadValue.value;
      } else if (isFailure(monadValue)) {
        this.context.ifTrue = false;
        this.context.$implicit = monadValue.value;
      }
    } else {
      this.context.ifTrue = false;
    }
    updateView(this.context, this.refs);
  }

  @Input()
  set cvIfSuccessThen(templateReference: TemplateRef<IfContext<TE | TD>> | null) {
    this.refs.thenTemplateRef = templateReference;
    this.refs.thenViewRef = null;
    updateView(this.context, this.refs);
  }

  @Input()
  set cvIfSuccessElse(templateReference: TemplateRef<IfContext<TE | TD>> | null) {
    this.refs.elseTemplateRef = templateReference;
    this.refs.elseViewRef = null;
    updateView(this.context, this.refs);
  }

  static ngTemplateGuard_cvIfSuccess: 'binding';

  static ngTemplateContextGuard<TE, TD>(
    _directive: IfSuccessDirective<TE, TD>,
    _context: any
  ): _context is IfContext<Exclude<TD, false | 0 | '' | null | undefined>> {
    return true;
  }
}
