import { Directive, Input, ViewContainerRef, TemplateRef } from '@angular/core';

import { isPending, Result } from '@lonli-lokli/ts-result';

import { initialIfContext, initialReferences, IfContext, updateView } from './common';

@Directive({ selector: '[cvIfPending]', standalone: true })
export class IfPendingDirective<TE = unknown, TD = unknown> {
  private _context: IfContext<TE | TD> = initialIfContext();

  private _refs = initialReferences();

  constructor(viewContainer: ViewContainerRef, templateReference: TemplateRef<IfContext<TE | TD>>) {
    this._refs.viewContainer = viewContainer;
    this._refs.thenTemplateRef = templateReference;
  }

  @Input()
  set cvIfPending(data: Result<TE, TD> | null) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    this._context.$implicit = null!;
    this._context.ifTrue = data !== null && isPending(data) ? true : false;
    updateView(this._context, this._refs);
  }

  @Input()
  set cvIfPendingThen(templateReference: TemplateRef<IfContext<TE | TD>> | null) {
    this._refs.thenTemplateRef = templateReference;
    this._refs.thenViewRef = null;
    updateView(this._context, this._refs);
  }

  @Input()
  set cvIfPendingElse(templateReference: TemplateRef<IfContext<TE | TD>> | null) {
    this._refs.elseTemplateRef = templateReference;
    this._refs.elseViewRef = null;
    updateView(this._context, this._refs);
  }

  static ngTemplateGuard_cvIfPending: 'binding';

  static ngTemplateContextGuard<TE, TD>(
    _directive: IfPendingDirective<TE, TD>,
    _context: any
  ): _context is IfContext<Exclude<TD, false | 0 | '' | null | undefined>> {
    return true;
  }
}
