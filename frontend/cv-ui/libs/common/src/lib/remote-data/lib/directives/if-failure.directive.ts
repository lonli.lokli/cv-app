import { Directive, Input, ViewContainerRef, TemplateRef } from '@angular/core';

import { isFailure, isSuccess, Result } from '@lonli-lokli/ts-result';

import { initialIfContext, initialReferences, IfContext, updateView } from './common';

@Directive({ selector: '[cvIfFailure]', standalone: true })
export class IfFailureDirective<TE = unknown, TD = unknown> {
  private _context: IfContext<TE | TD> = initialIfContext();

  private _refs = initialReferences();

  constructor(viewContainer: ViewContainerRef, templateReference: TemplateRef<IfContext<TE | TD>>) {
    this._refs.viewContainer = viewContainer;
    this._refs.thenTemplateRef = templateReference;
  }

  @Input()
  set cvIfFailure(data: Result<TE, TD> | null) {
    if (data) {
      if (isFailure(data)) {
        this._context.ifTrue = true;
        this._context.$implicit = data.value;
      }
      if (isSuccess(data)) {
        this._context.ifTrue = false;
        this._context.$implicit = data.value;
      }
    }
    updateView(this._context, this._refs);
  }

  @Input()
  set cvIfFailureThen(templateReference: TemplateRef<IfContext<TE | TD>> | null) {
    this._refs.thenTemplateRef = templateReference;
    this._refs.thenViewRef = null;
    updateView(this._context, this._refs);
  }

  @Input()
  set cvIfFailureElse(templateReference: TemplateRef<IfContext<TE | TD>> | null) {
    this._refs.elseTemplateRef = templateReference;
    this._refs.elseViewRef = null;
    updateView(this._context, this._refs);
  }

  static ngTemplateGuard_cvIfFailure: 'binding';

  static ngTemplateContextGuard<TE, TD>(
    _dirirective: IfFailureDirective<TE, TD>,
    _context: any
  ): _context is IfContext<Exclude<TE, false | 0 | '' | null | undefined>> {
    return true;
  }
}
