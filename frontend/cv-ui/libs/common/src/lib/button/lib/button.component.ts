import {
  Component,
  Input,
  HostListener,
  HostBinding,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter,
  TemplateRef
} from '@angular/core';
import { of } from 'rxjs';
import { tap, switchMap, catchError } from 'rxjs/operators';

import { CvIconType, IconComponent } from '@cv/common/icons';

import {
  ButtonClickEvent,
  ButtonMode,
  ButtonLabelTemplateContext,
  ButtonDropdownTemplateContext
} from './button.model';
import { NgIf, NgSwitch, NgSwitchCase, NgTemplateOutlet, PercentPipe } from '@angular/common';
import { TippyDirective } from '@ngneat/helipopper';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'button[cv-button]',
  exportAs: 'cvButton',
  standalone: true,
  styleUrls: ['./button.component.scss'],
  templateUrl: './button.component.html',
  imports: [TippyDirective, IconComponent, NgIf, PercentPipe, NgSwitch, NgSwitchCase, NgTemplateOutlet],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent {
  @Input()
  public label: string | undefined;

  @Input()
  public icon: CvIconType | undefined;

  @Input()
  public iconColor: string | undefined;

  @Input()
  public disabled = false;

  @Input()
  public externalProgress = false;

  @Input()
  public progressPercent: number | undefined;

  @Input()
  public tabIndex = 0;

  @Output()
  public buttonClick = new EventEmitter<ButtonClickEvent>();

  @HostBinding('class.disabled')
  public get isDisabled() {
    return this.disabled || this.inProgress;
  }

  @HostBinding('attr.tabindex')
  public get tabIdx() {
    return this.disabled ? -1 : this.tabIndex;
  }

  public modes = ButtonMode;

  public get mode(): ButtonMode {
    if (this.dropdownTemplateRef) {
      return ButtonMode.dropdown;
    }

    return ButtonMode.simple;
  }

  public labelTemplateRef: TemplateRef<ButtonLabelTemplateContext> | null = null;
  public dropdownTemplateRef: TemplateRef<ButtonDropdownTemplateContext> | null = null;

  public get labelTemplateContext(): ButtonLabelTemplateContext {
    return {
      inProgress: this.inProgress
    };
  }

  public get inProgress() {
    return this.buttonClickProcessing || this.progressPercent !== undefined || this.externalProgress;
  }

  private buttonClickProcessing = false;

  constructor(private changeDetectorReference: ChangeDetectorRef) {}

  @HostListener('click')
  public onClick() {
    of(undefined)
      .pipe(
        tap(() => {
          this.buttonClickProcessing = true;
          this.changeDetectorReference.markForCheck();
        }),
        switchMap(() => {
          const event = new ButtonClickEvent();
          this.buttonClick.emit(event);
          return event.step;
        }),
        catchError(() => of(true))
      )
      .subscribe(() => {
        this.buttonClickProcessing = false;
        this.changeDetectorReference.markForCheck();
      });
  }
}
