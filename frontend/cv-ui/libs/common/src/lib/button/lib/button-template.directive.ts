import { Directive, TemplateRef } from '@angular/core';

import { ButtonComponent } from './button.component';
import { ButtonDropdownTemplateContext, ButtonLabelTemplateContext } from './button.model';

@Directive({ selector: '[cvButtonLabelTemplate]', standalone: true })
export class ButtonLabelTemplateDirective {
  public constructor(templateReference: TemplateRef<ButtonLabelTemplateContext>, button: ButtonComponent) {
    button.labelTemplateRef = templateReference;
  }
}

@Directive({ selector: '[cvButtonDropdownTemplate]', standalone: true })
export class ButtonDropdownTemplateDirective {
  public constructor(templateReference: TemplateRef<ButtonDropdownTemplateContext>, button: ButtonComponent) {
    button.dropdownTemplateRef = templateReference;
  }
}
