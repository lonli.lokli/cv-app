import { Observable, of } from 'rxjs';

export enum ButtonMode {
  simple,
  dropdown
}

export class ButtonClickEvent {
  public step: Observable<any> = of({});
  public wait(action: Observable<any>) {
    this.step = action;
  }

  constructor(public data?: any) {}
}

export type ButtonLabelTemplateContext = { $implicit?: unknown } | { inProgress: boolean };

export interface ButtonDropdownTemplateContext {
  hide: () => void;
}
