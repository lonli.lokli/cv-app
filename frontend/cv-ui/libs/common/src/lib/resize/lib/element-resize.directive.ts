import { Output, Directive, ElementRef, EventEmitter, NgZone, OnInit, Input } from '@angular/core';
import { debounceTime, Observable } from 'rxjs';

import { BaseDirective } from '@cv/common/core';

@Directive({
  selector: '[cvResize]',
  standalone: true
})
export class ElementResizeDirective extends BaseDirective implements OnInit {
  @Output()
  public cvResize: EventEmitter<HTMLElement> = new EventEmitter<HTMLElement>();

  @Input()
  public delay = 100;

  private element: HTMLElement;

  constructor(
    elementReference: ElementRef<HTMLElement>,
    private ngZone: NgZone
  ) {
    super();
    this.element =
      elementReference.nativeElement instanceof Comment
        ? (elementReference.nativeElement.parentNode as HTMLElement) // allow use on ng-container
        : elementReference.nativeElement;
  }
  ngOnInit(): void {
    this.addSubscription(
      new ResizeObservable(this.element).pipe(debounceTime(this.delay)).subscribe(() => {
        this.ngZone.run(() => this.cvResize.emit(this.element));
      })
    );
  }
}

class ResizeObservable extends Observable<ResizeObserverEntry[]> {
  constructor(element: HTMLElement) {
    super(subscriber => {
      const resizeObserver = new ResizeObserver(entries => {
        subscriber.next(entries);
      });

      // Observe one or multiple elements
      resizeObserver.observe(element);

      return function unsubscribe() {
        resizeObserver.unobserve(element);
        resizeObserver.disconnect();
      };
    });
  }
}
