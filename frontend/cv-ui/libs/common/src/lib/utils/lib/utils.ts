import { HttpErrorResponse } from '@angular/common/http';

export function isInstance<T>(ctor: new (...arguments_: any[]) => T): (x: any) => x is T {
  return <(x: any) => x is T>(x => x instanceof ctor);
}

export const extractErrorDescription = (error: any, defaultErrorMessage: string): string =>
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  error instanceof HttpErrorResponse ? error.error?.error_description ?? error.message : defaultErrorMessage;

export const isDefined = <T>(value: T): value is NonNullable<T> => value !== undefined && value !== null;

export const isNullable = <T>(value: T | null | undefined): value is null | undefined =>
  value === undefined || value === null;

export function hasOneOrMore<TA, TI = TA>(x: ArrayLike<TI> | null | undefined): x is [TI, ...TI[]] {
  return isDefined(x) && x.length > 0;
}

export function isLength<T>(array: T[], length: 0): array is [];
export function isLength<T>(array: T[], length: 1): array is [T];
export function isLength<T>(array: T[], length: 2): array is [T, T];
export function isLength<T>(array: T[], length: 3): array is [T, T, T];
export function isLength<T>(array: T[], length: number): array is T[] {
  return array.length === length;
}
export const ensureKey = <T>(k: keyof T) => k;

export const logit = (message = 'DEBUG, ', ...a: unknown[]) => {
  console.log(`%c[${message}]`, 'background: #009688; color: #fff; padding: 3px; font-size: 9px;', a);
  return true;
};

export const isEmptyString = (value: string | null | undefined): value is null | undefined | '' =>
  isNullable(value) || value.trim() === '';

export const isNonEmptyString = (value: string | null | undefined): value is null | undefined | '' =>
  isDefined(value) && value.trim() !== '';
