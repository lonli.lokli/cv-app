import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';

import { ApiError } from '@cv/api';

import { isDefined } from './utils';

export const getOriginFromUrl = (url: string | null): string => {
  const unknownUrl = '(unknown url)';

  if (!url) {
    return unknownUrl;
  }

  const regex = /(https?:\/\/)?([^\s/]+\/)(.*)/;
  const result = regex.exec(url);
  return result ? `${result[1]}${result[2]}` : unknownUrl;
};

const apiErrorTitle = 'API Failure';
export const parseApiError = (error: HttpErrorResponse, errorMessage = 'Cannot load data.'): ApiError => {
  const asJson = errorAsJson(error.error);
  switch (error.status) {
    case 0:
      return {
        title: apiErrorTitle,
        details: getOriginFromUrl(error.url || '') + ' is not accessible at the moment.'
      };
    case HttpStatusCode.NotFound:
      return {
        title: apiErrorTitle,
        details: `Something went wrong while accessing ${error.url}`
      };
    default:
      return isDefined(error.error) && isCvError(asJson)
        ? {
            details: asJson.detail,
            title: asJson.title
          }
        : {
            title: apiErrorTitle,
            details: errorMessage
          };
  }
};

const safeParseAsJson = (v: string) => {
  try {
    return JSON.parse(v);
  } catch {
    return v;
  }
};
const errorAsJson = (error: unknown) => (typeof error === 'string' ? safeParseAsJson(error) : error);

type CvErrorResponse = {
  detail: string;
  status: number;
  title: string;
};

const isCvError = (error: unknown): error is CvErrorResponse =>
  typeof error === 'object' && error !== null && 'detail' in error && 'title' in error;
