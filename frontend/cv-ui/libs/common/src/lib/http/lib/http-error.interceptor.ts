import { HttpRequest, HttpEvent, HttpErrorResponse, HttpInterceptorFn, HttpHandlerFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { Observable, throwError, catchError } from 'rxjs';

import { NOTIFY_ERROR } from '@cv/common/core';
import { NotificationService } from '@cv/common/notification';
import { getOriginFromUrl, parseApiError } from '@cv/common/utils';

export const HTTP_ERROR_INTERCEPTOR: HttpInterceptorFn = (
  request: HttpRequest<unknown>,
  next: HttpHandlerFn
): Observable<HttpEvent<unknown>> =>
  next(request).pipe(
    catchError(response => {
      const notifyError = request.context.get(NOTIFY_ERROR);
      const notificationService = inject(NotificationService);
      if (response instanceof HttpErrorResponse && notifyError) {
        // HTTP status codes 400/422 used as validation response
        switch (response.status) {
          case 400:
            {
              if (response.headers.get('X-Notify-User') === 'True') {
                notificationService.showError(response.error);
              } else {
                try {
                  const httpError: any = JSON.parse(response.error);
                  console.log(httpError);
                } catch (error) {
                  console.error('Http Error', error);
                }

                notificationService.showError(`Something went wrong while accessing ${response.url}`);
              }
            }
            break;
          case 401:
            break;

          // TODO: maybe we do not need notifications here? And move all to parseApiError

          case 422:
            notificationService.showError(`${response.error}`);
            break;

          case 405:
            notificationService.showError(`${getOriginFromUrl(response.url)} : ${response.statusText}`);
            break;

          default: {
            // It might be an error from dev Middleware
            const apiError = parseApiError(response, undefined);
            if (apiError) {
              notificationService.showApiError(apiError);
            }
          }
        }
      }
      return throwError(() => response);
    })
  );
