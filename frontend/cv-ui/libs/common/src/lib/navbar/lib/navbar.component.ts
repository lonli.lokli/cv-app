import { ChangeDetectionStrategy, Component, ElementRef, Input, NgZone } from '@angular/core';
import { RouterLinkActive, RouterModule } from '@angular/router';
import { tap } from 'rxjs';

import { AfterViewInitDirective, BaseComponent, executeOnStable } from '@cv/common/core';
import { ElementResizeDirective } from '@cv/common/resize';
import { Nullable } from '@cv/typings';

import { NavbarData } from './navbar.model';
import { ButtonDropdownTemplateDirective } from '@cv/common/button';
import { NgFor, NgIf } from '@angular/common';

interface PageData {
  path: string;
  data: NavbarData;
}

interface NavigationBarData extends PageData {
  width?: number;
  link?: RouterLinkActive;
  overflowed?: boolean;
}
@Component({
  selector: 'cv-navbar',
  standalone: true,
  imports: [RouterModule, ElementResizeDirective, ButtonDropdownTemplateDirective, AfterViewInitDirective, NgIf, NgFor],
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent extends BaseComponent<NavbarComponent> {
  @Input()
  public pages: Nullable<PageData>[] = [];

  public hasOverflowItems = false;

  public get itemsEx() {
    return this.pages as NavigationBarData[];
  }

  private navbarWidth = 0;
  private navsWidth = 0;

  constructor(private ngZone: NgZone) {
    super();
    this.observeInputProperty('pages')
      .pipe(tap(() => executeOnStable(this.ngZone, () => this.ngZone.run(() => this.updateNavbar()))))
      .subscribe();
  }

  onResize(element: HTMLElement) {
    this.navbarWidth = element.clientWidth;
    this.updateNavbar();
  }

  public afterNavBarInit(elementReference: ElementRef, nav: NavigationBarData, linkActive: RouterLinkActive) {
    if (!nav.width) {
      const rootElement = elementReference.nativeElement as HTMLElement;
      nav.width = rootElement.offsetWidth;
      nav.link = linkActive;

      this.navsWidth += nav.width;
    }
  }

  private updateNavbar() {
    let overflowedWidth = 0;

    const lastElementIndex = this.itemsEx.length - 1;
    for (let index = lastElementIndex; index >= 0; index--) {
      const navBar = this.itemsEx[index];
      navBar.overflowed = false;

      if (!navBar.link?.isActive && this.navsWidth - overflowedWidth > this.navbarWidth) {
        navBar.overflowed = true;

        if (!overflowedWidth) {
          const moreButtonWidth = 92;
          overflowedWidth -= moreButtonWidth;
        }
        overflowedWidth += navBar.width ?? 0;
      }
    }

    this.hasOverflowItems = this.itemsEx.some(item => item.overflowed === true);
  }
}
