export class NavbarData {
  constructor(
    public name: string,
    public options: {
      group?: string;
    } = {}
  ) {}
}
