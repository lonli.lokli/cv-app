import { Input, Component, HostBinding, ChangeDetectionStrategy } from '@angular/core';
import { Observable, of as observableOf } from 'rxjs';
import { delay, switchMap } from 'rxjs/operators';

import { BaseComponent } from '@cv/common/core';
import { IconComponent } from '@cv/common/icons';
import { AsyncPipe, NgIf } from '@angular/common';

@Component({
  selector: 'cv-progress',
  standalone: true,
  styleUrls: ['./progress.component.scss'],
  templateUrl: './progress.component.html',
  imports: [IconComponent, NgIf, AsyncPipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressComponent extends BaseComponent<ProgressComponent> {
  @HostBinding('class.cvProgress')
  @Input()
  public progress = false;

  @Input()
  public text = 'Loading...';

  @Input()
  public delay = 50;

  public progress$: Observable<boolean>;

  constructor() {
    super();

    this.progress$ = this.observeInputProperty('progress', true).pipe(
      switchMap(value => {
        if (this.delay !== 0) {
          return observableOf(value).pipe(delay(value ? this.delay : 0));
        }
        return observableOf(value);
      })
    );
  }
}
