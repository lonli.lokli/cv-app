export * from './lib/tab.component';
export * from './lib/tabset.component';
export * from './lib/tab-heading.directive';
export * from './lib/tab-template.directive';
