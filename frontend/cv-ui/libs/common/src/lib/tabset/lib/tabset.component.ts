import {
  Component,
  QueryList,
  Input,
  Output,
  EventEmitter,
  ContentChildren,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  OnInit
} from '@angular/core';
import { startWith, Subscription } from 'rxjs';

import { BaseComponent } from '@cv/common/core';

import { TabComponent } from './tab.component';
import { NgFor, NgIf, NgTemplateOutlet } from '@angular/common';

interface SubscriptionPairs {
  tab: TabComponent;
  subscription: Subscription;
}

@Component({
  selector: 'cv-tabset',
  standalone: true,
  styleUrls: ['./tabset.component.scss'],
  templateUrl: './tabset.component.html',
  imports: [NgFor, NgIf, NgTemplateOutlet]
})
export class TabsetComponent
  extends BaseComponent<TabsetComponent>
  implements AfterContentInit, AfterViewChecked, AfterViewInit, OnInit
{
  @Input()
  public activeTabName = '';

  @Input()
  public activateFirstTab = false;

  @Output()
  public activeTabNameChange: EventEmitter<string> = new EventEmitter<string>();

  @ContentChildren(TabComponent, { descendants: true })
  public tabs: QueryList<TabComponent> | undefined;

  private viewInited = false;

  private removedActiveTabIndex: number | undefined;
  private subscriptionPairs: SubscriptionPairs[] = [];

  ngOnInit(): void {
    this.addSubscription(
      this.observeInputProperty('activeTabName', true).subscribe(() => {
        if (this.viewInited) {
          this.setActiveTabByName();
        }
      })
    );
  }

  ngAfterContentInit() {
    if (this.tabs) {
      this.addSubscription(
        this.tabs.changes.pipe(startWith(new QueryList<TabComponent>())).subscribe(() => {
          {
            const tabs = this.tabs!.toArray();
            // Remove
            this.subscriptionPairs.forEach((pair: SubscriptionPairs, index: number) => {
              const { tab, subscription } = pair;
              if (!tabs.includes(tab)) {
                if (tab.active) {
                  this.removedActiveTabIndex = index;
                }
                if (!subscription.closed) {
                  subscription.unsubscribe();
                }
                this.subscriptionPairs.splice(index, 1);
                tab.removed.emit(tab);
              }
            });
            // Add new
            tabs
              .filter((tab: TabComponent) => this.subscriptionPairs.findIndex(pair => pair.tab === tab) === -1)
              .forEach((tab: TabComponent) => {
                this.subscriptionPairs.push({
                  tab,
                  subscription: tab.activated.subscribe((t: TabComponent) => this.onTabSelect(t))
                });
              });
          }
        })
      );
    }
  }

  ngAfterViewChecked(): void {
    if (this.removedActiveTabIndex !== undefined) {
      const newActiveIndex = this.getClosestTabIndex(this.removedActiveTabIndex);
      if (newActiveIndex !== -1 && this.tabs) {
        this.tabs.toArray()[newActiveIndex].active = true;
      }
      this.removedActiveTabIndex = undefined;
    }
  }

  private getClosestTabIndex(index: number): number {
    if (this.tabs) {
      const tabs = this.tabs.toArray();
      if (index === 0 && this.isTabCanBeActive(tabs[index])) {
        return index;
      }

      const tabsLength = tabs.length;
      for (let step = 1; step <= tabsLength; step += 1) {
        const previousIndex = index - step;
        const nextIndex = index + step;
        if (this.isTabCanBeActive(tabs[previousIndex])) {
          return previousIndex;
        }
        if (this.isTabCanBeActive(tabs[nextIndex])) {
          return nextIndex;
        }
      }
    }
    return -1;
  }

  private isTabCanBeActive(tab: TabComponent) {
    return tab && !tab.disabled;
  }

  public ngAfterViewInit(): void {
    this.viewInited = true;
    if (this.activeTabName) {
      this.setActiveTabByName();
    } else if (this.getActiveIndex() === -1 && this.tabs && this.activateFirstTab) {
      this.tabs.forEach((tab: TabComponent, index: number) => {
        tab.active = index === 0;
      });
    }
  }

  private getActiveIndex() {
    return this.tabs?.toArray().findIndex(tab => tab.active);
  }

  private setActiveTabByName() {
    if (this.tabs) {
      const tab = this.tabs.find(t => t.name === this.activeTabName);
      if (tab && !tab.active) {
        tab.active = true;
      }
    }
  }

  public onTabSelect(selectedTab: TabComponent) {
    if (this.tabs) {
      this.tabs.filter((tab: TabComponent) => tab !== selectedTab).forEach((tab: TabComponent) => (tab.active = false));

      this.activeTabName = selectedTab.name;
      this.activeTabNameChange.next(this.activeTabName);
    }
  }

  onResize() {}

  setActiveTab(tab: TabComponent) {
    tab.active = true;
  }
}
