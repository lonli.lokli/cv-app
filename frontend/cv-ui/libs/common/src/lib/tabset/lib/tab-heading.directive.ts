import { Directive, TemplateRef } from '@angular/core';

import { TabComponent } from './tab.component';

@Directive({ selector: '[cvTabHeading]', standalone: true })
export class TabHeadingDirective {
  public constructor(
    public templateReference: TemplateRef<any>,
    tab: TabComponent
  ) {
    tab.headingReference = templateReference;
  }
}
