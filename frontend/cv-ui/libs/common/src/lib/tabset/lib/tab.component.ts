import { NgIf, NgSwitch, NgSwitchCase } from '@angular/common';
import { Component, EventEmitter, HostBinding, Input, Output, TemplateRef, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'cv-tab',
  standalone: true,
  styleUrls: ['./tab.component.scss'],
  templateUrl: './tab.component.html',
  imports: [NgSwitch, NgSwitchCase, NgIf]
})
export class TabComponent {
  @Input() public name = '';
  @Input() public heading = '';
  @Input() public disabled = false;
  @Input() public removable = false;
  @Input() public customClass = '';

  @HostBinding('class.active')
  @Input()
  public get active(): boolean {
    return this._active;
  }

  public set active(active: boolean) {
    if ((this.disabled && active) || !active) {
      if (!active) {
        this._active = active;
      }

      this.deactivated.emit(this);
      this.changeDetectorReference.markForCheck();
      return;
    }

    this._active = active;
    this.activated.emit(this);
    this.changeDetectorReference.markForCheck();
  }

  @HostBinding('class.has-content')
  @Input()
  public get hasContent(): boolean {
    return this._hasContent;
  }

  public set hasContent(value: boolean) {
    this._hasContent = value;
  }

  @Output() public activated: EventEmitter<TabComponent> = new EventEmitter<TabComponent>(false);
  @Output() public deactivated: EventEmitter<TabComponent> = new EventEmitter<TabComponent>(false);
  @Output() public removed: EventEmitter<TabComponent> = new EventEmitter<TabComponent>(false);

  @HostBinding('class.tab-pane') public addClass = true;

  public headingReference: TemplateRef<any> | null = null;
  public contentReference: TemplateRef<any> | null = null;

  public get headingResolved(): string {
    return this.heading || this.name;
  }

  protected _active = false;
  private _hasContent = true;

  public constructor(private changeDetectorReference: ChangeDetectorRef) {}
}
