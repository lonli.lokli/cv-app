import { Directive, TemplateRef } from '@angular/core';

import { TabComponent } from './tab.component';

@Directive({ selector: '[cvTabTemplate]', standalone: true })
export class TabTemplateDirective {
  public constructor(
    public templateReference: TemplateRef<any>,
    tab: TabComponent
  ) {
    tab.contentReference = templateReference;
  }
}
