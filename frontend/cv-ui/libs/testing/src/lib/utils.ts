import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Provider, Type } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { API_PROVIDERS } from '@cv/api';

import { BuildSettings, HOST_SETTINGS } from '@cv/common/core';

export const commonRootModulesToImport = [HttpClientTestingModule, RouterTestingModule, API_PROVIDERS];

export const commonRootServicesToMock: Type<any>[] = [BuildSettings];

export const TEST_PROVIDERS: Provider[] = [
  {
    provide: HOST_SETTINGS,
    useValue: {
      api: 'test_endpoint'
    }
  }
];
