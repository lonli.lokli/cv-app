import { Injectable } from '@angular/core';

import { DocumentContracts, ExternalApiCall as ApiResponse, tDocumentsService, mapValidation } from '@cv/api';
import { DocumentDm } from '@cv/docs/typings';

@Injectable()
export class DocumentService {
  constructor(private transportSvc: tDocumentsService) {}

  public getDocuments(): ApiResponse<DocumentDm[]> {
    return this.transportSvc.getApiDocuments().pipe(mapValidation(DocumentContracts));
  }

  public saveDocument(file: File): ApiResponse<void> {
    return this.transportSvc.putApiDocuments({
      body: file
    });
  }

  public downloadDocument(documentId: string): ApiResponse<Blob> {
    return this.transportSvc.getApiDocumentsDocumentId({
      documentId: documentId
    });
  }

  public removeDocument(documentId: string): ApiResponse<void> {
    return this.transportSvc.deleteApiDocumentsDocumentId({
      documentId: documentId
    });
  }
}
