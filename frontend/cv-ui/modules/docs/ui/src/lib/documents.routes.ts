import { Routes } from '@angular/router';

import { DocumentService } from '@cv/docs/data-access';

import { DocumentListComponent } from './document-list/document-list.component';
import { DocumentsComponent } from './documents.component';

export const DOCUMENTS_ROUTES: Routes = [
  {
    path: '',
    component: DocumentsComponent,
    providers: [DocumentService],
    children: [
      {
        path: '',
        component: DocumentListComponent
      }
    ]
  }
];
