import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { DocumentDm } from '@cv/docs/typings';

@Component({
  selector: 'docs-document-item',
  standalone: true,
  templateUrl: './document-item.component.html',
  styleUrls: ['./document-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocumentItemComponent {
  @Input() doc!: DocumentDm;
  @Output() remove: EventEmitter<void> = new EventEmitter<void>();
  @Output() download: EventEmitter<void> = new EventEmitter<void>();

  public removeItem() {
    this.remove.emit();
  }

  public downloadItem() {
    this.download.emit();
  }
}
