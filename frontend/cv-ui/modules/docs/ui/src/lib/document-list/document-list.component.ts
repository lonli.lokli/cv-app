import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { Store } from 'effector';

import { ApiError } from '@cv/api';
import { fadeInAnimation } from '@cv/common/animation';
import { AfterViewInitDirective, BaseComponent, LetDirective } from '@cv/common/core';
import { IfSuccessDirective, RemoteDataComponent } from '@cv/common/remote-data';
import { hasOneOrMore } from '@cv/common/utils';
import { DocumentDm } from '@cv/docs/typings';
import { Result } from '@lonli-lokli/ts-result';

import { DocumentItemComponent } from '../document-item/document-item.component';
import { DocumentListService } from './document-list.service';
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'docs-document-list',
  standalone: true,
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss'],
  imports: [
    RemoteDataComponent,
    IfSuccessDirective,
    DocumentItemComponent,
    LetDirective,
    AfterViewInitDirective,
    AsyncPipe
  ],
  providers: [DocumentListService],

  animations: [fadeInAnimation],
  host: {
    '[@fadeInAnimation]': ''
  },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocumentListComponent extends BaseComponent {
  private api = inject(DocumentListService);
  public data$: Store<Result<ApiError, DocumentDm[]>> = this.api.$documents;

  upload(target: HTMLInputElement | null): void {
    if (!target) return;
    const fileList = target.files;
    if (hasOneOrMore(fileList)) {
      this.api.addDocument(fileList[0]);
    }
  }

  removeDocumentItem(document: DocumentDm): void {
    this.api.removeDocument(document.id);
  }

  downloadDocumentItem(document: DocumentDm): void {
    this.api.downloadDocument(document.id);
  }

  requestLoadDocuments() {
    this.api.loadDocuments();
  }

  onRefresh() {
    this.api.refreshData();
  }
}
