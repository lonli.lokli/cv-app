import { createEffect, createEvent, createStore, sample } from 'effector';
import FileSaver from 'file-saver';
import { lastValueFrom } from 'rxjs';

import { ApiError } from '@cv/api';
import { DocumentService } from '@cv/docs/data-access';
import { DocumentDm } from '@cv/docs/typings';
import { fromEither, initial, pending, Result } from '@lonli-lokli/ts-result';
import { mapEither, tapEither } from '@cv/fp';
import { Injectable } from '@angular/core';

@Injectable()
export class DocumentListService {
  // events
  public loadDocuments = createEvent();
  public downloadDocument = createEvent<string>();
  public addDocument = createEvent<File>();
  public removeDocument = createEvent<string>();
  public refreshData = createEvent();

  // stores
  public $documents = createStore<Result<ApiError, DocumentDm[]>>(initial);

  constructor(private api: DocumentService) {
    const loadDocumentsFx = createEffect(() =>
      lastValueFrom(this.api.getDocuments().pipe(mapEither(documents => documents.map(d => d as DocumentDm))))
    );

    const addDocumentFx = createEffect((file: File) => lastValueFrom(this.api.saveDocument(file)));

    const removeDocumentFx = createEffect((documentId: string) => lastValueFrom(this.api.removeDocument(documentId)));

    const downloadDocumentFx = createEffect((documentId: string) =>
      lastValueFrom(this.api.downloadDocument(documentId).pipe(tapEither(FileSaver.saveAs)))
    );

    sample({ clock: this.removeDocument, target: removeDocumentFx });

    sample({ clock: this.downloadDocument, target: downloadDocumentFx });

    sample({
      clock: [this.loadDocuments, addDocumentFx.doneData, removeDocumentFx.doneData, this.refreshData],
      target: loadDocumentsFx
    });

    this.$documents
      .on(loadDocumentsFx.pending, (previous, inProgress) => (inProgress ? pending : previous))
      .on(loadDocumentsFx.doneData, (_, data) => fromEither(data));
  }
}
