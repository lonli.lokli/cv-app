import { Injectable } from '@angular/core';

import { JobContracts, tJobsService, mapValidation } from '@cv/api';

@Injectable()
export class JobService {
  constructor(private transportSvc: tJobsService) {}

  public getJobs() {
    return this.transportSvc.getApiJobs().pipe(mapValidation(JobContracts));
  }
}
