import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'jobs-component',
  standalone: true,
  imports: [RouterModule],
  styleUrls: ['./jobs.component.scss'],
  templateUrl: './jobs.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JobsComponent {}
