import { NgFor } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BaseSubscribable, CallPipe } from '@cv/common/core';
import { TabComponent, TabHeadingDirective, TabsetComponent } from '@cv/common/tabset';
import { ColleagueSection } from '@cv/typings';
import { map, pluck } from 'rxjs';

import { JobsNavigationService } from '../jobs-navigation.service';
import { stateSectionToTitle } from '../shared/state-section-to-title';

@Component({
  selector: 'jobs-tabs-component',
  standalone: true,
  templateUrl: './tabs-view.component.html',
  styleUrls: ['./tabs-view.component.scss'],
  imports: [RouterModule, TabsetComponent, TabComponent, TabHeadingDirective, CallPipe, NgFor],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabsViewComponent extends BaseSubscribable {
  public tab: ColleagueSection | undefined;
  public tabs: ColleagueSection[] = ['request', 'require'];

  constructor(private navigationService: JobsNavigationService) {
    super();
    this.addSubscription(
      this.navigationService.stateParams$
        .pipe(
          pluck('section'),
          map((section: ColleagueSection | undefined) => section ?? this.tabs[0])
        )
        .subscribe((section: ColleagueSection) => (this.tab = section))
    );
  }

  public toTitle(section: ColleagueSection) {
    return stateSectionToTitle(section);
  }

  goToTab(section: ColleagueSection) {
    this.navigationService.doNavigate(section);
  }
}
