import { createEffect, createEvent, createStore, sample } from 'effector';
import { lastValueFrom } from 'rxjs';

import { ApiError } from '@cv/api';
import { fromEither, initial, pending, Result } from '@lonli-lokli/ts-result';
import { JobService } from '@cv/jobs/data-access';
import { JobDm } from '@cv/jobs/typings';
import { Injectable } from '@angular/core';

@Injectable()
export class RequireComponentService {
  // events
  public loadJobsRequest = createEvent();

  // stores
  public $jobs = createStore<Result<ApiError, JobDm[]>>(initial);
  constructor(private apiService: JobService) {
    // effects
    const loadJobsFx = createEffect(() => lastValueFrom(this.apiService.getJobs()));

    // logic

    sample({
      clock: this.loadJobsRequest,
      target: loadJobsFx
    });

    sample({
      clock: loadJobsFx.pending,
      filter: inProgress => inProgress,
      fn: _ => pending,
      target: this.$jobs
    });

    sample({
      clock: loadJobsFx.doneData,
      fn: jobs => fromEither(jobs),
      target: this.$jobs
    });
  }
}
