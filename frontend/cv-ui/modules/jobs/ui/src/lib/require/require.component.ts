import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DialogService } from '@ngneat/dialog';
import { DxDataGridModule } from 'devextreme-angular';
import { Store } from 'effector';

import { ApiError } from '@cv/api';
import { IfSuccessDirective, RemoteDataComponent } from '@cv/common/remote-data';
import { Result } from '@lonli-lokli/ts-result';
import { AddJobModalComponent } from '@cv/jobs/modal';
import { JobDm } from '@cv/jobs/typings';

import { AfterViewInitDirective, LetDirective } from '@cv/common/core';
import { ButtonComponent } from '@cv/common/button';
import { RequireComponentService } from './require.component.service';
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'jobs-require',
  standalone: true,
  imports: [
    DxDataGridModule,
    RemoteDataComponent,
    IfSuccessDirective,
    LetDirective,
    AfterViewInitDirective,
    ButtonComponent,
    AsyncPipe
  ],
  providers: [RequireComponentService],
  templateUrl: './require.component.html',
  styleUrls: ['./require.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequireComponent {
  public data$: Store<Result<ApiError, JobDm[]>>;
  public readonly allowedPageSizes = [5, 10, 'all' as const];

  constructor(
    private dialog: DialogService,
    private svc: RequireComponentService
  ) {
    this.data$ = this.svc.$jobs;
  }

  requestLoadJobs() {
    this.svc.loadJobsRequest();
  }

  onAddJob() {
    this.dialog.open(AddJobModalComponent);
  }
}
