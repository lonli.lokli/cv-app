import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { BaseNavigationService } from '@cv/common/core';
import { ColleagueSection } from '@cv/typings';

@Injectable()
export class JobsNavigationService extends BaseNavigationService<ColleagueSection, ''> {
  constructor(router: Router, title: Title) {
    super(router, title, 'colleague');
  }

  public doNavigate(section: ColleagueSection) {
    this.navigateTo(section, undefined, undefined);
  }
}
