import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'jobs-request',
  standalone: true,
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestComponent {}
