import { Routes } from '@angular/router';
import { identity } from 'rxjs';

import { JobService } from '@cv/jobs/data-access';
import { ColleagueSection } from '@cv/typings';

import { JobsNavigationService } from './jobs-navigation.service';
import { JobsComponent } from './jobs.component';
import { RequestComponent } from './request/request.component';
import { RequireComponent } from './require/require.component';
import { ColleagueRoutePath } from './shared/route-path';
import { TabsViewComponent } from './tabs-view/tabs-view.component';

export const COLLEAGUES_ROUTES: Routes = [
  {
    path: '',
    component: JobsComponent,
    providers: [JobService, JobsNavigationService],
    children: [
      {
        path: '',
        component: TabsViewComponent,
        children: [
          {
            path: ColleagueRoutePath.request,
            component: RequestComponent,
            data: {
              section: identity<ColleagueSection>('request')
            }
          },
          {
            path: ColleagueRoutePath.require,
            component: RequireComponent,
            data: {
              section: identity<ColleagueSection>('require')
            }
          }
        ]
      }
    ]
  }
];
