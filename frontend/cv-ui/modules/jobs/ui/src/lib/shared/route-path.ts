import { ColleagueSection } from '@cv/typings';
import { identity } from 'rxjs';

export class ColleagueRoutePath {
  public static request = identity<ColleagueSection>('request');
  public static require = identity<ColleagueSection>('require');
}
