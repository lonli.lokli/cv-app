import { NeverError } from '@cv/fp';
import { ColleagueSection } from '@cv/typings';

export const stateSectionToTitle = (section: ColleagueSection): string => {
  switch (section) {
    case 'request':
      return 'Request';
    case 'require':
      return 'Require';
    default:
      throw new NeverError(section);
  }
};
