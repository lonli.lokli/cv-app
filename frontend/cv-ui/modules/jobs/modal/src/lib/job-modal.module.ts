import { NgModule } from '@angular/core';

import { AddJobModalComponent } from './add-job-modal/add-job-modal.component';

const components = [AddJobModalComponent];

@NgModule({
  declarations: [...components]
})
export class JobsModalModule {}
