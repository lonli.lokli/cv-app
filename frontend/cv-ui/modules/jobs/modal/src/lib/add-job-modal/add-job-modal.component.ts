import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DialogRef } from '@ngneat/dialog';

interface Data {
  title: string;
}

@Component({
  templateUrl: 'add-job-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddJobModalComponent {
  get title() {
    if (!this.reference.data) return 'Hello world';
    return this.reference.data.title;
  }

  constructor(public reference: DialogRef<Data, boolean>) {}
}
