export interface SalaryDm {
  min: number;
  max: number;
}

export interface CompanyDm {
  name: string;
}
export interface JobDm {
  salary: SalaryDm;
  company: CompanyDm;
}
