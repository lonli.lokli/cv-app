import { Routes } from '@angular/router';

import { InfoService } from '@cv/about/data-access';

import { AboutComponent } from './about.component';

export const ABOUT_ROUTES: Routes = [
  {
    path: '',
    providers: [InfoService],
    component: AboutComponent
  }
];
