import { HttpContext } from '@angular/common/http';
import { createEffect, createEvent, createStore, sample } from 'effector';
import { lastValueFrom } from 'rxjs';

import { InfoService } from '@cv/about/data-access';
import { ApiError } from '@cv/api';
import { NOTIFY_ERROR } from '@cv/common/core';
import { fromEither, initial, pending, Result } from '@lonli-lokli/ts-result';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiInfoService {
  // events
  public loadInfo = createEvent();

  // stores
  public $info = createStore<Result<ApiError, Record<string, unknown>>>(initial);

  constructor(private api: InfoService) {
    // effects

    // do not show notification as it will close popup
    const loadInfoFx = createEffect(() => lastValueFrom(this.api.getInfos(new HttpContext().set(NOTIFY_ERROR, false))));

    // logic

    sample({
      clock: this.loadInfo,
      target: loadInfoFx
    });

    sample({
      clock: loadInfoFx.pending,
      filter: inProgress => inProgress,
      fn: _ => pending,
      target: this.$info
    });

    sample({
      clock: loadInfoFx.doneData,
      fn: infos => fromEither(infos),
      target: this.$info
    });
  }
}
