import { AsyncPipe, JsonPipe, KeyValuePipe, NgFor } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { InfoService } from '@cv/about/data-access';

import { BaseComponent, LetDirective, AfterViewInitDirective, BuildSettings } from '@cv/common/core';
import { IfSuccessDirective, RemoteDataComponent } from '@cv/common/remote-data';

import { ApiInfoService } from './api-info.service';

@Component({
  selector: 'about-api-info',
  standalone: true,
  templateUrl: './api-info.component.html',
  styleUrls: ['./api-info.component.scss'],
  imports: [
    RemoteDataComponent,
    IfSuccessDirective,
    LetDirective,
    AfterViewInitDirective,
    JsonPipe,
    AsyncPipe,
    NgFor,
    KeyValuePipe
  ],
  providers: [ApiInfoService, InfoService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApiInfoComponent extends BaseComponent {
  private svc: ApiInfoService = inject(ApiInfoService);
  public info$ = this.svc.$info;
  public buildInfo = inject(BuildSettings);

  public load() {
    this.svc.loadInfo();
  }
}
