import { ChangeDetectionStrategy, Component } from '@angular/core';

import { fadeInAnimation } from '@cv/common/animation';

@Component({
  selector: 'about-component',
  standalone: true,
  templateUrl: './about.component.html',
  animations: [fadeInAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    '[@fadeInAnimation]': ''
  }
})
export class AboutComponent {}
