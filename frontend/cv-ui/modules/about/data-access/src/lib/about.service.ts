import { HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { tInfoService } from '@cv/api';

@Injectable()
export class InfoService {
  constructor(private transportSvc: tInfoService) {}

  public getInfos(context?: HttpContext) {
    return this.transportSvc.getInfo(undefined, context);
  }
}
