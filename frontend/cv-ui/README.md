# Fair CV Frontend

This project contains frontend source code for 'Fair CV' application.

## Table Of Content

- [Prerequisites](#prerequisites)
- [Running Locally](#running-locally)
- [Project Structure](#project-structure)
- [License](#license)

<a name="prerequisites"></a>

## Prerequisites

- Node.js ([18](https://nodejs.org/dist/latest-v18.x/))

<a name="running-locally"></a>

## Running application locally

- Run `npm run start`
- Open `http://localhost:4200/` in the browser

<a name="project-structure"></a>

## Project Structure

| Folder Name | Description                         |
| ----------- | ----------------------------------- |
| apps        | Application (thin shells)           |
| doubles     | Test settings and methods           |
| libs        | Libraries with shared functionality |
| modules     | Pages (aka modules) of application  |
| tools       | Generators, scripts for application |

<a name="license"></a>

## License

MIT
