[![pipeline status](https://gitlab.com/lonli.lokli/cv-app/badges/develop/pipeline.svg)](https://gitlab.com/lonli.lokli/cv-app/-/commits/develop)  [![coverage](https://gitlab.com/lonli.lokli/cv-app/badges/develop/coverage.svg?job=test&key_text=API+Coverage&key_width=80)](https://gitlab.com/lonli.lokli/cv-app/-/commits/develop)  [![coverage](https://gitlab.com/lonli.lokli/cv-app/badges/develop/coverage.svg?job=test:jest&key_text=UI+Coverage&key_width=80)](https://gitlab.com/lonli.lokli/cv-app/-/commits/develop)

[PROD](https://faircv.today/)

# High-level architecture
![architecture.svg](/infrastructure/architecture.svg)

# Structure
- [Infrastructure scripts](/infrastructure/README.md)
- [API (.NET Core)](/backend/cv-api/README.md)
- [UI (Angular)](/frontend/cv-ui/README.md)

# THANKS
[List of people, articles and ideas](/THANKS.md)