Resources used:
* https://medium.com/@michaeldimoudis/hardening-asp-net-core-3-1-docker-images-f0c2ede1667f
* https://www.softwaredeveloper.blog/multi-project-dotnet-core-solution-in-docker-image
* https://github.com/terraform-aws-modules/terraform-aws-ecs
* https://andrewlock.net/using-serilog-aspnetcore-in-asp-net-core-3-logging-the-selected-endpoint-name-with-serilog/
* https://medium.com/xebia-engineering/best-practices-to-create-organize-terraform-code-for-aws-2f41625
* https://github.com/SamHerbert/SVG-Loaders
* https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs