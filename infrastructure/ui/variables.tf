locals {
  region        = "us-east-1" # CloudFront expects ACM resources in us-east-1 region only
  bucket_name   = "prod.faircv.today" # it's used in gitlab
  domain_name   = "www.faircv.today"
}