# High-level architecture
![architecture.svg](/architecture.svg)

All infrastructure is hosted on `AWS` and created with `Terraform`. Also you can open [Wiki](https://gitlab.com/lonli.lokli/cv-app/-/wikis/home) if you have access.