locals {
  region           = "eu-west-2" # Europe (London)
  namespace        = "complete-ecs"
  ecs_service_name = "cv-api"
  stage            = "production"
  owner            = "you"
  usage            = "ECS"

  api_env_s3_bucketname = "vo-api-s3-env"

  # This is the convention we use to know what belongs to each other
  resources_name = "${local.namespace}-${local.stage}"

  newrelic_account_id = "754728514883"
  newrelic_external_id = "3191275"

  tags_as_map = {
    Owner       = "user"
    Environment = "dev"
  }
}
