resource "aws_cloudwatch_log_group" "service" {
  name              = "ecs-${var.cluster_name}"
  retention_in_days = 14
}

resource "aws_ecs_task_definition" "service" {
  family = var.service
  execution_role_arn = var.task_execution_role_arn
  cpu = 512
  memory = 512
  container_definitions = <<EOF
[
  {
    "name": "${var.service}",
    "image": "${var.service}",
    "cpu": 512,
    "memory": 512,
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 8080
      }
    ],
    "environmentFiles": [
      {
        "value": "arn:aws:s3:::${var.api_env_s3_bucketname}/${var.api_env_filename}",
        "type": "s3"
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "${var.region}",
        "awslogs-group": "ecs-${var.cluster_name}",
        "awslogs-stream-prefix": "service"
      }
    }
  }
]
EOF
}

resource "aws_ecs_service" "service" {
  name = var.service
  cluster = var.cluster_id
  task_definition = aws_ecs_task_definition.service.arn

  desired_count = 1

  deployment_maximum_percent = 100
  deployment_minimum_healthy_percent = 0
}