terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.45"
    }
    newrelic = {
      source  = "newrelic/newrelic"
      version = ">= 2.42.0"
    }
  }
}