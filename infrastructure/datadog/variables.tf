locals {
  newrelic_account_id = "3191275"
  newrelic_region = "EU"
  newrelic_cloudwatch_endpoint = "https://aws-api.eu01.nr-data.net/cloudwatch-metrics/v1"
  newrelic_linked_account_name = "FairCV_01"

  aws_region           = "eu-west-2" # AWS Region Europe (London)
}