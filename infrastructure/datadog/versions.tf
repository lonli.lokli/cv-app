terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.45"
    }
    local = {
      source  = "hashicorp/local"
      version = ">= 1.3"
    }
    datadog = {
      source  = "datadog/datadog"
      version = ">= 2.12"
    }
  }
}
